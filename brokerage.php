<?php
  session_start();
  include "etc/om_config.inc";
  if(!isset($_SESSION['user']))
{
  header("Location: login.php");
}
else
{
?>
<HTML>
<HEAD><TITLE>Item settings</TITLE></HEAD>
<BODY bgColor="#FFCEE7">
  <FORM name="form1" action="<?php echo $_SERVER['PHP_SELF']; ?>" METHOD="post">
  <A href="./index.php">Home</A>&nbsp;&nbsp;&nbsp;<A href="./itemAdd.php">Add Item</A>
  <TABLE border="1" cellspacing="0" cellpadding="6">
  <TR>
  <TD align="center" align="center">
    <?php
      if(isset($_SESSION['ses_decidePackFor']) && $_SESSION['ses_decidePackFor'] == 7 )
        $selectedExchange = "F_O";
      else if(isset($_SESSION['ses_decidePackFor']) && $_SESSION['ses_decidePackFor'] == 6 )
        $selectedExchange = "MCX";
      else
        $selectedExchange = "All";
      if(isset($_POST['exchangeName']))
        $selectedExchange = $_POST['exchangeName'];
      
	  //CREATE THE EXCHANGE COMBO : start
      $selectExchang = "SELECT * FROM exchange";
      $selectExchangResult = mysql_query($selectExchang);
      $exchangeId = "";
      $exchange = "";
      echo "<SELECT name='exchangeName' onChange='submit();'>";
      echo "<option>All</option>";
      while($rowExchange = mysql_fetch_array($selectExchangResult))
      {
        if($rowExchange['exchange'] == $selectedExchange)           //THIS IS FOR SELECTED VALUE WHICH WE SELECT
          echo "<option selected>".$rowExchange['exchange']."</option>"; 
        else
          echo "<option>".$rowExchange['exchange']."</option>";
      }
      echo "</SELECT>";
      //CREATE THE EXCHANGE COMBO : end
    ?>
  </TD>
</TR>	
  <TR>
    <TD colspan="9" ><B>Item settings : </B></TD>
  </TR>
  <TR>
    <TD>Item</TD>
    <TD>Brok1</TD>
    <TD>Brok2</TD>
    <TD>OneSideBrok</TD>
    <TD>Minimum</TD>
    <TD>Price on</TD>
    <TD>Price Range</TD>
	<TD>Exchange</TD>
    <TD>&nbsp;</TD>
    <TD>&nbsp;</TD>
  </TR>
<?php
  //  include "etc/om_config.inc";
    $query = "SELECT * FROM item ";
    if(isset($selectedExchange) && $selectedExchange != "All")
		$query .= "where exchange ='".$selectedExchange."' ";        

	$query .= "ORDER BY exchange,item";

	$result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
      echo "
        <TR>
          <TD align='right'>".$row['item']."</TD>
          <TD align='right'>".$row['brok']."</TD>
          <TD align='right'>".$row['brok2']."</TD>
          <TD align='right'>".$row['oneSideBrok']."</TD>
          <TD align='right'>".$row['min']."</TD>
          <TD align='right'>".$row['priceOn']."</TD>
          <TD align='right' NOWRAP>".$row['rangeStart']." :: ".$row['rangeEnd']."</TD>
		  <TD align='right'>".$row['exchange']."</TD>
          <TD align='right'><a href='editBrokerage.php?item=".$row['item']."'>Edit</a></TD>
          <TD align='right'><a href='deleteBrokerage.php?item=".$row['item']."' ONCLICK='return confirm(\"Are You Sure?\");'>Delete</a></TD>
        </TR>" ;  
    }
?>
  </TABLE>
  </FORM>
</BODY>
</HTML>
<?php
}
?>