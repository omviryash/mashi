# Database Backup#
# Backup Date: 2016-02-29 11:20:11

drop table if exists bankmaster;
create table bankmaster (
  bankId int(6) not null auto_increment,
  bankName varchar(60) not null ,
  phone1 varchar(12) not null ,
  phone2 varchar(12) not null ,
  PRIMARY KEY (bankId)
);

insert into bankmaster (bankId, bankName, phone1, phone2) values ('1', 'Bill', '', '');
drop table if exists bhavcopy;
create table bhavcopy (
  bhavcopyid int(10) not null auto_increment,
  exchange varchar(30) not null ,
  bhavcopyDate date default '0000-00-00' not null ,
  sessionId varchar(15) not null ,
  marketType varchar(15) not null ,
  instrumentId int(10) default '0' not null ,
  instrumentName varchar(15) not null ,
  scriptCode int(10) default '0' not null ,
  contractCode varchar(20) not null ,
  scriptGroup varchar(5) not null ,
  scriptType varchar(5) not null ,
  expiryDate date default '0000-00-00' not null ,
  expiryDateBc varchar(10) not null ,
  strikePrice float default '0' not null ,
  optionType varchar(4) not null ,
  previousClosePrice float default '0' not null ,
  openPrice float default '0' not null ,
  highPrice float default '0' not null ,
  lowPrice float default '0' not null ,
  closePrice float default '0' not null ,
  totalQtyTrade int(10) default '0' not null ,
  totalValueTrade double default '0' not null ,
  lifeHigh float default '0' not null ,
  lifeLow float default '0' not null ,
  quoteUnits varchar(10) not null ,
  settlementPrice float default '0' not null ,
  noOfTrades int(6) default '0' not null ,
  openInterest double default '0' not null ,
  avgTradePrice float default '0' not null ,
  tdcl float default '0' not null ,
  lstTradePrice float default '0' not null ,
  remarks text not null ,
  PRIMARY KEY (bhavcopyid)
);

drop table if exists brokrecieved;
create table brokrecieved (
  brokRecievedId int(11) unsigned not null auto_increment,
  brokDate date default '0000-00-00' ,
  clientId int(6) default '0' ,
  exchange varchar(20) ,
  brok double ,
  lot double ,
  PRIMARY KEY (brokRecievedId)
);

drop table if exists cashflow;
create table cashflow (
  cashFlowId int(6) not null auto_increment,
  clientId int(10) default '0' not null ,
  itemIdExpiryDate varchar(50) ,
  dwStatus char(2) not null ,
  dwAmount double default '0' not null ,
  plStatus char(2) not null ,
  plAmount double default '0' not null ,
  transactionDate date ,
  transType varchar(20) ,
  transMode varchar(30) not null ,
  exchange varchar(20) ,
  PRIMARY KEY (cashFlowId)
);

drop table if exists client;
create table client (
  clientId int(6) not null auto_increment,
  passwd varchar(30) ,
  firstName varchar(35) ,
  middleName varchar(35) ,
  lastName varchar(35) ,
  oneSideBrok float ,
  openingDate date ,
  opening float ,
  deposit int(6) ,
  currentBal float ,
  address text ,
  phone varchar(30) ,
  mobile varchar(22) ,
  fax varchar(10) ,
  email varchar(60) ,
  oneSide tinyint(1) default '0' ,
  remiser int(6) default '0' ,
  remiserBrok float default '0' ,
  remiserBrokIn tinyint(1) default '1' ,
  clientBroker int(2) not null ,
  PRIMARY KEY (clientId)
);

insert into client (clientId, passwd, firstName, middleName, lastName, oneSideBrok, openingDate, opening, deposit, currentBal, address, phone, mobile, fax, email, oneSide, remiser, remiserBrok, remiserBrokIn, clientBroker) values ('3012', NULL, 'MAYURBHAI', '', '', NULL, NULL, NULL, '0', NULL, '', '', '', '', '', '0', '0', '0', '1', '0');
insert into client (clientId, passwd, firstName, middleName, lastName, oneSideBrok, openingDate, opening, deposit, currentBal, address, phone, mobile, fax, email, oneSide, remiser, remiserBrok, remiserBrokIn, clientBroker) values ('3020', NULL, 'RAVIBHAI', '', '', NULL, NULL, NULL, '0', NULL, '', '', '', '', '', '0', '0', '0', '1', '0');
insert into client (clientId, passwd, firstName, middleName, lastName, oneSideBrok, openingDate, opening, deposit, currentBal, address, phone, mobile, fax, email, oneSide, remiser, remiserBrok, remiserBrokIn, clientBroker) values ('3038', NULL, 'KAMLESHBHAO ', 'SONI', '', NULL, NULL, NULL, '0', NULL, '', '', '', '', '', '0', '0', '0', '1', '0');
insert into client (clientId, passwd, firstName, middleName, lastName, oneSideBrok, openingDate, opening, deposit, currentBal, address, phone, mobile, fax, email, oneSide, remiser, remiserBrok, remiserBrokIn, clientBroker) values ('3042', NULL, 'LALABHAI', 'UNA', '', NULL, NULL, NULL, '0', NULL, '', '', '', '', '', '0', '0', '0', '1', '0');
insert into client (clientId, passwd, firstName, middleName, lastName, oneSideBrok, openingDate, opening, deposit, currentBal, address, phone, mobile, fax, email, oneSide, remiser, remiserBrok, remiserBrokIn, clientBroker) values ('3083', NULL, 'HITESHBHAI', '', '', NULL, NULL, NULL, '0', NULL, '', '', '', '', '', '0', '0', '0', '1', '0');
insert into client (clientId, passwd, firstName, middleName, lastName, oneSideBrok, openingDate, opening, deposit, currentBal, address, phone, mobile, fax, email, oneSide, remiser, remiserBrok, remiserBrokIn, clientBroker) values ('3091', NULL, 'DHRUBHAI', '', '', NULL, NULL, NULL, '0', NULL, '', '', '', '', '', '0', '0', '0', '1', '0');
insert into client (clientId, passwd, firstName, middleName, lastName, oneSideBrok, openingDate, opening, deposit, currentBal, address, phone, mobile, fax, email, oneSide, remiser, remiserBrok, remiserBrokIn, clientBroker) values ('3095', NULL, 'PINAKINBHAI', 'PHOPHANI', '', NULL, NULL, NULL, '0', NULL, '', '', '', '', '', '0', '0', '0', '1', '0');
insert into client (clientId, passwd, firstName, middleName, lastName, oneSideBrok, openingDate, opening, deposit, currentBal, address, phone, mobile, fax, email, oneSide, remiser, remiserBrok, remiserBrokIn, clientBroker) values ('9999', NULL, 'zz', '', '', NULL, NULL, NULL, '0', NULL, '', '', '', '', '', '0', '0', '0', '1', '1');
insert into client (clientId, passwd, firstName, middleName, lastName, oneSideBrok, openingDate, opening, deposit, currentBal, address, phone, mobile, fax, email, oneSide, remiser, remiserBrok, remiserBrokIn, clientBroker) values ('10006', NULL, 'NIRANJAN', 'VYAS', '', NULL, NULL, NULL, '0', NULL, '', '', '', '', '', '0', '0', '0', '1', '0');
insert into client (clientId, passwd, firstName, middleName, lastName, oneSideBrok, openingDate, opening, deposit, currentBal, address, phone, mobile, fax, email, oneSide, remiser, remiserBrok, remiserBrokIn, clientBroker) values ('10008', NULL, 'MAHESHBHAI', '', '', NULL, NULL, NULL, '0', NULL, '', '', '', '', '', '0', '0', '0', '1', '0');
insert into client (clientId, passwd, firstName, middleName, lastName, oneSideBrok, openingDate, opening, deposit, currentBal, address, phone, mobile, fax, email, oneSide, remiser, remiserBrok, remiserBrokIn, clientBroker) values ('10009', NULL, 'NIRANJANBHAI', '', '', NULL, NULL, NULL, '0', NULL, '', '', '', '', '', '0', '0', '0', '1', '0');
insert into client (clientId, passwd, firstName, middleName, lastName, oneSideBrok, openingDate, opening, deposit, currentBal, address, phone, mobile, fax, email, oneSide, remiser, remiserBrok, remiserBrokIn, clientBroker) values ('10010', NULL, 'SANDYBHAI', '', '', NULL, NULL, NULL, '0', NULL, '', '', '', '', '', '0', '0', '0', '1', '0');
drop table if exists clientbrok;
create table clientbrok (
  clientBrokId int(6) not null auto_increment,
  clientId int(6) ,
  itemId varchar(50) ,
  exchange varchar(30) not null ,
  oneSideBrok float ,
  brok1 float ,
  brok2 float ,
  PRIMARY KEY (clientBrokId)
);

insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1', '9999', 'ABAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2', '9999', 'ABB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3', '9999', 'ABGSHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('4', '9999', 'ABIRLANUVO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('5', '9999', 'ACC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('6', '9999', 'ADLABSFILM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('7', '9999', 'ALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('8', '9999', 'ALOKTEXT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('9', '9999', 'AMBUJACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('10', '9999', 'AMTEKAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('11', '9999', 'ANDHRABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('12', '9999', 'APIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('13', '9999', 'APTECHT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('14', '9999', 'ARVIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('15', '9999', 'ASHOKLEY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('16', '9999', 'ASIANPAINT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('17', '9999', 'AUROPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('18', '9999', 'AXISBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('19', '9999', 'BAJAJAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('20', '9999', 'BAJAJHIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('21', '9999', 'BAJAJHLDNG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('22', '9999', 'BALAJITELE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('23', '9999', 'BALLARPUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('24', '9999', 'BALRAMCHIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('25', '9999', 'BANKBARODA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('26', '9999', 'BANKINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('27', '9999', 'BANKNIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('28', '9999', 'BATAINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('29', '9999', 'BEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('30', '9999', 'BEML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('31', '9999', 'BHARATFORG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('32', '9999', 'BHARTIARTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('33', '9999', 'BHEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('34', '9999', 'BHUSANSTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('35', '9999', 'BIOCON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('36', '9999', 'BIRLACORPN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('37', '9999', 'BOMDYEING', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('38', '9999', 'BOSCHLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('39', '9999', 'BPCL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('40', '9999', 'BRFL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('41', '9999', 'CAIRN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('42', '9999', 'CANBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('43', '9999', 'CENTRALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('44', '9999', 'CENTURYTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('45', '9999', 'CESC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('46', '9999', 'CHAMBLFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('47', '9999', 'CHENNPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('48', '9999', 'CIPLA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('49', '9999', 'COLPAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('50', '9999', 'CONCOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('51', '9999', 'CORPBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('52', '9999', 'CROMPGREAV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('53', '9999', 'CUMMINSIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('54', '9999', 'DABUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('55', '9999', 'DCB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('56', '9999', 'DCHL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('57', '9999', 'DENABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('58', '9999', 'DISHTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('59', '9999', 'DIVISLAB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('60', '9999', 'DLF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('61', '9999', 'DRREDDY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('62', '9999', 'EDELWEISS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('63', '9999', 'EDUCOMP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('64', '9999', 'EKC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('65', '9999', 'ESCORTS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('66', '9999', 'ESSAROIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('67', '9999', 'EVERONN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('68', '9999', 'FEDERALBNK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('69', '9999', 'FINANTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('70', '9999', 'FSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('71', '9999', 'GAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('72', '9999', 'GDL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('73', '9999', 'GESHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('74', '9999', 'GITANJALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('75', '9999', 'GLAXO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('76', '9999', 'GMRINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('77', '9999', 'GNFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('78', '9999', 'GRASIM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('79', '9999', 'GSPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('80', '9999', 'GTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('81', '9999', 'GTLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('82', '9999', 'GTOFFSHORE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('83', '9999', 'GUJALKALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('84', '9999', 'GVKPIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('85', '9999', 'HAVELLS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('86', '9999', 'HCC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('87', '9999', 'HCLINSYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('88', '9999', 'HCLTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('89', '9999', 'HDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('90', '9999', 'HDFCBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('91', '9999', 'HDIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('92', '9999', 'HEROHONDA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('93', '9999', 'HINDALCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('94', '9999', 'HINDOILEXP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('95', '9999', 'HINDPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('96', '9999', 'HINDUNILVR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('97', '9999', 'HINDZINC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('98', '9999', 'HOTELEELA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('99', '9999', 'IBREALEST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('100', '9999', 'ICICIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('101', '9999', 'ICSA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('102', '9999', 'IDBI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('103', '9999', 'IDEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('104', '9999', 'IDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('105', '9999', 'IFCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('106', '9999', 'INDHOTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('107', '9999', 'INDIACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('108', '9999', 'INDIAINFO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('109', '9999', 'INDIANB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('110', '9999', 'INDUSINDBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('111', '9999', 'INFOSYSTCH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('112', '9999', 'IOB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('113', '9999', 'IOC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('114', '9999', 'IRB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('115', '9999', 'ISPATIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('116', '9999', 'ITC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('117', '9999', 'IVRCLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('118', '9999', 'JETAIRWAYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('119', '9999', 'JINDALSAW', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('120', '9999', 'JINDALSTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('121', '9999', 'JPASSOCIAT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('122', '9999', 'JPHYDRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('123', '9999', 'JSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('124', '9999', 'JSWSTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('125', '9999', 'KESORAMIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('126', '9999', 'KFA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('127', '9999', 'KOTAKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('128', '9999', 'KSK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('129', '9999', 'KSOILS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('130', '9999', 'KTKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('131', '9999', 'LAXMIMACH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('132', '9999', 'LICHSGFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('133', '9999', 'LITL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('134', '9999', 'LT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('135', '9999', 'LUPIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('136', '9999', 'MAHLIFE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('137', '9999', 'MAHSEAMLES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('138', '9999', 'MARUTI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('139', '9999', 'MCDOWELL_N', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('140', '9999', 'MINDTREE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('141', '9999', 'minifty', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('142', '9999', 'MLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('143', '9999', 'MONNETISPA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('144', '9999', 'MOSERBAER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('145', '9999', 'MPHASIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('146', '9999', 'MRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('147', '9999', 'MRPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('148', '9999', 'MTNL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('149', '9999', 'M_M', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('150', '9999', 'NAGARCONST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('151', '9999', 'NAGARFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('152', '9999', 'NATIONALUM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('153', '9999', 'NBVENTURES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('154', '9999', 'NDTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('155', '9999', 'NETWORK18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('156', '9999', 'NEYVELILIG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('157', '9999', 'NIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('158', '9999', 'NIITLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('159', '9999', 'NOIDATOLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('160', '9999', 'NTPC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('161', '9999', 'OFSS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('162', '9999', 'ONGC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('163', '9999', 'OPTOCIRCUI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('164', '9999', 'ORCHIDCHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('165', '9999', 'ORIENTBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('166', '9999', 'PANTALOONR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('167', '9999', 'PATELENG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('168', '9999', 'PATNI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('169', '9999', 'PENINLAND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('170', '9999', 'PETRONET', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('171', '9999', 'PFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('172', '9999', 'PIRHEALTH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('173', '9999', 'PNB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('174', '9999', 'POLARIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('175', '9999', 'POWERGRID', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('176', '9999', 'PRAJIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('177', '9999', 'PTC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('178', '9999', 'PUNJLLOYD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('179', '9999', 'RAJESHEXPO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('180', '9999', 'RANBAXY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('181', '9999', 'RCOM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('182', '9999', 'RECLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('183', '9999', 'RELCAPITAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('184', '9999', 'RELIANCE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('185', '9999', 'RELINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('186', '9999', 'RENUKA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('187', '9999', 'RIIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('188', '9999', 'RNRL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('189', '9999', 'ROLTA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('190', '9999', 'RPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('191', '9999', 'RPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('192', '9999', 'SAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('193', '9999', 'SBIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('194', '9999', 'SCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('195', '9999', 'SESAGOA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('196', '9999', 'SIEMENS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('197', '9999', 'SINTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('198', '9999', 'SKUMARSYNF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('199', '9999', 'SREINTFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('200', '9999', 'SRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('201', '9999', 'STAR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('202', '9999', 'STER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('203', '9999', 'STERLINBIO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('204', '9999', 'SUNPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('205', '9999', 'SUNTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('206', '9999', 'SUZLON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('207', '9999', 'SYNDIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('208', '9999', 'TATACHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('209', '9999', 'TATACOMM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('210', '9999', 'TATAMOTORS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('211', '9999', 'TATAPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('212', '9999', 'TATASTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('213', '9999', 'TATATEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('214', '9999', 'TCS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('215', '9999', 'TECHM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('216', '9999', 'THERMAX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('217', '9999', 'TITAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('218', '9999', 'TORNTPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('219', '9999', 'TRIVENI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('220', '9999', 'TTML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('221', '9999', 'TULIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('222', '9999', 'TV18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('223', '9999', 'TVSMOTOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('224', '9999', 'UCOBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('225', '9999', 'ULTRACEMCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('226', '9999', 'UNIONBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('227', '9999', 'UNIPHOS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('228', '9999', 'UNITECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('229', '9999', 'UTVSOF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('230', '9999', 'VIJAYABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('231', '9999', 'VOLTAS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('232', '9999', 'WELGUJ', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('233', '9999', 'WIPRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('234', '9999', 'WOCKPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('235', '9999', 'YESBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('236', '9999', 'COPPER', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('237', '9999', 'CRUDEOIL', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('238', '9999', 'GOLD', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('239', '9999', 'GOLDM', 'MCX', '100', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('240', '9999', 'LEAD', 'MCX', '1000', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('241', '9999', 'NATURALGAS', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('242', '9999', 'NICKEL', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('243', '9999', 'SILVER', 'MCX', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('244', '9999', 'ZINC', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('245', '3012', 'ABAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('246', '3012', 'ABB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('247', '3012', 'ABGSHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('248', '3012', 'ABIRLANUVO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('249', '3012', 'ACC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('250', '3012', 'ADLABSFILM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('251', '3012', 'ALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('252', '3012', 'ALOKTEXT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('253', '3012', 'AMBUJACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('254', '3012', 'AMTEKAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('255', '3012', 'ANDHRABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('256', '3012', 'APIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('257', '3012', 'APTECHT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('258', '3012', 'ARVIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('259', '3012', 'ASHOKLEY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('260', '3012', 'ASIANPAINT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('261', '3012', 'AUROPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('262', '3012', 'AXISBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('263', '3012', 'BAJAJAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('264', '3012', 'BAJAJHIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('265', '3012', 'BAJAJHLDNG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('266', '3012', 'BALAJITELE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('267', '3012', 'BALLARPUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('268', '3012', 'BALRAMCHIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('269', '3012', 'BANKBARODA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('270', '3012', 'BANKINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('271', '3012', 'BANKNIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('272', '3012', 'BATAINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('273', '3012', 'BEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('274', '3012', 'BEML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('275', '3012', 'BHARATFORG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('276', '3012', 'BHARTIARTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('277', '3012', 'BHEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('278', '3012', 'BHUSANSTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('279', '3012', 'BIOCON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('280', '3012', 'BIRLACORPN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('281', '3012', 'BOMDYEING', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('282', '3012', 'BOSCHLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('283', '3012', 'BPCL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('284', '3012', 'BRFL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('285', '3012', 'CAIRN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('286', '3012', 'CANBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('287', '3012', 'CENTRALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('288', '3012', 'CENTURYTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('289', '3012', 'CESC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('290', '3012', 'CHAMBLFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('291', '3012', 'CHENNPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('292', '3012', 'CIPLA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('293', '3012', 'COLPAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('294', '3012', 'CONCOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('295', '3012', 'CORPBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('296', '3012', 'CROMPGREAV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('297', '3012', 'CUMMINSIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('298', '3012', 'DABUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('299', '3012', 'DCB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('300', '3012', 'DCHL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('301', '3012', 'DENABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('302', '3012', 'DISHTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('303', '3012', 'DIVISLAB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('304', '3012', 'DLF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('305', '3012', 'DRREDDY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('306', '3012', 'EDELWEISS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('307', '3012', 'EDUCOMP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('308', '3012', 'EKC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('309', '3012', 'ESCORTS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('310', '3012', 'ESSAROIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('311', '3012', 'EVERONN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('312', '3012', 'FEDERALBNK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('313', '3012', 'FINANTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('314', '3012', 'FSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('315', '3012', 'GAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('316', '3012', 'GDL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('317', '3012', 'GESHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('318', '3012', 'GITANJALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('319', '3012', 'GLAXO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('320', '3012', 'GMRINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('321', '3012', 'GNFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('322', '3012', 'GRASIM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('323', '3012', 'GSPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('324', '3012', 'GTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('325', '3012', 'GTLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('326', '3012', 'GTOFFSHORE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('327', '3012', 'GUJALKALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('328', '3012', 'GVKPIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('329', '3012', 'HAVELLS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('330', '3012', 'HCC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('331', '3012', 'HCLINSYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('332', '3012', 'HCLTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('333', '3012', 'HDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('334', '3012', 'HDFCBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('335', '3012', 'HDIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('336', '3012', 'HEROHONDA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('337', '3012', 'HINDALCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('338', '3012', 'HINDOILEXP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('339', '3012', 'HINDPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('340', '3012', 'HINDUNILVR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('341', '3012', 'HINDZINC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('342', '3012', 'HOTELEELA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('343', '3012', 'IBREALEST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('344', '3012', 'ICICIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('345', '3012', 'ICSA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('346', '3012', 'IDBI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('347', '3012', 'IDEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('348', '3012', 'IDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('349', '3012', 'IFCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('350', '3012', 'INDHOTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('351', '3012', 'INDIACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('352', '3012', 'INDIAINFO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('353', '3012', 'INDIANB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('354', '3012', 'INDUSINDBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('355', '3012', 'INFOSYSTCH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('356', '3012', 'IOB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('357', '3012', 'IOC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('358', '3012', 'IRB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('359', '3012', 'ISPATIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('360', '3012', 'ITC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('361', '3012', 'IVRCLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('362', '3012', 'JETAIRWAYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('363', '3012', 'JINDALSAW', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('364', '3012', 'JINDALSTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('365', '3012', 'JPASSOCIAT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('366', '3012', 'JPHYDRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('367', '3012', 'JSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('368', '3012', 'JSWSTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('369', '3012', 'KESORAMIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('370', '3012', 'KFA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('371', '3012', 'KOTAKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('372', '3012', 'KSK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('373', '3012', 'KSOILS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('374', '3012', 'KTKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('375', '3012', 'LAXMIMACH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('376', '3012', 'LICHSGFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('377', '3012', 'LITL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('378', '3012', 'LT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('379', '3012', 'LUPIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('380', '3012', 'MAHLIFE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('381', '3012', 'MAHSEAMLES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('382', '3012', 'MARUTI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('383', '3012', 'MCDOWELL_N', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('384', '3012', 'MINDTREE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('385', '3012', 'minifty', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('386', '3012', 'MLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('387', '3012', 'MONNETISPA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('388', '3012', 'MOSERBAER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('389', '3012', 'MPHASIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('390', '3012', 'MRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('391', '3012', 'MRPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('392', '3012', 'MTNL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('393', '3012', 'M_M', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('394', '3012', 'NAGARCONST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('395', '3012', 'NAGARFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('396', '3012', 'NATIONALUM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('397', '3012', 'NBVENTURES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('398', '3012', 'NDTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('399', '3012', 'NETWORK18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('400', '3012', 'NEYVELILIG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('401', '3012', 'NIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('402', '3012', 'NIITLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('403', '3012', 'NOIDATOLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('404', '3012', 'NTPC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('405', '3012', 'OFSS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('406', '3012', 'ONGC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('407', '3012', 'OPTOCIRCUI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('408', '3012', 'ORCHIDCHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('409', '3012', 'ORIENTBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('410', '3012', 'PANTALOONR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('411', '3012', 'PATELENG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('412', '3012', 'PATNI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('413', '3012', 'PENINLAND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('414', '3012', 'PETRONET', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('415', '3012', 'PFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('416', '3012', 'PIRHEALTH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('417', '3012', 'PNB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('418', '3012', 'POLARIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('419', '3012', 'POWERGRID', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('420', '3012', 'PRAJIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('421', '3012', 'PTC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('422', '3012', 'PUNJLLOYD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('423', '3012', 'RAJESHEXPO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('424', '3012', 'RANBAXY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('425', '3012', 'RCOM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('426', '3012', 'RECLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('427', '3012', 'RELCAPITAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('428', '3012', 'RELIANCE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('429', '3012', 'RELINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('430', '3012', 'RENUKA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('431', '3012', 'RIIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('432', '3012', 'RNRL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('433', '3012', 'ROLTA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('434', '3012', 'RPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('435', '3012', 'RPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('436', '3012', 'SAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('437', '3012', 'SBIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('438', '3012', 'SCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('439', '3012', 'SESAGOA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('440', '3012', 'SIEMENS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('441', '3012', 'SINTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('442', '3012', 'SKUMARSYNF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('443', '3012', 'SREINTFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('444', '3012', 'SRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('445', '3012', 'STAR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('446', '3012', 'STER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('447', '3012', 'STERLINBIO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('448', '3012', 'SUNPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('449', '3012', 'SUNTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('450', '3012', 'SUZLON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('451', '3012', 'SYNDIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('452', '3012', 'TATACHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('453', '3012', 'TATACOMM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('454', '3012', 'TATAMOTORS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('455', '3012', 'TATAPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('456', '3012', 'TATASTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('457', '3012', 'TATATEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('458', '3012', 'TCS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('459', '3012', 'TECHM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('460', '3012', 'THERMAX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('461', '3012', 'TITAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('462', '3012', 'TORNTPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('463', '3012', 'TRIVENI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('464', '3012', 'TTML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('465', '3012', 'TULIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('466', '3012', 'TV18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('467', '3012', 'TVSMOTOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('468', '3012', 'UCOBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('469', '3012', 'ULTRACEMCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('470', '3012', 'UNIONBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('471', '3012', 'UNIPHOS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('472', '3012', 'UNITECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('473', '3012', 'UTVSOF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('474', '3012', 'VIJAYABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('475', '3012', 'VOLTAS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('476', '3012', 'WELGUJ', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('477', '3012', 'WIPRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('478', '3012', 'WOCKPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('479', '3012', 'YESBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('480', '3012', 'COPPER', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('481', '3012', 'CRUDEOIL', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('482', '3012', 'GOLD', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('483', '3012', 'GOLDM', 'MCX', '100', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('484', '3012', 'LEAD', 'MCX', '1000', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('485', '3012', 'NATURALGAS', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('486', '3012', 'NICKEL', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('487', '3012', 'SILVER', 'MCX', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('488', '3012', 'ZINC', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('733', '3020', 'ABAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('734', '3020', 'ABB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('735', '3020', 'ABGSHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('736', '3020', 'ABIRLANUVO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('737', '3020', 'ACC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('738', '3020', 'ADLABSFILM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('739', '3020', 'ALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('740', '3020', 'ALOKTEXT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('741', '3020', 'AMBUJACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('742', '3020', 'AMTEKAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('743', '3020', 'ANDHRABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('744', '3020', 'APIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('745', '3020', 'APTECHT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('746', '3020', 'ARVIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('747', '3020', 'ASHOKLEY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('748', '3020', 'ASIANPAINT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('749', '3020', 'AUROPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('750', '3020', 'AXISBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('751', '3020', 'BAJAJAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('752', '3020', 'BAJAJHIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('753', '3020', 'BAJAJHLDNG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('754', '3020', 'BALAJITELE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('755', '3020', 'BALLARPUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('756', '3020', 'BALRAMCHIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('757', '3020', 'BANKBARODA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('758', '3020', 'BANKINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('759', '3020', 'BANKNIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('760', '3020', 'BATAINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('761', '3020', 'BEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('762', '3020', 'BEML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('763', '3020', 'BHARATFORG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('764', '3020', 'BHARTIARTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('765', '3020', 'BHEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('766', '3020', 'BHUSANSTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('767', '3020', 'BIOCON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('768', '3020', 'BIRLACORPN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('769', '3020', 'BOMDYEING', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('770', '3020', 'BOSCHLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('771', '3020', 'BPCL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('772', '3020', 'BRFL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('773', '3020', 'CAIRN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('774', '3020', 'CANBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('775', '3020', 'CENTRALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('776', '3020', 'CENTURYTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('777', '3020', 'CESC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('778', '3020', 'CHAMBLFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('779', '3020', 'CHENNPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('780', '3020', 'CIPLA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('781', '3020', 'COLPAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('782', '3020', 'CONCOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('783', '3020', 'CORPBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('784', '3020', 'CROMPGREAV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('785', '3020', 'CUMMINSIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('786', '3020', 'DABUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('787', '3020', 'DCB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('788', '3020', 'DCHL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('789', '3020', 'DENABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('790', '3020', 'DISHTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('791', '3020', 'DIVISLAB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('792', '3020', 'DLF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('793', '3020', 'DRREDDY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('794', '3020', 'EDELWEISS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('795', '3020', 'EDUCOMP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('796', '3020', 'EKC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('797', '3020', 'ESCORTS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('798', '3020', 'ESSAROIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('799', '3020', 'EVERONN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('800', '3020', 'FEDERALBNK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('801', '3020', 'FINANTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('802', '3020', 'FSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('803', '3020', 'GAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('804', '3020', 'GDL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('805', '3020', 'GESHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('806', '3020', 'GITANJALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('807', '3020', 'GLAXO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('808', '3020', 'GMRINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('809', '3020', 'GNFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('810', '3020', 'GRASIM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('811', '3020', 'GSPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('812', '3020', 'GTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('813', '3020', 'GTLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('814', '3020', 'GTOFFSHORE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('815', '3020', 'GUJALKALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('816', '3020', 'GVKPIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('817', '3020', 'HAVELLS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('818', '3020', 'HCC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('819', '3020', 'HCLINSYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('820', '3020', 'HCLTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('821', '3020', 'HDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('822', '3020', 'HDFCBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('823', '3020', 'HDIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('824', '3020', 'HEROHONDA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('825', '3020', 'HINDALCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('826', '3020', 'HINDOILEXP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('827', '3020', 'HINDPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('828', '3020', 'HINDUNILVR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('829', '3020', 'HINDZINC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('830', '3020', 'HOTELEELA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('831', '3020', 'IBREALEST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('832', '3020', 'ICICIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('833', '3020', 'ICSA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('834', '3020', 'IDBI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('835', '3020', 'IDEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('836', '3020', 'IDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('837', '3020', 'IFCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('838', '3020', 'INDHOTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('839', '3020', 'INDIACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('840', '3020', 'INDIAINFO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('841', '3020', 'INDIANB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('842', '3020', 'INDUSINDBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('843', '3020', 'INFOSYSTCH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('844', '3020', 'IOB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('845', '3020', 'IOC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('846', '3020', 'IRB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('847', '3020', 'ISPATIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('848', '3020', 'ITC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('849', '3020', 'IVRCLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('850', '3020', 'JETAIRWAYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('851', '3020', 'JINDALSAW', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('852', '3020', 'JINDALSTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('853', '3020', 'JPASSOCIAT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('854', '3020', 'JPHYDRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('855', '3020', 'JSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('856', '3020', 'JSWSTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('857', '3020', 'KESORAMIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('858', '3020', 'KFA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('859', '3020', 'KOTAKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('860', '3020', 'KSK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('861', '3020', 'KSOILS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('862', '3020', 'KTKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('863', '3020', 'LAXMIMACH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('864', '3020', 'LICHSGFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('865', '3020', 'LITL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('866', '3020', 'LT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('867', '3020', 'LUPIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('868', '3020', 'MAHLIFE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('869', '3020', 'MAHSEAMLES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('870', '3020', 'MARUTI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('871', '3020', 'MCDOWELL_N', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('872', '3020', 'MINDTREE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('873', '3020', 'minifty', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('874', '3020', 'MLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('875', '3020', 'MONNETISPA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('876', '3020', 'MOSERBAER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('877', '3020', 'MPHASIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('878', '3020', 'MRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('879', '3020', 'MRPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('880', '3020', 'MTNL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('881', '3020', 'M_M', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('882', '3020', 'NAGARCONST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('883', '3020', 'NAGARFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('884', '3020', 'NATIONALUM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('885', '3020', 'NBVENTURES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('886', '3020', 'NDTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('887', '3020', 'NETWORK18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('888', '3020', 'NEYVELILIG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('889', '3020', 'NIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('890', '3020', 'NIITLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('891', '3020', 'NOIDATOLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('892', '3020', 'NTPC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('893', '3020', 'OFSS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('894', '3020', 'ONGC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('895', '3020', 'OPTOCIRCUI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('896', '3020', 'ORCHIDCHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('897', '3020', 'ORIENTBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('898', '3020', 'PANTALOONR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('899', '3020', 'PATELENG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('900', '3020', 'PATNI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('901', '3020', 'PENINLAND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('902', '3020', 'PETRONET', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('903', '3020', 'PFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('904', '3020', 'PIRHEALTH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('905', '3020', 'PNB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('906', '3020', 'POLARIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('907', '3020', 'POWERGRID', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('908', '3020', 'PRAJIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('909', '3020', 'PTC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('910', '3020', 'PUNJLLOYD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('911', '3020', 'RAJESHEXPO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('912', '3020', 'RANBAXY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('913', '3020', 'RCOM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('914', '3020', 'RECLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('915', '3020', 'RELCAPITAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('916', '3020', 'RELIANCE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('917', '3020', 'RELINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('918', '3020', 'RENUKA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('919', '3020', 'RIIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('920', '3020', 'RNRL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('921', '3020', 'ROLTA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('922', '3020', 'RPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('923', '3020', 'RPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('924', '3020', 'SAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('925', '3020', 'SBIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('926', '3020', 'SCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('927', '3020', 'SESAGOA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('928', '3020', 'SIEMENS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('929', '3020', 'SINTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('930', '3020', 'SKUMARSYNF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('931', '3020', 'SREINTFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('932', '3020', 'SRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('933', '3020', 'STAR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('934', '3020', 'STER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('935', '3020', 'STERLINBIO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('936', '3020', 'SUNPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('937', '3020', 'SUNTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('938', '3020', 'SUZLON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('939', '3020', 'SYNDIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('940', '3020', 'TATACHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('941', '3020', 'TATACOMM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('942', '3020', 'TATAMOTORS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('943', '3020', 'TATAPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('944', '3020', 'TATASTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('945', '3020', 'TATATEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('946', '3020', 'TCS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('947', '3020', 'TECHM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('948', '3020', 'THERMAX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('949', '3020', 'TITAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('950', '3020', 'TORNTPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('951', '3020', 'TRIVENI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('952', '3020', 'TTML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('953', '3020', 'TULIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('954', '3020', 'TV18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('955', '3020', 'TVSMOTOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('956', '3020', 'UCOBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('957', '3020', 'ULTRACEMCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('958', '3020', 'UNIONBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('959', '3020', 'UNIPHOS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('960', '3020', 'UNITECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('961', '3020', 'UTVSOF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('962', '3020', 'VIJAYABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('963', '3020', 'VOLTAS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('964', '3020', 'WELGUJ', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('965', '3020', 'WIPRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('966', '3020', 'WOCKPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('967', '3020', 'YESBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('968', '3020', 'COPPER', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('969', '3020', 'CRUDEOIL', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('970', '3020', 'GOLD', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('971', '3020', 'GOLDM', 'MCX', '100', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('972', '3020', 'LEAD', 'MCX', '1000', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('973', '3020', 'NATURALGAS', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('974', '3020', 'NICKEL', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('975', '3020', 'SILVER', 'MCX', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('976', '3020', 'ZINC', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('977', '3038', 'ABAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('978', '3038', 'ABB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('979', '3038', 'ABGSHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('980', '3038', 'ABIRLANUVO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('981', '3038', 'ACC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('982', '3038', 'ADLABSFILM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('983', '3038', 'ALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('984', '3038', 'ALOKTEXT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('985', '3038', 'AMBUJACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('986', '3038', 'AMTEKAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('987', '3038', 'ANDHRABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('988', '3038', 'APIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('989', '3038', 'APTECHT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('990', '3038', 'ARVIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('991', '3038', 'ASHOKLEY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('992', '3038', 'ASIANPAINT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('993', '3038', 'AUROPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('994', '3038', 'AXISBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('995', '3038', 'BAJAJAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('996', '3038', 'BAJAJHIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('997', '3038', 'BAJAJHLDNG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('998', '3038', 'BALAJITELE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('999', '3038', 'BALLARPUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1000', '3038', 'BALRAMCHIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1001', '3038', 'BANKBARODA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1002', '3038', 'BANKINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1003', '3038', 'BANKNIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1004', '3038', 'BATAINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1005', '3038', 'BEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1006', '3038', 'BEML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1007', '3038', 'BHARATFORG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1008', '3038', 'BHARTIARTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1009', '3038', 'BHEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1010', '3038', 'BHUSANSTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1011', '3038', 'BIOCON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1012', '3038', 'BIRLACORPN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1013', '3038', 'BOMDYEING', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1014', '3038', 'BOSCHLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1015', '3038', 'BPCL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1016', '3038', 'BRFL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1017', '3038', 'CAIRN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1018', '3038', 'CANBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1019', '3038', 'CENTRALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1020', '3038', 'CENTURYTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1021', '3038', 'CESC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1022', '3038', 'CHAMBLFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1023', '3038', 'CHENNPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1024', '3038', 'CIPLA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1025', '3038', 'COLPAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1026', '3038', 'CONCOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1027', '3038', 'CORPBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1028', '3038', 'CROMPGREAV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1029', '3038', 'CUMMINSIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1030', '3038', 'DABUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1031', '3038', 'DCB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1032', '3038', 'DCHL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1033', '3038', 'DENABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1034', '3038', 'DISHTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1035', '3038', 'DIVISLAB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1036', '3038', 'DLF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1037', '3038', 'DRREDDY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1038', '3038', 'EDELWEISS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1039', '3038', 'EDUCOMP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1040', '3038', 'EKC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1041', '3038', 'ESCORTS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1042', '3038', 'ESSAROIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1043', '3038', 'EVERONN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1044', '3038', 'FEDERALBNK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1045', '3038', 'FINANTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1046', '3038', 'FSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1047', '3038', 'GAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1048', '3038', 'GDL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1049', '3038', 'GESHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1050', '3038', 'GITANJALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1051', '3038', 'GLAXO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1052', '3038', 'GMRINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1053', '3038', 'GNFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1054', '3038', 'GRASIM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1055', '3038', 'GSPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1056', '3038', 'GTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1057', '3038', 'GTLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1058', '3038', 'GTOFFSHORE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1059', '3038', 'GUJALKALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1060', '3038', 'GVKPIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1061', '3038', 'HAVELLS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1062', '3038', 'HCC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1063', '3038', 'HCLINSYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1064', '3038', 'HCLTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1065', '3038', 'HDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1066', '3038', 'HDFCBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1067', '3038', 'HDIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1068', '3038', 'HEROHONDA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1069', '3038', 'HINDALCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1070', '3038', 'HINDOILEXP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1071', '3038', 'HINDPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1072', '3038', 'HINDUNILVR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1073', '3038', 'HINDZINC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1074', '3038', 'HOTELEELA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1075', '3038', 'IBREALEST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1076', '3038', 'ICICIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1077', '3038', 'ICSA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1078', '3038', 'IDBI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1079', '3038', 'IDEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1080', '3038', 'IDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1081', '3038', 'IFCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1082', '3038', 'INDHOTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1083', '3038', 'INDIACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1084', '3038', 'INDIAINFO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1085', '3038', 'INDIANB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1086', '3038', 'INDUSINDBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1087', '3038', 'INFOSYSTCH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1088', '3038', 'IOB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1089', '3038', 'IOC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1090', '3038', 'IRB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1091', '3038', 'ISPATIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1092', '3038', 'ITC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1093', '3038', 'IVRCLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1094', '3038', 'JETAIRWAYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1095', '3038', 'JINDALSAW', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1096', '3038', 'JINDALSTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1097', '3038', 'JPASSOCIAT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1098', '3038', 'JPHYDRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1099', '3038', 'JSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1100', '3038', 'JSWSTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1101', '3038', 'KESORAMIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1102', '3038', 'KFA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1103', '3038', 'KOTAKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1104', '3038', 'KSK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1105', '3038', 'KSOILS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1106', '3038', 'KTKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1107', '3038', 'LAXMIMACH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1108', '3038', 'LICHSGFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1109', '3038', 'LITL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1110', '3038', 'LT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1111', '3038', 'LUPIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1112', '3038', 'MAHLIFE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1113', '3038', 'MAHSEAMLES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1114', '3038', 'MARUTI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1115', '3038', 'MCDOWELL_N', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1116', '3038', 'MINDTREE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1117', '3038', 'minifty', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1118', '3038', 'MLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1119', '3038', 'MONNETISPA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1120', '3038', 'MOSERBAER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1121', '3038', 'MPHASIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1122', '3038', 'MRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1123', '3038', 'MRPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1124', '3038', 'MTNL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1125', '3038', 'M_M', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1126', '3038', 'NAGARCONST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1127', '3038', 'NAGARFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1128', '3038', 'NATIONALUM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1129', '3038', 'NBVENTURES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1130', '3038', 'NDTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1131', '3038', 'NETWORK18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1132', '3038', 'NEYVELILIG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1133', '3038', 'NIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1134', '3038', 'NIITLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1135', '3038', 'NOIDATOLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1136', '3038', 'NTPC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1137', '3038', 'OFSS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1138', '3038', 'ONGC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1139', '3038', 'OPTOCIRCUI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1140', '3038', 'ORCHIDCHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1141', '3038', 'ORIENTBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1142', '3038', 'PANTALOONR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1143', '3038', 'PATELENG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1144', '3038', 'PATNI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1145', '3038', 'PENINLAND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1146', '3038', 'PETRONET', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1147', '3038', 'PFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1148', '3038', 'PIRHEALTH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1149', '3038', 'PNB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1150', '3038', 'POLARIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1151', '3038', 'POWERGRID', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1152', '3038', 'PRAJIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1153', '3038', 'PTC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1154', '3038', 'PUNJLLOYD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1155', '3038', 'RAJESHEXPO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1156', '3038', 'RANBAXY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1157', '3038', 'RCOM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1158', '3038', 'RECLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1159', '3038', 'RELCAPITAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1160', '3038', 'RELIANCE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1161', '3038', 'RELINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1162', '3038', 'RENUKA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1163', '3038', 'RIIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1164', '3038', 'RNRL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1165', '3038', 'ROLTA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1166', '3038', 'RPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1167', '3038', 'RPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1168', '3038', 'SAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1169', '3038', 'SBIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1170', '3038', 'SCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1171', '3038', 'SESAGOA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1172', '3038', 'SIEMENS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1173', '3038', 'SINTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1174', '3038', 'SKUMARSYNF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1175', '3038', 'SREINTFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1176', '3038', 'SRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1177', '3038', 'STAR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1178', '3038', 'STER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1179', '3038', 'STERLINBIO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1180', '3038', 'SUNPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1181', '3038', 'SUNTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1182', '3038', 'SUZLON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1183', '3038', 'SYNDIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1184', '3038', 'TATACHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1185', '3038', 'TATACOMM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1186', '3038', 'TATAMOTORS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1187', '3038', 'TATAPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1188', '3038', 'TATASTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1189', '3038', 'TATATEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1190', '3038', 'TCS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1191', '3038', 'TECHM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1192', '3038', 'THERMAX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1193', '3038', 'TITAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1194', '3038', 'TORNTPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1195', '3038', 'TRIVENI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1196', '3038', 'TTML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1197', '3038', 'TULIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1198', '3038', 'TV18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1199', '3038', 'TVSMOTOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1200', '3038', 'UCOBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1201', '3038', 'ULTRACEMCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1202', '3038', 'UNIONBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1203', '3038', 'UNIPHOS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1204', '3038', 'UNITECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1205', '3038', 'UTVSOF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1206', '3038', 'VIJAYABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1207', '3038', 'VOLTAS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1208', '3038', 'WELGUJ', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1209', '3038', 'WIPRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1210', '3038', 'WOCKPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1211', '3038', 'YESBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1212', '3038', 'COPPER', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1213', '3038', 'CRUDEOIL', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1214', '3038', 'GOLD', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1215', '3038', 'GOLDM', 'MCX', '100', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1216', '3038', 'LEAD', 'MCX', '1000', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1217', '3038', 'NATURALGAS', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1218', '3038', 'NICKEL', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1219', '3038', 'SILVER', 'MCX', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1220', '3038', 'ZINC', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1221', '3091', 'ABAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1222', '3091', 'ABB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1223', '3091', 'ABGSHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1224', '3091', 'ABIRLANUVO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1225', '3091', 'ACC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1226', '3091', 'ADLABSFILM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1227', '3091', 'ALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1228', '3091', 'ALOKTEXT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1229', '3091', 'AMBUJACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1230', '3091', 'AMTEKAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1231', '3091', 'ANDHRABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1232', '3091', 'APIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1233', '3091', 'APTECHT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1234', '3091', 'ARVIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1235', '3091', 'ASHOKLEY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1236', '3091', 'ASIANPAINT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1237', '3091', 'AUROPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1238', '3091', 'AXISBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1239', '3091', 'BAJAJAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1240', '3091', 'BAJAJHIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1241', '3091', 'BAJAJHLDNG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1242', '3091', 'BALAJITELE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1243', '3091', 'BALLARPUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1244', '3091', 'BALRAMCHIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1245', '3091', 'BANKBARODA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1246', '3091', 'BANKINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1247', '3091', 'BANKNIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1248', '3091', 'BATAINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1249', '3091', 'BEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1250', '3091', 'BEML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1251', '3091', 'BHARATFORG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1252', '3091', 'BHARTIARTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1253', '3091', 'BHEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1254', '3091', 'BHUSANSTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1255', '3091', 'BIOCON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1256', '3091', 'BIRLACORPN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1257', '3091', 'BOMDYEING', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1258', '3091', 'BOSCHLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1259', '3091', 'BPCL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1260', '3091', 'BRFL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1261', '3091', 'CAIRN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1262', '3091', 'CANBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1263', '3091', 'CENTRALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1264', '3091', 'CENTURYTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1265', '3091', 'CESC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1266', '3091', 'CHAMBLFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1267', '3091', 'CHENNPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1268', '3091', 'CIPLA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1269', '3091', 'COLPAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1270', '3091', 'CONCOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1271', '3091', 'CORPBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1272', '3091', 'CROMPGREAV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1273', '3091', 'CUMMINSIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1274', '3091', 'DABUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1275', '3091', 'DCB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1276', '3091', 'DCHL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1277', '3091', 'DENABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1278', '3091', 'DISHTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1279', '3091', 'DIVISLAB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1280', '3091', 'DLF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1281', '3091', 'DRREDDY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1282', '3091', 'EDELWEISS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1283', '3091', 'EDUCOMP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1284', '3091', 'EKC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1285', '3091', 'ESCORTS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1286', '3091', 'ESSAROIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1287', '3091', 'EVERONN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1288', '3091', 'FEDERALBNK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1289', '3091', 'FINANTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1290', '3091', 'FSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1291', '3091', 'GAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1292', '3091', 'GDL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1293', '3091', 'GESHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1294', '3091', 'GITANJALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1295', '3091', 'GLAXO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1296', '3091', 'GMRINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1297', '3091', 'GNFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1298', '3091', 'GRASIM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1299', '3091', 'GSPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1300', '3091', 'GTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1301', '3091', 'GTLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1302', '3091', 'GTOFFSHORE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1303', '3091', 'GUJALKALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1304', '3091', 'GVKPIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1305', '3091', 'HAVELLS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1306', '3091', 'HCC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1307', '3091', 'HCLINSYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1308', '3091', 'HCLTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1309', '3091', 'HDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1310', '3091', 'HDFCBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1311', '3091', 'HDIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1312', '3091', 'HEROHONDA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1313', '3091', 'HINDALCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1314', '3091', 'HINDOILEXP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1315', '3091', 'HINDPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1316', '3091', 'HINDUNILVR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1317', '3091', 'HINDZINC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1318', '3091', 'HOTELEELA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1319', '3091', 'IBREALEST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1320', '3091', 'ICICIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1321', '3091', 'ICSA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1322', '3091', 'IDBI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1323', '3091', 'IDEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1324', '3091', 'IDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1325', '3091', 'IFCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1326', '3091', 'INDHOTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1327', '3091', 'INDIACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1328', '3091', 'INDIAINFO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1329', '3091', 'INDIANB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1330', '3091', 'INDUSINDBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1331', '3091', 'INFOSYSTCH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1332', '3091', 'IOB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1333', '3091', 'IOC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1334', '3091', 'IRB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1335', '3091', 'ISPATIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1336', '3091', 'ITC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1337', '3091', 'IVRCLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1338', '3091', 'JETAIRWAYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1339', '3091', 'JINDALSAW', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1340', '3091', 'JINDALSTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1341', '3091', 'JPASSOCIAT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1342', '3091', 'JPHYDRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1343', '3091', 'JSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1344', '3091', 'JSWSTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1345', '3091', 'KESORAMIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1346', '3091', 'KFA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1347', '3091', 'KOTAKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1348', '3091', 'KSK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1349', '3091', 'KSOILS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1350', '3091', 'KTKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1351', '3091', 'LAXMIMACH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1352', '3091', 'LICHSGFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1353', '3091', 'LITL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1354', '3091', 'LT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1355', '3091', 'LUPIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1356', '3091', 'MAHLIFE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1357', '3091', 'MAHSEAMLES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1358', '3091', 'MARUTI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1359', '3091', 'MCDOWELL_N', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1360', '3091', 'MINDTREE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1361', '3091', 'minifty', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1362', '3091', 'MLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1363', '3091', 'MONNETISPA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1364', '3091', 'MOSERBAER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1365', '3091', 'MPHASIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1366', '3091', 'MRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1367', '3091', 'MRPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1368', '3091', 'MTNL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1369', '3091', 'M_M', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1370', '3091', 'NAGARCONST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1371', '3091', 'NAGARFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1372', '3091', 'NATIONALUM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1373', '3091', 'NBVENTURES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1374', '3091', 'NDTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1375', '3091', 'NETWORK18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1376', '3091', 'NEYVELILIG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1377', '3091', 'NIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1378', '3091', 'NIITLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1379', '3091', 'NOIDATOLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1380', '3091', 'NTPC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1381', '3091', 'OFSS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1382', '3091', 'ONGC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1383', '3091', 'OPTOCIRCUI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1384', '3091', 'ORCHIDCHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1385', '3091', 'ORIENTBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1386', '3091', 'PANTALOONR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1387', '3091', 'PATELENG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1388', '3091', 'PATNI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1389', '3091', 'PENINLAND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1390', '3091', 'PETRONET', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1391', '3091', 'PFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1392', '3091', 'PIRHEALTH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1393', '3091', 'PNB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1394', '3091', 'POLARIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1395', '3091', 'POWERGRID', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1396', '3091', 'PRAJIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1397', '3091', 'PTC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1398', '3091', 'PUNJLLOYD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1399', '3091', 'RAJESHEXPO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1400', '3091', 'RANBAXY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1401', '3091', 'RCOM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1402', '3091', 'RECLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1403', '3091', 'RELCAPITAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1404', '3091', 'RELIANCE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1405', '3091', 'RELINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1406', '3091', 'RENUKA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1407', '3091', 'RIIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1408', '3091', 'RNRL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1409', '3091', 'ROLTA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1410', '3091', 'RPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1411', '3091', 'RPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1412', '3091', 'SAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1413', '3091', 'SBIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1414', '3091', 'SCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1415', '3091', 'SESAGOA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1416', '3091', 'SIEMENS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1417', '3091', 'SINTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1418', '3091', 'SKUMARSYNF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1419', '3091', 'SREINTFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1420', '3091', 'SRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1421', '3091', 'STAR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1422', '3091', 'STER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1423', '3091', 'STERLINBIO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1424', '3091', 'SUNPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1425', '3091', 'SUNTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1426', '3091', 'SUZLON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1427', '3091', 'SYNDIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1428', '3091', 'TATACHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1429', '3091', 'TATACOMM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1430', '3091', 'TATAMOTORS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1431', '3091', 'TATAPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1432', '3091', 'TATASTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1433', '3091', 'TATATEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1434', '3091', 'TCS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1435', '3091', 'TECHM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1436', '3091', 'THERMAX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1437', '3091', 'TITAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1438', '3091', 'TORNTPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1439', '3091', 'TRIVENI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1440', '3091', 'TTML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1441', '3091', 'TULIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1442', '3091', 'TV18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1443', '3091', 'TVSMOTOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1444', '3091', 'UCOBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1445', '3091', 'ULTRACEMCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1446', '3091', 'UNIONBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1447', '3091', 'UNIPHOS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1448', '3091', 'UNITECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1449', '3091', 'UTVSOF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1450', '3091', 'VIJAYABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1451', '3091', 'VOLTAS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1452', '3091', 'WELGUJ', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1453', '3091', 'WIPRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1454', '3091', 'WOCKPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1455', '3091', 'YESBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1456', '3091', 'COPPER', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1457', '3091', 'CRUDEOIL', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1458', '3091', 'GOLD', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1459', '3091', 'GOLDM', 'MCX', '100', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1460', '3091', 'LEAD', 'MCX', '1000', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1461', '3091', 'NATURALGAS', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1462', '3091', 'NICKEL', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1463', '3091', 'SILVER', 'MCX', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1464', '3091', 'ZINC', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1465', '3083', 'ABAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1466', '3083', 'ABB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1467', '3083', 'ABGSHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1468', '3083', 'ABIRLANUVO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1469', '3083', 'ACC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1470', '3083', 'ADLABSFILM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1471', '3083', 'ALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1472', '3083', 'ALOKTEXT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1473', '3083', 'AMBUJACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1474', '3083', 'AMTEKAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1475', '3083', 'ANDHRABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1476', '3083', 'APIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1477', '3083', 'APTECHT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1478', '3083', 'ARVIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1479', '3083', 'ASHOKLEY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1480', '3083', 'ASIANPAINT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1481', '3083', 'AUROPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1482', '3083', 'AXISBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1483', '3083', 'BAJAJAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1484', '3083', 'BAJAJHIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1485', '3083', 'BAJAJHLDNG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1486', '3083', 'BALAJITELE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1487', '3083', 'BALLARPUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1488', '3083', 'BALRAMCHIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1489', '3083', 'BANKBARODA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1490', '3083', 'BANKINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1491', '3083', 'BANKNIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1492', '3083', 'BATAINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1493', '3083', 'BEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1494', '3083', 'BEML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1495', '3083', 'BHARATFORG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1496', '3083', 'BHARTIARTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1497', '3083', 'BHEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1498', '3083', 'BHUSANSTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1499', '3083', 'BIOCON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1500', '3083', 'BIRLACORPN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1501', '3083', 'BOMDYEING', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1502', '3083', 'BOSCHLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1503', '3083', 'BPCL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1504', '3083', 'BRFL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1505', '3083', 'CAIRN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1506', '3083', 'CANBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1507', '3083', 'CENTRALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1508', '3083', 'CENTURYTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1509', '3083', 'CESC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1510', '3083', 'CHAMBLFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1511', '3083', 'CHENNPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1512', '3083', 'CIPLA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1513', '3083', 'COLPAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1514', '3083', 'CONCOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1515', '3083', 'CORPBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1516', '3083', 'CROMPGREAV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1517', '3083', 'CUMMINSIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1518', '3083', 'DABUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1519', '3083', 'DCB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1520', '3083', 'DCHL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1521', '3083', 'DENABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1522', '3083', 'DISHTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1523', '3083', 'DIVISLAB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1524', '3083', 'DLF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1525', '3083', 'DRREDDY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1526', '3083', 'EDELWEISS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1527', '3083', 'EDUCOMP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1528', '3083', 'EKC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1529', '3083', 'ESCORTS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1530', '3083', 'ESSAROIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1531', '3083', 'EVERONN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1532', '3083', 'FEDERALBNK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1533', '3083', 'FINANTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1534', '3083', 'FSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1535', '3083', 'GAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1536', '3083', 'GDL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1537', '3083', 'GESHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1538', '3083', 'GITANJALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1539', '3083', 'GLAXO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1540', '3083', 'GMRINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1541', '3083', 'GNFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1542', '3083', 'GRASIM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1543', '3083', 'GSPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1544', '3083', 'GTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1545', '3083', 'GTLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1546', '3083', 'GTOFFSHORE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1547', '3083', 'GUJALKALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1548', '3083', 'GVKPIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1549', '3083', 'HAVELLS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1550', '3083', 'HCC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1551', '3083', 'HCLINSYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1552', '3083', 'HCLTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1553', '3083', 'HDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1554', '3083', 'HDFCBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1555', '3083', 'HDIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1556', '3083', 'HEROHONDA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1557', '3083', 'HINDALCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1558', '3083', 'HINDOILEXP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1559', '3083', 'HINDPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1560', '3083', 'HINDUNILVR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1561', '3083', 'HINDZINC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1562', '3083', 'HOTELEELA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1563', '3083', 'IBREALEST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1564', '3083', 'ICICIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1565', '3083', 'ICSA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1566', '3083', 'IDBI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1567', '3083', 'IDEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1568', '3083', 'IDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1569', '3083', 'IFCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1570', '3083', 'INDHOTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1571', '3083', 'INDIACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1572', '3083', 'INDIAINFO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1573', '3083', 'INDIANB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1574', '3083', 'INDUSINDBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1575', '3083', 'INFOSYSTCH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1576', '3083', 'IOB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1577', '3083', 'IOC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1578', '3083', 'IRB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1579', '3083', 'ISPATIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1580', '3083', 'ITC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1581', '3083', 'IVRCLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1582', '3083', 'JETAIRWAYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1583', '3083', 'JINDALSAW', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1584', '3083', 'JINDALSTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1585', '3083', 'JPASSOCIAT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1586', '3083', 'JPHYDRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1587', '3083', 'JSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1588', '3083', 'JSWSTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1589', '3083', 'KESORAMIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1590', '3083', 'KFA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1591', '3083', 'KOTAKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1592', '3083', 'KSK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1593', '3083', 'KSOILS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1594', '3083', 'KTKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1595', '3083', 'LAXMIMACH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1596', '3083', 'LICHSGFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1597', '3083', 'LITL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1598', '3083', 'LT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1599', '3083', 'LUPIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1600', '3083', 'MAHLIFE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1601', '3083', 'MAHSEAMLES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1602', '3083', 'MARUTI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1603', '3083', 'MCDOWELL_N', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1604', '3083', 'MINDTREE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1605', '3083', 'minifty', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1606', '3083', 'MLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1607', '3083', 'MONNETISPA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1608', '3083', 'MOSERBAER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1609', '3083', 'MPHASIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1610', '3083', 'MRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1611', '3083', 'MRPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1612', '3083', 'MTNL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1613', '3083', 'M_M', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1614', '3083', 'NAGARCONST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1615', '3083', 'NAGARFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1616', '3083', 'NATIONALUM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1617', '3083', 'NBVENTURES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1618', '3083', 'NDTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1619', '3083', 'NETWORK18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1620', '3083', 'NEYVELILIG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1621', '3083', 'NIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1622', '3083', 'NIITLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1623', '3083', 'NOIDATOLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1624', '3083', 'NTPC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1625', '3083', 'OFSS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1626', '3083', 'ONGC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1627', '3083', 'OPTOCIRCUI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1628', '3083', 'ORCHIDCHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1629', '3083', 'ORIENTBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1630', '3083', 'PANTALOONR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1631', '3083', 'PATELENG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1632', '3083', 'PATNI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1633', '3083', 'PENINLAND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1634', '3083', 'PETRONET', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1635', '3083', 'PFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1636', '3083', 'PIRHEALTH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1637', '3083', 'PNB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1638', '3083', 'POLARIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1639', '3083', 'POWERGRID', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1640', '3083', 'PRAJIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1641', '3083', 'PTC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1642', '3083', 'PUNJLLOYD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1643', '3083', 'RAJESHEXPO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1644', '3083', 'RANBAXY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1645', '3083', 'RCOM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1646', '3083', 'RECLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1647', '3083', 'RELCAPITAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1648', '3083', 'RELIANCE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1649', '3083', 'RELINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1650', '3083', 'RENUKA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1651', '3083', 'RIIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1652', '3083', 'RNRL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1653', '3083', 'ROLTA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1654', '3083', 'RPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1655', '3083', 'RPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1656', '3083', 'SAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1657', '3083', 'SBIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1658', '3083', 'SCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1659', '3083', 'SESAGOA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1660', '3083', 'SIEMENS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1661', '3083', 'SINTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1662', '3083', 'SKUMARSYNF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1663', '3083', 'SREINTFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1664', '3083', 'SRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1665', '3083', 'STAR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1666', '3083', 'STER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1667', '3083', 'STERLINBIO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1668', '3083', 'SUNPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1669', '3083', 'SUNTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1670', '3083', 'SUZLON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1671', '3083', 'SYNDIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1672', '3083', 'TATACHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1673', '3083', 'TATACOMM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1674', '3083', 'TATAMOTORS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1675', '3083', 'TATAPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1676', '3083', 'TATASTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1677', '3083', 'TATATEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1678', '3083', 'TCS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1679', '3083', 'TECHM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1680', '3083', 'THERMAX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1681', '3083', 'TITAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1682', '3083', 'TORNTPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1683', '3083', 'TRIVENI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1684', '3083', 'TTML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1685', '3083', 'TULIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1686', '3083', 'TV18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1687', '3083', 'TVSMOTOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1688', '3083', 'UCOBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1689', '3083', 'ULTRACEMCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1690', '3083', 'UNIONBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1691', '3083', 'UNIPHOS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1692', '3083', 'UNITECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1693', '3083', 'UTVSOF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1694', '3083', 'VIJAYABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1695', '3083', 'VOLTAS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1696', '3083', 'WELGUJ', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1697', '3083', 'WIPRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1698', '3083', 'WOCKPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1699', '3083', 'YESBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1700', '3083', 'COPPER', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1701', '3083', 'CRUDEOIL', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1702', '3083', 'GOLD', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1703', '3083', 'GOLDM', 'MCX', '100', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1704', '3083', 'LEAD', 'MCX', '1000', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1705', '3083', 'NATURALGAS', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1706', '3083', 'NICKEL', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1707', '3083', 'SILVER', 'MCX', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1708', '3083', 'ZINC', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1709', '3042', 'ABAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1710', '3042', 'ABB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1711', '3042', 'ABGSHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1712', '3042', 'ABIRLANUVO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1713', '3042', 'ACC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1714', '3042', 'ADLABSFILM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1715', '3042', 'ALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1716', '3042', 'ALOKTEXT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1717', '3042', 'AMBUJACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1718', '3042', 'AMTEKAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1719', '3042', 'ANDHRABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1720', '3042', 'APIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1721', '3042', 'APTECHT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1722', '3042', 'ARVIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1723', '3042', 'ASHOKLEY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1724', '3042', 'ASIANPAINT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1725', '3042', 'AUROPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1726', '3042', 'AXISBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1727', '3042', 'BAJAJAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1728', '3042', 'BAJAJHIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1729', '3042', 'BAJAJHLDNG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1730', '3042', 'BALAJITELE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1731', '3042', 'BALLARPUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1732', '3042', 'BALRAMCHIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1733', '3042', 'BANKBARODA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1734', '3042', 'BANKINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1735', '3042', 'BANKNIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1736', '3042', 'BATAINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1737', '3042', 'BEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1738', '3042', 'BEML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1739', '3042', 'BHARATFORG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1740', '3042', 'BHARTIARTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1741', '3042', 'BHEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1742', '3042', 'BHUSANSTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1743', '3042', 'BIOCON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1744', '3042', 'BIRLACORPN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1745', '3042', 'BOMDYEING', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1746', '3042', 'BOSCHLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1747', '3042', 'BPCL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1748', '3042', 'BRFL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1749', '3042', 'CAIRN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1750', '3042', 'CANBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1751', '3042', 'CENTRALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1752', '3042', 'CENTURYTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1753', '3042', 'CESC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1754', '3042', 'CHAMBLFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1755', '3042', 'CHENNPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1756', '3042', 'CIPLA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1757', '3042', 'COLPAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1758', '3042', 'CONCOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1759', '3042', 'CORPBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1760', '3042', 'CROMPGREAV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1761', '3042', 'CUMMINSIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1762', '3042', 'DABUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1763', '3042', 'DCB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1764', '3042', 'DCHL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1765', '3042', 'DENABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1766', '3042', 'DISHTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1767', '3042', 'DIVISLAB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1768', '3042', 'DLF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1769', '3042', 'DRREDDY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1770', '3042', 'EDELWEISS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1771', '3042', 'EDUCOMP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1772', '3042', 'EKC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1773', '3042', 'ESCORTS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1774', '3042', 'ESSAROIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1775', '3042', 'EVERONN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1776', '3042', 'FEDERALBNK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1777', '3042', 'FINANTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1778', '3042', 'FSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1779', '3042', 'GAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1780', '3042', 'GDL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1781', '3042', 'GESHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1782', '3042', 'GITANJALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1783', '3042', 'GLAXO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1784', '3042', 'GMRINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1785', '3042', 'GNFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1786', '3042', 'GRASIM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1787', '3042', 'GSPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1788', '3042', 'GTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1789', '3042', 'GTLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1790', '3042', 'GTOFFSHORE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1791', '3042', 'GUJALKALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1792', '3042', 'GVKPIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1793', '3042', 'HAVELLS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1794', '3042', 'HCC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1795', '3042', 'HCLINSYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1796', '3042', 'HCLTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1797', '3042', 'HDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1798', '3042', 'HDFCBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1799', '3042', 'HDIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1800', '3042', 'HEROHONDA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1801', '3042', 'HINDALCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1802', '3042', 'HINDOILEXP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1803', '3042', 'HINDPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1804', '3042', 'HINDUNILVR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1805', '3042', 'HINDZINC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1806', '3042', 'HOTELEELA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1807', '3042', 'IBREALEST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1808', '3042', 'ICICIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1809', '3042', 'ICSA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1810', '3042', 'IDBI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1811', '3042', 'IDEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1812', '3042', 'IDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1813', '3042', 'IFCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1814', '3042', 'INDHOTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1815', '3042', 'INDIACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1816', '3042', 'INDIAINFO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1817', '3042', 'INDIANB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1818', '3042', 'INDUSINDBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1819', '3042', 'INFOSYSTCH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1820', '3042', 'IOB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1821', '3042', 'IOC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1822', '3042', 'IRB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1823', '3042', 'ISPATIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1824', '3042', 'ITC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1825', '3042', 'IVRCLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1826', '3042', 'JETAIRWAYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1827', '3042', 'JINDALSAW', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1828', '3042', 'JINDALSTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1829', '3042', 'JPASSOCIAT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1830', '3042', 'JPHYDRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1831', '3042', 'JSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1832', '3042', 'JSWSTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1833', '3042', 'KESORAMIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1834', '3042', 'KFA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1835', '3042', 'KOTAKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1836', '3042', 'KSK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1837', '3042', 'KSOILS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1838', '3042', 'KTKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1839', '3042', 'LAXMIMACH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1840', '3042', 'LICHSGFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1841', '3042', 'LITL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1842', '3042', 'LT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1843', '3042', 'LUPIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1844', '3042', 'MAHLIFE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1845', '3042', 'MAHSEAMLES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1846', '3042', 'MARUTI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1847', '3042', 'MCDOWELL_N', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1848', '3042', 'MINDTREE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1849', '3042', 'minifty', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1850', '3042', 'MLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1851', '3042', 'MONNETISPA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1852', '3042', 'MOSERBAER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1853', '3042', 'MPHASIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1854', '3042', 'MRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1855', '3042', 'MRPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1856', '3042', 'MTNL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1857', '3042', 'M_M', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1858', '3042', 'NAGARCONST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1859', '3042', 'NAGARFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1860', '3042', 'NATIONALUM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1861', '3042', 'NBVENTURES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1862', '3042', 'NDTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1863', '3042', 'NETWORK18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1864', '3042', 'NEYVELILIG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1865', '3042', 'NIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1866', '3042', 'NIITLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1867', '3042', 'NOIDATOLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1868', '3042', 'NTPC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1869', '3042', 'OFSS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1870', '3042', 'ONGC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1871', '3042', 'OPTOCIRCUI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1872', '3042', 'ORCHIDCHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1873', '3042', 'ORIENTBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1874', '3042', 'PANTALOONR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1875', '3042', 'PATELENG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1876', '3042', 'PATNI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1877', '3042', 'PENINLAND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1878', '3042', 'PETRONET', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1879', '3042', 'PFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1880', '3042', 'PIRHEALTH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1881', '3042', 'PNB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1882', '3042', 'POLARIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1883', '3042', 'POWERGRID', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1884', '3042', 'PRAJIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1885', '3042', 'PTC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1886', '3042', 'PUNJLLOYD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1887', '3042', 'RAJESHEXPO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1888', '3042', 'RANBAXY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1889', '3042', 'RCOM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1890', '3042', 'RECLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1891', '3042', 'RELCAPITAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1892', '3042', 'RELIANCE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1893', '3042', 'RELINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1894', '3042', 'RENUKA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1895', '3042', 'RIIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1896', '3042', 'RNRL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1897', '3042', 'ROLTA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1898', '3042', 'RPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1899', '3042', 'RPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1900', '3042', 'SAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1901', '3042', 'SBIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1902', '3042', 'SCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1903', '3042', 'SESAGOA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1904', '3042', 'SIEMENS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1905', '3042', 'SINTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1906', '3042', 'SKUMARSYNF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1907', '3042', 'SREINTFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1908', '3042', 'SRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1909', '3042', 'STAR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1910', '3042', 'STER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1911', '3042', 'STERLINBIO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1912', '3042', 'SUNPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1913', '3042', 'SUNTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1914', '3042', 'SUZLON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1915', '3042', 'SYNDIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1916', '3042', 'TATACHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1917', '3042', 'TATACOMM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1918', '3042', 'TATAMOTORS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1919', '3042', 'TATAPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1920', '3042', 'TATASTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1921', '3042', 'TATATEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1922', '3042', 'TCS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1923', '3042', 'TECHM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1924', '3042', 'THERMAX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1925', '3042', 'TITAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1926', '3042', 'TORNTPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1927', '3042', 'TRIVENI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1928', '3042', 'TTML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1929', '3042', 'TULIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1930', '3042', 'TV18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1931', '3042', 'TVSMOTOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1932', '3042', 'UCOBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1933', '3042', 'ULTRACEMCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1934', '3042', 'UNIONBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1935', '3042', 'UNIPHOS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1936', '3042', 'UNITECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1937', '3042', 'UTVSOF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1938', '3042', 'VIJAYABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1939', '3042', 'VOLTAS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1940', '3042', 'WELGUJ', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1941', '3042', 'WIPRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1942', '3042', 'WOCKPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1943', '3042', 'YESBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1944', '3042', 'COPPER', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1945', '3042', 'CRUDEOIL', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1946', '3042', 'GOLD', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1947', '3042', 'GOLDM', 'MCX', '100', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1948', '3042', 'LEAD', 'MCX', '1000', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1949', '3042', 'NATURALGAS', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1950', '3042', 'NICKEL', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1951', '3042', 'SILVER', 'MCX', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1952', '3042', 'ZINC', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1953', '10006', 'ABAN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1954', '10006', 'ABB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1955', '10006', 'ABGSHIP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1956', '10006', 'ABIRLANUVO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1957', '10006', 'ACC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1958', '10006', 'ADLABSFILM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1959', '10006', 'ALBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1960', '10006', 'ALOKTEXT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1961', '10006', 'AMBUJACEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1962', '10006', 'AMTEKAUTO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1963', '10006', 'ANDHRABANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1964', '10006', 'APIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1965', '10006', 'APTECHT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1966', '10006', 'ARVIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1967', '10006', 'ASHOKLEY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1968', '10006', 'ASIANPAINT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1969', '10006', 'AUROPHARMA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1970', '10006', 'AXISBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1971', '10006', 'BAJAJAUTO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1972', '10006', 'BAJAJHIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1973', '10006', 'BAJAJHLDNG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1974', '10006', 'BALAJITELE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1975', '10006', 'BALLARPUR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1976', '10006', 'BALRAMCHIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1977', '10006', 'BANKBARODA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1978', '10006', 'BANKINDIA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1979', '10006', 'BANKNIFTY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1980', '10006', 'BATAINDIA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1981', '10006', 'BEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1982', '10006', 'BEML', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1983', '10006', 'BHARATFORG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1984', '10006', 'BHARTIARTL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1985', '10006', 'BHEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1986', '10006', 'BHUSANSTL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1987', '10006', 'BIOCON', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1988', '10006', 'BIRLACORPN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1989', '10006', 'BOMDYEING', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1990', '10006', 'BOSCHLTD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1991', '10006', 'BPCL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1992', '10006', 'BRFL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1993', '10006', 'CAIRN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1994', '10006', 'CANBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1995', '10006', 'CENTRALBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1996', '10006', 'CENTURYTEX', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1997', '10006', 'CESC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1998', '10006', 'CHAMBLFERT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('1999', '10006', 'CHENNPETRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2000', '10006', 'CIPLA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2001', '10006', 'COLPAL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2002', '10006', 'CONCOR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2003', '10006', 'CORPBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2004', '10006', 'CROMPGREAV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2005', '10006', 'CUMMINSIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2006', '10006', 'DABUR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2007', '10006', 'DCB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2008', '10006', 'DCHL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2009', '10006', 'DENABANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2010', '10006', 'DISHTV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2011', '10006', 'DIVISLAB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2012', '10006', 'DLF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2013', '10006', 'DRREDDY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2014', '10006', 'EDELWEISS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2015', '10006', 'EDUCOMP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2016', '10006', 'EKC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2017', '10006', 'ESCORTS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2018', '10006', 'ESSAROIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2019', '10006', 'EVERONN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2020', '10006', 'FEDERALBNK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2021', '10006', 'FINANTECH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2022', '10006', 'FSL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2023', '10006', 'GAIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2024', '10006', 'GDL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2025', '10006', 'GESHIP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2026', '10006', 'GITANJALI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2027', '10006', 'GLAXO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2028', '10006', 'GMRINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2029', '10006', 'GNFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2030', '10006', 'GRASIM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2031', '10006', 'GSPL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2032', '10006', 'GTL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2033', '10006', 'GTLINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2034', '10006', 'GTOFFSHORE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2035', '10006', 'GUJALKALI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2036', '10006', 'GVKPIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2037', '10006', 'HAVELLS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2038', '10006', 'HCC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2039', '10006', 'HCLINSYS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2040', '10006', 'HCLTECH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2041', '10006', 'HDFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2042', '10006', 'HDFCBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2043', '10006', 'HDIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2044', '10006', 'HEROHONDA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2045', '10006', 'HINDALCO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2046', '10006', 'HINDOILEXP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2047', '10006', 'HINDPETRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2048', '10006', 'HINDUNILVR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2049', '10006', 'HINDZINC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2050', '10006', 'HOTELEELA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2051', '10006', 'IBREALEST', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2052', '10006', 'ICICIBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2053', '10006', 'ICSA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2054', '10006', 'IDBI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2055', '10006', 'IDEA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2056', '10006', 'IDFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2057', '10006', 'IFCI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2058', '10006', 'INDHOTEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2059', '10006', 'INDIACEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2060', '10006', 'INDIAINFO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2061', '10006', 'INDIANB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2062', '10006', 'INDUSINDBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2063', '10006', 'INFOSYSTCH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2064', '10006', 'IOB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2065', '10006', 'IOC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2066', '10006', 'IRB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2067', '10006', 'ISPATIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2068', '10006', 'ITC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2069', '10006', 'IVRCLINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2070', '10006', 'JETAIRWAYS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2071', '10006', 'JINDALSAW', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2072', '10006', 'JINDALSTEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2073', '10006', 'JPASSOCIAT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2074', '10006', 'JPHYDRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2075', '10006', 'JSL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2076', '10006', 'JSWSTEEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2077', '10006', 'KESORAMIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2078', '10006', 'KFA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2079', '10006', 'KOTAKBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2080', '10006', 'KSK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2081', '10006', 'KSOILS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2082', '10006', 'KTKBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2083', '10006', 'LAXMIMACH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2084', '10006', 'LICHSGFIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2085', '10006', 'LITL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2086', '10006', 'LT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2087', '10006', 'LUPIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2088', '10006', 'MAHLIFE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2089', '10006', 'MAHSEAMLES', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2090', '10006', 'MARUTI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2091', '10006', 'MCDOWELL_N', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2092', '10006', 'MINDTREE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2093', '10006', 'minifty', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2094', '10006', 'MLL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2095', '10006', 'MONNETISPA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2096', '10006', 'MOSERBAER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2097', '10006', 'MPHASIS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2098', '10006', 'MRF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2099', '10006', 'MRPL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2100', '10006', 'MTNL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2101', '10006', 'M_M', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2102', '10006', 'NAGARCONST', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2103', '10006', 'NAGARFERT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2104', '10006', 'NATIONALUM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2105', '10006', 'NBVENTURES', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2106', '10006', 'NDTV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2107', '10006', 'NETWORK18', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2108', '10006', 'NEYVELILIG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2109', '10006', 'NIFTY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2110', '10006', 'NIITLTD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2111', '10006', 'NOIDATOLL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2112', '10006', 'NTPC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2113', '10006', 'OFSS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2114', '10006', 'ONGC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2115', '10006', 'OPTOCIRCUI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2116', '10006', 'ORCHIDCHEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2117', '10006', 'ORIENTBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2118', '10006', 'PANTALOONR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2119', '10006', 'PATELENG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2120', '10006', 'PATNI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2121', '10006', 'PENINLAND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2122', '10006', 'PETRONET', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2123', '10006', 'PFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2124', '10006', 'PIRHEALTH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2125', '10006', 'PNB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2126', '10006', 'POLARIS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2127', '10006', 'POWERGRID', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2128', '10006', 'PRAJIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2129', '10006', 'PTC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2130', '10006', 'PUNJLLOYD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2131', '10006', 'RAJESHEXPO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2132', '10006', 'RANBAXY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2133', '10006', 'RCOM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2134', '10006', 'RECLTD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2135', '10006', 'RELCAPITAL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2136', '10006', 'RELIANCE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2137', '10006', 'RELINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2138', '10006', 'RENUKA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2139', '10006', 'RIIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2140', '10006', 'RNRL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2141', '10006', 'ROLTA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2142', '10006', 'RPL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2143', '10006', 'RPOWER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2144', '10006', 'SAIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2145', '10006', 'SBIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2146', '10006', 'SCI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2147', '10006', 'SESAGOA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2148', '10006', 'SIEMENS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2149', '10006', 'SINTEX', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2150', '10006', 'SKUMARSYNF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2151', '10006', 'SREINTFIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2152', '10006', 'SRF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2153', '10006', 'STAR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2154', '10006', 'STER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2155', '10006', 'STERLINBIO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2156', '10006', 'SUNPHARMA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2157', '10006', 'SUNTV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2158', '10006', 'SUZLON', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2159', '10006', 'SYNDIBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2160', '10006', 'TATACHEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2161', '10006', 'TATACOMM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2162', '10006', 'TATAMOTORS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2163', '10006', 'TATAPOWER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2164', '10006', 'TATASTEEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2165', '10006', 'TATATEA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2166', '10006', 'TCS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2167', '10006', 'TECHM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2168', '10006', 'THERMAX', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2169', '10006', 'TITAN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2170', '10006', 'TORNTPOWER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2171', '10006', 'TRIVENI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2172', '10006', 'TTML', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2173', '10006', 'TULIP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2174', '10006', 'TV18', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2175', '10006', 'TVSMOTOR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2176', '10006', 'UCOBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2177', '10006', 'ULTRACEMCO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2178', '10006', 'UNIONBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2179', '10006', 'UNIPHOS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2180', '10006', 'UNITECH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2181', '10006', 'UTVSOF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2182', '10006', 'VIJAYABANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2183', '10006', 'VOLTAS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2184', '10006', 'WELGUJ', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2185', '10006', 'WIPRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2186', '10006', 'WOCKPHARMA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2187', '10006', 'YESBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2188', '10006', 'COPPER', 'MCX', '500', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2189', '10006', 'CRUDEOIL', 'MCX', '500', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2190', '10006', 'GOLD', 'MCX', '500', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2191', '10006', 'GOLDM', 'MCX', '100', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2192', '10006', 'LEAD', 'MCX', '1000', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2193', '10006', 'NATURALGAS', 'MCX', '0', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2194', '10006', 'NICKEL', 'MCX', '0', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2195', '10006', 'SILVER', 'MCX', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2196', '10006', 'ZINC', 'MCX', '0', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2197', '3095', 'ABAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2198', '3095', 'ABB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2199', '3095', 'ABGSHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2200', '3095', 'ABIRLANUVO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2201', '3095', 'ACC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2202', '3095', 'ADLABSFILM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2203', '3095', 'ALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2204', '3095', 'ALOKTEXT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2205', '3095', 'AMBUJACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2206', '3095', 'AMTEKAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2207', '3095', 'ANDHRABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2208', '3095', 'APIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2209', '3095', 'APTECHT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2210', '3095', 'ARVIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2211', '3095', 'ASHOKLEY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2212', '3095', 'ASIANPAINT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2213', '3095', 'AUROPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2214', '3095', 'AXISBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2215', '3095', 'BAJAJAUTO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2216', '3095', 'BAJAJHIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2217', '3095', 'BAJAJHLDNG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2218', '3095', 'BALAJITELE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2219', '3095', 'BALLARPUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2220', '3095', 'BALRAMCHIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2221', '3095', 'BANKBARODA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2222', '3095', 'BANKINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2223', '3095', 'BANKNIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2224', '3095', 'BATAINDIA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2225', '3095', 'BEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2226', '3095', 'BEML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2227', '3095', 'BHARATFORG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2228', '3095', 'BHARTIARTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2229', '3095', 'BHEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2230', '3095', 'BHUSANSTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2231', '3095', 'BIOCON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2232', '3095', 'BIRLACORPN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2233', '3095', 'BOMDYEING', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2234', '3095', 'BOSCHLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2235', '3095', 'BPCL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2236', '3095', 'BRFL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2237', '3095', 'CAIRN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2238', '3095', 'CANBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2239', '3095', 'CENTRALBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2240', '3095', 'CENTURYTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2241', '3095', 'CESC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2242', '3095', 'CHAMBLFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2243', '3095', 'CHENNPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2244', '3095', 'CIPLA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2245', '3095', 'COLPAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2246', '3095', 'CONCOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2247', '3095', 'CORPBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2248', '3095', 'CROMPGREAV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2249', '3095', 'CUMMINSIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2250', '3095', 'DABUR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2251', '3095', 'DCB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2252', '3095', 'DCHL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2253', '3095', 'DENABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2254', '3095', 'DISHTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2255', '3095', 'DIVISLAB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2256', '3095', 'DLF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2257', '3095', 'DRREDDY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2258', '3095', 'EDELWEISS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2259', '3095', 'EDUCOMP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2260', '3095', 'EKC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2261', '3095', 'ESCORTS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2262', '3095', 'ESSAROIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2263', '3095', 'EVERONN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2264', '3095', 'FEDERALBNK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2265', '3095', 'FINANTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2266', '3095', 'FSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2267', '3095', 'GAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2268', '3095', 'GDL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2269', '3095', 'GESHIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2270', '3095', 'GITANJALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2271', '3095', 'GLAXO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2272', '3095', 'GMRINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2273', '3095', 'GNFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2274', '3095', 'GRASIM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2275', '3095', 'GSPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2276', '3095', 'GTL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2277', '3095', 'GTLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2278', '3095', 'GTOFFSHORE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2279', '3095', 'GUJALKALI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2280', '3095', 'GVKPIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2281', '3095', 'HAVELLS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2282', '3095', 'HCC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2283', '3095', 'HCLINSYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2284', '3095', 'HCLTECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2285', '3095', 'HDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2286', '3095', 'HDFCBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2287', '3095', 'HDIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2288', '3095', 'HEROHONDA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2289', '3095', 'HINDALCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2290', '3095', 'HINDOILEXP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2291', '3095', 'HINDPETRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2292', '3095', 'HINDUNILVR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2293', '3095', 'HINDZINC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2294', '3095', 'HOTELEELA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2295', '3095', 'IBREALEST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2296', '3095', 'ICICIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2297', '3095', 'ICSA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2298', '3095', 'IDBI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2299', '3095', 'IDEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2300', '3095', 'IDFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2301', '3095', 'IFCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2302', '3095', 'INDHOTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2303', '3095', 'INDIACEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2304', '3095', 'INDIAINFO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2305', '3095', 'INDIANB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2306', '3095', 'INDUSINDBK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2307', '3095', 'INFOSYSTCH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2308', '3095', 'IOB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2309', '3095', 'IOC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2310', '3095', 'IRB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2311', '3095', 'ISPATIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2312', '3095', 'ITC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2313', '3095', 'IVRCLINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2314', '3095', 'JETAIRWAYS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2315', '3095', 'JINDALSAW', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2316', '3095', 'JINDALSTEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2317', '3095', 'JPASSOCIAT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2318', '3095', 'JPHYDRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2319', '3095', 'JSL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2320', '3095', 'JSWSTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2321', '3095', 'KESORAMIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2322', '3095', 'KFA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2323', '3095', 'KOTAKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2324', '3095', 'KSK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2325', '3095', 'KSOILS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2326', '3095', 'KTKBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2327', '3095', 'LAXMIMACH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2328', '3095', 'LICHSGFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2329', '3095', 'LITL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2330', '3095', 'LT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2331', '3095', 'LUPIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2332', '3095', 'MAHLIFE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2333', '3095', 'MAHSEAMLES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2334', '3095', 'MARUTI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2335', '3095', 'MCDOWELL_N', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2336', '3095', 'MINDTREE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2337', '3095', 'minifty', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2338', '3095', 'MLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2339', '3095', 'MONNETISPA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2340', '3095', 'MOSERBAER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2341', '3095', 'MPHASIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2342', '3095', 'MRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2343', '3095', 'MRPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2344', '3095', 'MTNL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2345', '3095', 'M_M', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2346', '3095', 'NAGARCONST', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2347', '3095', 'NAGARFERT', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2348', '3095', 'NATIONALUM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2349', '3095', 'NBVENTURES', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2350', '3095', 'NDTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2351', '3095', 'NETWORK18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2352', '3095', 'NEYVELILIG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2353', '3095', 'NIFTY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2354', '3095', 'NIITLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2355', '3095', 'NOIDATOLL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2356', '3095', 'NTPC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2357', '3095', 'OFSS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2358', '3095', 'ONGC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2359', '3095', 'OPTOCIRCUI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2360', '3095', 'ORCHIDCHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2361', '3095', 'ORIENTBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2362', '3095', 'PANTALOONR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2363', '3095', 'PATELENG', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2364', '3095', 'PATNI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2365', '3095', 'PENINLAND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2366', '3095', 'PETRONET', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2367', '3095', 'PFC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2368', '3095', 'PIRHEALTH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2369', '3095', 'PNB', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2370', '3095', 'POLARIS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2371', '3095', 'POWERGRID', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2372', '3095', 'PRAJIND', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2373', '3095', 'PTC', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2374', '3095', 'PUNJLLOYD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2375', '3095', 'RAJESHEXPO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2376', '3095', 'RANBAXY', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2377', '3095', 'RCOM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2378', '3095', 'RECLTD', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2379', '3095', 'RELCAPITAL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2380', '3095', 'RELIANCE', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2381', '3095', 'RELINFRA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2382', '3095', 'RENUKA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2383', '3095', 'RIIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2384', '3095', 'RNRL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2385', '3095', 'ROLTA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2386', '3095', 'RPL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2387', '3095', 'RPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2388', '3095', 'SAIL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2389', '3095', 'SBIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2390', '3095', 'SCI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2391', '3095', 'SESAGOA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2392', '3095', 'SIEMENS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2393', '3095', 'SINTEX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2394', '3095', 'SKUMARSYNF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2395', '3095', 'SREINTFIN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2396', '3095', 'SRF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2397', '3095', 'STAR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2398', '3095', 'STER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2399', '3095', 'STERLINBIO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2400', '3095', 'SUNPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2401', '3095', 'SUNTV', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2402', '3095', 'SUZLON', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2403', '3095', 'SYNDIBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2404', '3095', 'TATACHEM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2405', '3095', 'TATACOMM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2406', '3095', 'TATAMOTORS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2407', '3095', 'TATAPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2408', '3095', 'TATASTEEL', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2409', '3095', 'TATATEA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2410', '3095', 'TCS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2411', '3095', 'TECHM', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2412', '3095', 'THERMAX', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2413', '3095', 'TITAN', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2414', '3095', 'TORNTPOWER', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2415', '3095', 'TRIVENI', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2416', '3095', 'TTML', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2417', '3095', 'TULIP', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2418', '3095', 'TV18', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2419', '3095', 'TVSMOTOR', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2420', '3095', 'UCOBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2421', '3095', 'ULTRACEMCO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2422', '3095', 'UNIONBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2423', '3095', 'UNIPHOS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2424', '3095', 'UNITECH', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2425', '3095', 'UTVSOF', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2426', '3095', 'VIJAYABANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2427', '3095', 'VOLTAS', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2428', '3095', 'WELGUJ', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2429', '3095', 'WIPRO', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2430', '3095', 'WOCKPHARMA', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2431', '3095', 'YESBANK', 'F_O', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2432', '3095', 'COPPER', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2433', '3095', 'CRUDEOIL', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2434', '3095', 'GOLD', 'MCX', '500', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2435', '3095', 'GOLDM', 'MCX', '100', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2436', '3095', 'LEAD', 'MCX', '1000', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2437', '3095', 'NATURALGAS', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2438', '3095', 'NICKEL', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2439', '3095', 'SILVER', 'MCX', '150', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2440', '3095', 'ZINC', 'MCX', '0', '0', '0');
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2441', '10008', 'ABAN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2442', '10008', 'ABB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2443', '10008', 'ABGSHIP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2444', '10008', 'ABIRLANUVO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2445', '10008', 'ACC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2446', '10008', 'ADLABSFILM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2447', '10008', 'ALBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2448', '10008', 'ALOKTEXT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2449', '10008', 'AMBUJACEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2450', '10008', 'AMTEKAUTO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2451', '10008', 'ANDHRABANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2452', '10008', 'APIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2453', '10008', 'APTECHT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2454', '10008', 'ARVIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2455', '10008', 'ASHOKLEY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2456', '10008', 'ASIANPAINT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2457', '10008', 'AUROPHARMA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2458', '10008', 'AXISBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2459', '10008', 'BAJAJAUTO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2460', '10008', 'BAJAJHIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2461', '10008', 'BAJAJHLDNG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2462', '10008', 'BALAJITELE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2463', '10008', 'BALLARPUR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2464', '10008', 'BALRAMCHIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2465', '10008', 'BANKBARODA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2466', '10008', 'BANKINDIA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2467', '10008', 'BANKNIFTY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2468', '10008', 'BATAINDIA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2469', '10008', 'BEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2470', '10008', 'BEML', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2471', '10008', 'BHARATFORG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2472', '10008', 'BHARTIARTL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2473', '10008', 'BHEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2474', '10008', 'BHUSANSTL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2475', '10008', 'BIOCON', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2476', '10008', 'BIRLACORPN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2477', '10008', 'BOMDYEING', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2478', '10008', 'BOSCHLTD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2479', '10008', 'BPCL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2480', '10008', 'BRFL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2481', '10008', 'CAIRN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2482', '10008', 'CANBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2483', '10008', 'CENTRALBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2484', '10008', 'CENTURYTEX', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2485', '10008', 'CESC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2486', '10008', 'CHAMBLFERT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2487', '10008', 'CHENNPETRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2488', '10008', 'CIPLA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2489', '10008', 'COLPAL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2490', '10008', 'CONCOR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2491', '10008', 'CORPBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2492', '10008', 'CROMPGREAV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2493', '10008', 'CUMMINSIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2494', '10008', 'DABUR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2495', '10008', 'DCB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2496', '10008', 'DCHL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2497', '10008', 'DENABANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2498', '10008', 'DISHTV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2499', '10008', 'DIVISLAB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2500', '10008', 'DLF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2501', '10008', 'DRREDDY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2502', '10008', 'EDELWEISS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2503', '10008', 'EDUCOMP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2504', '10008', 'EKC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2505', '10008', 'ESCORTS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2506', '10008', 'ESSAROIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2507', '10008', 'EVERONN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2508', '10008', 'FEDERALBNK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2509', '10008', 'FINANTECH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2510', '10008', 'FSL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2511', '10008', 'GAIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2512', '10008', 'GDL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2513', '10008', 'GESHIP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2514', '10008', 'GITANJALI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2515', '10008', 'GLAXO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2516', '10008', 'GMRINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2517', '10008', 'GNFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2518', '10008', 'GRASIM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2519', '10008', 'GSPL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2520', '10008', 'GTL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2521', '10008', 'GTLINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2522', '10008', 'GTOFFSHORE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2523', '10008', 'GUJALKALI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2524', '10008', 'GVKPIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2525', '10008', 'HAVELLS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2526', '10008', 'HCC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2527', '10008', 'HCLINSYS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2528', '10008', 'HCLTECH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2529', '10008', 'HDFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2530', '10008', 'HDFCBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2531', '10008', 'HDIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2532', '10008', 'HEROHONDA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2533', '10008', 'HINDALCO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2534', '10008', 'HINDOILEXP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2535', '10008', 'HINDPETRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2536', '10008', 'HINDUNILVR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2537', '10008', 'HINDZINC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2538', '10008', 'HOTELEELA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2539', '10008', 'IBREALEST', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2540', '10008', 'ICICIBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2541', '10008', 'ICSA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2542', '10008', 'IDBI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2543', '10008', 'IDEA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2544', '10008', 'IDFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2545', '10008', 'IFCI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2546', '10008', 'INDHOTEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2547', '10008', 'INDIACEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2548', '10008', 'INDIAINFO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2549', '10008', 'INDIANB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2550', '10008', 'INDUSINDBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2551', '10008', 'INFOSYSTCH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2552', '10008', 'IOB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2553', '10008', 'IOC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2554', '10008', 'IRB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2555', '10008', 'ISPATIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2556', '10008', 'ITC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2557', '10008', 'IVRCLINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2558', '10008', 'JETAIRWAYS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2559', '10008', 'JINDALSAW', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2560', '10008', 'JINDALSTEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2561', '10008', 'JPASSOCIAT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2562', '10008', 'JPHYDRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2563', '10008', 'JSL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2564', '10008', 'JSWSTEEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2565', '10008', 'KESORAMIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2566', '10008', 'KFA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2567', '10008', 'KOTAKBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2568', '10008', 'KSK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2569', '10008', 'KSOILS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2570', '10008', 'KTKBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2571', '10008', 'LAXMIMACH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2572', '10008', 'LICHSGFIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2573', '10008', 'LITL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2574', '10008', 'LT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2575', '10008', 'LUPIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2576', '10008', 'MAHLIFE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2577', '10008', 'MAHSEAMLES', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2578', '10008', 'MARUTI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2579', '10008', 'MCDOWELL_N', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2580', '10008', 'MINDTREE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2581', '10008', 'minifty', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2582', '10008', 'MLL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2583', '10008', 'MONNETISPA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2584', '10008', 'MOSERBAER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2585', '10008', 'MPHASIS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2586', '10008', 'MRF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2587', '10008', 'MRPL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2588', '10008', 'MTNL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2589', '10008', 'M_M', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2590', '10008', 'NAGARCONST', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2591', '10008', 'NAGARFERT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2592', '10008', 'NATIONALUM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2593', '10008', 'NBVENTURES', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2594', '10008', 'NDTV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2595', '10008', 'NETWORK18', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2596', '10008', 'NEYVELILIG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2597', '10008', 'NIFTY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2598', '10008', 'NIITLTD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2599', '10008', 'NOIDATOLL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2600', '10008', 'NTPC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2601', '10008', 'OFSS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2602', '10008', 'ONGC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2603', '10008', 'OPTOCIRCUI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2604', '10008', 'ORCHIDCHEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2605', '10008', 'ORIENTBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2606', '10008', 'PANTALOONR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2607', '10008', 'PATELENG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2608', '10008', 'PATNI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2609', '10008', 'PENINLAND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2610', '10008', 'PETRONET', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2611', '10008', 'PFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2612', '10008', 'PIRHEALTH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2613', '10008', 'PNB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2614', '10008', 'POLARIS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2615', '10008', 'POWERGRID', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2616', '10008', 'PRAJIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2617', '10008', 'PTC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2618', '10008', 'PUNJLLOYD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2619', '10008', 'RAJESHEXPO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2620', '10008', 'RANBAXY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2621', '10008', 'RCOM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2622', '10008', 'RECLTD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2623', '10008', 'RELCAPITAL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2624', '10008', 'RELIANCE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2625', '10008', 'RELINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2626', '10008', 'RENUKA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2627', '10008', 'RIIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2628', '10008', 'RNRL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2629', '10008', 'ROLTA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2630', '10008', 'RPL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2631', '10008', 'RPOWER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2632', '10008', 'SAIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2633', '10008', 'SBIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2634', '10008', 'SCI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2635', '10008', 'SESAGOA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2636', '10008', 'SIEMENS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2637', '10008', 'SINTEX', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2638', '10008', 'SKUMARSYNF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2639', '10008', 'SREINTFIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2640', '10008', 'SRF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2641', '10008', 'STAR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2642', '10008', 'STER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2643', '10008', 'STERLINBIO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2644', '10008', 'SUNPHARMA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2645', '10008', 'SUNTV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2646', '10008', 'SUZLON', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2647', '10008', 'SYNDIBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2648', '10008', 'TATACHEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2649', '10008', 'TATACOMM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2650', '10008', 'TATAMOTORS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2651', '10008', 'TATAPOWER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2652', '10008', 'TATASTEEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2653', '10008', 'TATATEA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2654', '10008', 'TCS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2655', '10008', 'TECHM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2656', '10008', 'THERMAX', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2657', '10008', 'TITAN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2658', '10008', 'TORNTPOWER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2659', '10008', 'TRIVENI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2660', '10008', 'TTML', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2661', '10008', 'TULIP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2662', '10008', 'TV18', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2663', '10008', 'TVSMOTOR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2664', '10008', 'UCOBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2665', '10008', 'ULTRACEMCO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2666', '10008', 'UNIONBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2667', '10008', 'UNIPHOS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2668', '10008', 'UNITECH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2669', '10008', 'UTVSOF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2670', '10008', 'VIJAYABANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2671', '10008', 'VOLTAS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2672', '10008', 'WELGUJ', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2673', '10008', 'WIPRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2674', '10008', 'WOCKPHARMA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2675', '10008', 'YESBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2676', '10008', 'COPPER', 'MCX', '500', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2677', '10008', 'CRUDEOIL', 'MCX', '500', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2678', '10008', 'GOLD', 'MCX', '500', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2679', '10008', 'GOLDM', 'MCX', '100', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2680', '10008', 'LEAD', 'MCX', '1000', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2681', '10008', 'NATURALGAS', 'MCX', '0', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2682', '10008', 'NICKEL', 'MCX', '0', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2683', '10008', 'SILVER', 'MCX', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2684', '10008', 'ZINC', 'MCX', '0', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2685', '10009', 'ABAN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2686', '10009', 'ABB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2687', '10009', 'ABGSHIP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2688', '10009', 'ABIRLANUVO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2689', '10009', 'ACC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2690', '10009', 'ADLABSFILM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2691', '10009', 'ALBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2692', '10009', 'ALOKTEXT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2693', '10009', 'AMBUJACEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2694', '10009', 'AMTEKAUTO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2695', '10009', 'ANDHRABANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2696', '10009', 'APIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2697', '10009', 'APTECHT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2698', '10009', 'ARVIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2699', '10009', 'ASHOKLEY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2700', '10009', 'ASIANPAINT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2701', '10009', 'AUROPHARMA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2702', '10009', 'AXISBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2703', '10009', 'BAJAJAUTO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2704', '10009', 'BAJAJHIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2705', '10009', 'BAJAJHLDNG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2706', '10009', 'BALAJITELE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2707', '10009', 'BALLARPUR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2708', '10009', 'BALRAMCHIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2709', '10009', 'BANKBARODA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2710', '10009', 'BANKINDIA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2711', '10009', 'BANKNIFTY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2712', '10009', 'BATAINDIA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2713', '10009', 'BEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2714', '10009', 'BEML', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2715', '10009', 'BHARATFORG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2716', '10009', 'BHARTIARTL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2717', '10009', 'BHEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2718', '10009', 'BHUSANSTL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2719', '10009', 'BIOCON', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2720', '10009', 'BIRLACORPN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2721', '10009', 'BOMDYEING', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2722', '10009', 'BOSCHLTD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2723', '10009', 'BPCL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2724', '10009', 'BRFL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2725', '10009', 'CAIRN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2726', '10009', 'CANBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2727', '10009', 'CENTRALBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2728', '10009', 'CENTURYTEX', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2729', '10009', 'CESC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2730', '10009', 'CHAMBLFERT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2731', '10009', 'CHENNPETRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2732', '10009', 'CIPLA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2733', '10009', 'COLPAL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2734', '10009', 'CONCOR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2735', '10009', 'CORPBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2736', '10009', 'CROMPGREAV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2737', '10009', 'CUMMINSIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2738', '10009', 'DABUR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2739', '10009', 'DCB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2740', '10009', 'DCHL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2741', '10009', 'DENABANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2742', '10009', 'DISHTV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2743', '10009', 'DIVISLAB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2744', '10009', 'DLF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2745', '10009', 'DRREDDY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2746', '10009', 'EDELWEISS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2747', '10009', 'EDUCOMP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2748', '10009', 'EKC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2749', '10009', 'ESCORTS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2750', '10009', 'ESSAROIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2751', '10009', 'EVERONN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2752', '10009', 'FEDERALBNK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2753', '10009', 'FINANTECH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2754', '10009', 'FSL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2755', '10009', 'GAIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2756', '10009', 'GDL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2757', '10009', 'GESHIP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2758', '10009', 'GITANJALI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2759', '10009', 'GLAXO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2760', '10009', 'GMRINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2761', '10009', 'GNFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2762', '10009', 'GRASIM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2763', '10009', 'GSPL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2764', '10009', 'GTL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2765', '10009', 'GTLINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2766', '10009', 'GTOFFSHORE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2767', '10009', 'GUJALKALI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2768', '10009', 'GVKPIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2769', '10009', 'HAVELLS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2770', '10009', 'HCC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2771', '10009', 'HCLINSYS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2772', '10009', 'HCLTECH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2773', '10009', 'HDFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2774', '10009', 'HDFCBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2775', '10009', 'HDIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2776', '10009', 'HEROHONDA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2777', '10009', 'HINDALCO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2778', '10009', 'HINDOILEXP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2779', '10009', 'HINDPETRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2780', '10009', 'HINDUNILVR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2781', '10009', 'HINDZINC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2782', '10009', 'HOTELEELA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2783', '10009', 'IBREALEST', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2784', '10009', 'ICICIBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2785', '10009', 'ICSA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2786', '10009', 'IDBI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2787', '10009', 'IDEA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2788', '10009', 'IDFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2789', '10009', 'IFCI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2790', '10009', 'INDHOTEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2791', '10009', 'INDIACEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2792', '10009', 'INDIAINFO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2793', '10009', 'INDIANB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2794', '10009', 'INDUSINDBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2795', '10009', 'INFOSYSTCH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2796', '10009', 'IOB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2797', '10009', 'IOC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2798', '10009', 'IRB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2799', '10009', 'ISPATIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2800', '10009', 'ITC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2801', '10009', 'IVRCLINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2802', '10009', 'JETAIRWAYS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2803', '10009', 'JINDALSAW', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2804', '10009', 'JINDALSTEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2805', '10009', 'JPASSOCIAT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2806', '10009', 'JPHYDRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2807', '10009', 'JSL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2808', '10009', 'JSWSTEEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2809', '10009', 'KESORAMIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2810', '10009', 'KFA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2811', '10009', 'KOTAKBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2812', '10009', 'KSK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2813', '10009', 'KSOILS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2814', '10009', 'KTKBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2815', '10009', 'LAXMIMACH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2816', '10009', 'LICHSGFIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2817', '10009', 'LITL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2818', '10009', 'LT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2819', '10009', 'LUPIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2820', '10009', 'MAHLIFE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2821', '10009', 'MAHSEAMLES', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2822', '10009', 'MARUTI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2823', '10009', 'MCDOWELL_N', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2824', '10009', 'MINDTREE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2825', '10009', 'minifty', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2826', '10009', 'MLL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2827', '10009', 'MONNETISPA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2828', '10009', 'MOSERBAER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2829', '10009', 'MPHASIS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2830', '10009', 'MRF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2831', '10009', 'MRPL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2832', '10009', 'MTNL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2833', '10009', 'M_M', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2834', '10009', 'NAGARCONST', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2835', '10009', 'NAGARFERT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2836', '10009', 'NATIONALUM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2837', '10009', 'NBVENTURES', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2838', '10009', 'NDTV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2839', '10009', 'NETWORK18', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2840', '10009', 'NEYVELILIG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2841', '10009', 'NIFTY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2842', '10009', 'NIITLTD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2843', '10009', 'NOIDATOLL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2844', '10009', 'NTPC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2845', '10009', 'OFSS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2846', '10009', 'ONGC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2847', '10009', 'OPTOCIRCUI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2848', '10009', 'ORCHIDCHEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2849', '10009', 'ORIENTBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2850', '10009', 'PANTALOONR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2851', '10009', 'PATELENG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2852', '10009', 'PATNI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2853', '10009', 'PENINLAND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2854', '10009', 'PETRONET', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2855', '10009', 'PFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2856', '10009', 'PIRHEALTH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2857', '10009', 'PNB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2858', '10009', 'POLARIS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2859', '10009', 'POWERGRID', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2860', '10009', 'PRAJIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2861', '10009', 'PTC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2862', '10009', 'PUNJLLOYD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2863', '10009', 'RAJESHEXPO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2864', '10009', 'RANBAXY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2865', '10009', 'RCOM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2866', '10009', 'RECLTD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2867', '10009', 'RELCAPITAL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2868', '10009', 'RELIANCE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2869', '10009', 'RELINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2870', '10009', 'RENUKA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2871', '10009', 'RIIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2872', '10009', 'RNRL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2873', '10009', 'ROLTA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2874', '10009', 'RPL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2875', '10009', 'RPOWER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2876', '10009', 'SAIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2877', '10009', 'SBIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2878', '10009', 'SCI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2879', '10009', 'SESAGOA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2880', '10009', 'SIEMENS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2881', '10009', 'SINTEX', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2882', '10009', 'SKUMARSYNF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2883', '10009', 'SREINTFIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2884', '10009', 'SRF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2885', '10009', 'STAR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2886', '10009', 'STER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2887', '10009', 'STERLINBIO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2888', '10009', 'SUNPHARMA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2889', '10009', 'SUNTV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2890', '10009', 'SUZLON', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2891', '10009', 'SYNDIBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2892', '10009', 'TATACHEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2893', '10009', 'TATACOMM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2894', '10009', 'TATAMOTORS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2895', '10009', 'TATAPOWER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2896', '10009', 'TATASTEEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2897', '10009', 'TATATEA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2898', '10009', 'TCS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2899', '10009', 'TECHM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2900', '10009', 'THERMAX', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2901', '10009', 'TITAN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2902', '10009', 'TORNTPOWER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2903', '10009', 'TRIVENI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2904', '10009', 'TTML', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2905', '10009', 'TULIP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2906', '10009', 'TV18', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2907', '10009', 'TVSMOTOR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2908', '10009', 'UCOBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2909', '10009', 'ULTRACEMCO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2910', '10009', 'UNIONBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2911', '10009', 'UNIPHOS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2912', '10009', 'UNITECH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2913', '10009', 'UTVSOF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2914', '10009', 'VIJAYABANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2915', '10009', 'VOLTAS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2916', '10009', 'WELGUJ', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2917', '10009', 'WIPRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2918', '10009', 'WOCKPHARMA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2919', '10009', 'YESBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2920', '10009', 'COPPER', 'MCX', '500', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2921', '10009', 'CRUDEOIL', 'MCX', '500', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2922', '10009', 'GOLD', 'MCX', '500', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2923', '10009', 'GOLDM', 'MCX', '100', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2924', '10009', 'LEAD', 'MCX', '1000', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2925', '10009', 'NATURALGAS', 'MCX', '0', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2926', '10009', 'NICKEL', 'MCX', '0', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2927', '10009', 'SILVER', 'MCX', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2928', '10009', 'ZINC', 'MCX', '0', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2929', '10010', 'ABAN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2930', '10010', 'ABB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2931', '10010', 'ABGSHIP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2932', '10010', 'ABIRLANUVO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2933', '10010', 'ACC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2934', '10010', 'ADLABSFILM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2935', '10010', 'ALBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2936', '10010', 'ALOKTEXT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2937', '10010', 'AMBUJACEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2938', '10010', 'AMTEKAUTO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2939', '10010', 'ANDHRABANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2940', '10010', 'APIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2941', '10010', 'APTECHT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2942', '10010', 'ARVIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2943', '10010', 'ASHOKLEY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2944', '10010', 'ASIANPAINT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2945', '10010', 'AUROPHARMA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2946', '10010', 'AXISBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2947', '10010', 'BAJAJAUTO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2948', '10010', 'BAJAJHIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2949', '10010', 'BAJAJHLDNG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2950', '10010', 'BALAJITELE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2951', '10010', 'BALLARPUR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2952', '10010', 'BALRAMCHIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2953', '10010', 'BANKBARODA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2954', '10010', 'BANKINDIA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2955', '10010', 'BANKNIFTY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2956', '10010', 'BATAINDIA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2957', '10010', 'BEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2958', '10010', 'BEML', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2959', '10010', 'BHARATFORG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2960', '10010', 'BHARTIARTL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2961', '10010', 'BHEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2962', '10010', 'BHUSANSTL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2963', '10010', 'BIOCON', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2964', '10010', 'BIRLACORPN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2965', '10010', 'BOMDYEING', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2966', '10010', 'BOSCHLTD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2967', '10010', 'BPCL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2968', '10010', 'BRFL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2969', '10010', 'CAIRN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2970', '10010', 'CANBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2971', '10010', 'CENTRALBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2972', '10010', 'CENTURYTEX', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2973', '10010', 'CESC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2974', '10010', 'CHAMBLFERT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2975', '10010', 'CHENNPETRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2976', '10010', 'CIPLA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2977', '10010', 'COLPAL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2978', '10010', 'CONCOR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2979', '10010', 'CORPBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2980', '10010', 'CROMPGREAV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2981', '10010', 'CUMMINSIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2982', '10010', 'DABUR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2983', '10010', 'DCB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2984', '10010', 'DCHL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2985', '10010', 'DENABANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2986', '10010', 'DISHTV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2987', '10010', 'DIVISLAB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2988', '10010', 'DLF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2989', '10010', 'DRREDDY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2990', '10010', 'EDELWEISS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2991', '10010', 'EDUCOMP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2992', '10010', 'EKC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2993', '10010', 'ESCORTS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2994', '10010', 'ESSAROIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2995', '10010', 'EVERONN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2996', '10010', 'FEDERALBNK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2997', '10010', 'FINANTECH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2998', '10010', 'FSL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('2999', '10010', 'GAIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3000', '10010', 'GDL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3001', '10010', 'GESHIP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3002', '10010', 'GITANJALI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3003', '10010', 'GLAXO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3004', '10010', 'GMRINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3005', '10010', 'GNFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3006', '10010', 'GRASIM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3007', '10010', 'GSPL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3008', '10010', 'GTL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3009', '10010', 'GTLINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3010', '10010', 'GTOFFSHORE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3011', '10010', 'GUJALKALI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3012', '10010', 'GVKPIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3013', '10010', 'HAVELLS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3014', '10010', 'HCC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3015', '10010', 'HCLINSYS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3016', '10010', 'HCLTECH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3017', '10010', 'HDFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3018', '10010', 'HDFCBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3019', '10010', 'HDIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3020', '10010', 'HEROHONDA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3021', '10010', 'HINDALCO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3022', '10010', 'HINDOILEXP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3023', '10010', 'HINDPETRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3024', '10010', 'HINDUNILVR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3025', '10010', 'HINDZINC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3026', '10010', 'HOTELEELA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3027', '10010', 'IBREALEST', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3028', '10010', 'ICICIBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3029', '10010', 'ICSA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3030', '10010', 'IDBI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3031', '10010', 'IDEA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3032', '10010', 'IDFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3033', '10010', 'IFCI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3034', '10010', 'INDHOTEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3035', '10010', 'INDIACEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3036', '10010', 'INDIAINFO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3037', '10010', 'INDIANB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3038', '10010', 'INDUSINDBK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3039', '10010', 'INFOSYSTCH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3040', '10010', 'IOB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3041', '10010', 'IOC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3042', '10010', 'IRB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3043', '10010', 'ISPATIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3044', '10010', 'ITC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3045', '10010', 'IVRCLINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3046', '10010', 'JETAIRWAYS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3047', '10010', 'JINDALSAW', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3048', '10010', 'JINDALSTEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3049', '10010', 'JPASSOCIAT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3050', '10010', 'JPHYDRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3051', '10010', 'JSL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3052', '10010', 'JSWSTEEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3053', '10010', 'KESORAMIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3054', '10010', 'KFA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3055', '10010', 'KOTAKBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3056', '10010', 'KSK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3057', '10010', 'KSOILS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3058', '10010', 'KTKBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3059', '10010', 'LAXMIMACH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3060', '10010', 'LICHSGFIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3061', '10010', 'LITL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3062', '10010', 'LT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3063', '10010', 'LUPIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3064', '10010', 'MAHLIFE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3065', '10010', 'MAHSEAMLES', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3066', '10010', 'MARUTI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3067', '10010', 'MCDOWELL_N', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3068', '10010', 'MINDTREE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3069', '10010', 'minifty', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3070', '10010', 'MLL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3071', '10010', 'MONNETISPA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3072', '10010', 'MOSERBAER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3073', '10010', 'MPHASIS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3074', '10010', 'MRF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3075', '10010', 'MRPL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3076', '10010', 'MTNL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3077', '10010', 'M_M', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3078', '10010', 'NAGARCONST', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3079', '10010', 'NAGARFERT', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3080', '10010', 'NATIONALUM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3081', '10010', 'NBVENTURES', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3082', '10010', 'NDTV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3083', '10010', 'NETWORK18', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3084', '10010', 'NEYVELILIG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3085', '10010', 'NIFTY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3086', '10010', 'NIITLTD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3087', '10010', 'NOIDATOLL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3088', '10010', 'NTPC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3089', '10010', 'OFSS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3090', '10010', 'ONGC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3091', '10010', 'OPTOCIRCUI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3092', '10010', 'ORCHIDCHEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3093', '10010', 'ORIENTBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3094', '10010', 'PANTALOONR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3095', '10010', 'PATELENG', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3096', '10010', 'PATNI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3097', '10010', 'PENINLAND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3098', '10010', 'PETRONET', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3099', '10010', 'PFC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3100', '10010', 'PIRHEALTH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3101', '10010', 'PNB', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3102', '10010', 'POLARIS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3103', '10010', 'POWERGRID', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3104', '10010', 'PRAJIND', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3105', '10010', 'PTC', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3106', '10010', 'PUNJLLOYD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3107', '10010', 'RAJESHEXPO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3108', '10010', 'RANBAXY', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3109', '10010', 'RCOM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3110', '10010', 'RECLTD', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3111', '10010', 'RELCAPITAL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3112', '10010', 'RELIANCE', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3113', '10010', 'RELINFRA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3114', '10010', 'RENUKA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3115', '10010', 'RIIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3116', '10010', 'RNRL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3117', '10010', 'ROLTA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3118', '10010', 'RPL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3119', '10010', 'RPOWER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3120', '10010', 'SAIL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3121', '10010', 'SBIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3122', '10010', 'SCI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3123', '10010', 'SESAGOA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3124', '10010', 'SIEMENS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3125', '10010', 'SINTEX', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3126', '10010', 'SKUMARSYNF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3127', '10010', 'SREINTFIN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3128', '10010', 'SRF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3129', '10010', 'STAR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3130', '10010', 'STER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3131', '10010', 'STERLINBIO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3132', '10010', 'SUNPHARMA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3133', '10010', 'SUNTV', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3134', '10010', 'SUZLON', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3135', '10010', 'SYNDIBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3136', '10010', 'TATACHEM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3137', '10010', 'TATACOMM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3138', '10010', 'TATAMOTORS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3139', '10010', 'TATAPOWER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3140', '10010', 'TATASTEEL', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3141', '10010', 'TATATEA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3142', '10010', 'TCS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3143', '10010', 'TECHM', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3144', '10010', 'THERMAX', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3145', '10010', 'TITAN', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3146', '10010', 'TORNTPOWER', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3147', '10010', 'TRIVENI', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3148', '10010', 'TTML', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3149', '10010', 'TULIP', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3150', '10010', 'TV18', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3151', '10010', 'TVSMOTOR', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3152', '10010', 'UCOBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3153', '10010', 'ULTRACEMCO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3154', '10010', 'UNIONBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3155', '10010', 'UNIPHOS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3156', '10010', 'UNITECH', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3157', '10010', 'UTVSOF', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3158', '10010', 'VIJAYABANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3159', '10010', 'VOLTAS', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3160', '10010', 'WELGUJ', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3161', '10010', 'WIPRO', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3162', '10010', 'WOCKPHARMA', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3163', '10010', 'YESBANK', 'F_O', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3164', '10010', 'COPPER', 'MCX', '500', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3165', '10010', 'CRUDEOIL', 'MCX', '500', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3166', '10010', 'GOLD', 'MCX', '500', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3167', '10010', 'GOLDM', 'MCX', '100', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3168', '10010', 'LEAD', 'MCX', '1000', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3169', '10010', 'NATURALGAS', 'MCX', '0', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3170', '10010', 'NICKEL', 'MCX', '0', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3171', '10010', 'SILVER', 'MCX', '150', NULL, NULL);
insert into clientbrok (clientBrokId, clientId, itemId, exchange, oneSideBrok, brok1, brok2) values ('3172', '10010', 'ZINC', 'MCX', '0', NULL, NULL);
drop table if exists clientexchange;
create table clientexchange (
  clientexchangeId int(6) not null auto_increment,
  clientId int(6) ,
  exchange varchar(50) ,
  brok int(50) ,
  brok1 float ,
  brok2 float ,
  parentClientId int(6) unsigned default '0' ,
  commissionIn char(1) ,
  commission double default '0' ,
  PRIMARY KEY (clientexchangeId)
);

drop table if exists exchange;
create table exchange (
  exchangeId int(6) unsigned not null auto_increment,
  exchange varchar(20) not null ,
  multiply tinyint(1) default '0' not null ,
  profitBankRate float ,
  lossBankRate float ,
  PRIMARY KEY (exchangeId)
);

insert into exchange (exchangeId, exchange, multiply, profitBankRate, lossBankRate) values ('1', 'MCX', '0', '1', '1');
insert into exchange (exchangeId, exchange, multiply, profitBankRate, lossBankRate) values ('2', 'F_O', '0', '0', '0');
drop table if exists expensemaster;
create table expensemaster (
  expensemasterId int(6) not null auto_increment,
  expenseName varchar(50) ,
  PRIMARY KEY (expensemasterId)
);

insert into expensemaster (expensemasterId, expenseName) values ('1', 'Light');
insert into expensemaster (expensemasterId, expenseName) values ('3', 'Petrol2');
drop table if exists expiry;
create table expiry (
  expiryId int(6) not null auto_increment,
  itemId varchar(50) ,
  expiryDate varchar(20) ,
  exchange varchar(20) not null ,
  PRIMARY KEY (expiryId)
);

insert into expiry (expiryId, itemId, expiryDate, exchange) values ('280', 'GOLD', '19MAR2009', 'MCX');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('281', 'CRUDEOIL', '19MAR2009', 'MCX');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('282', 'COPPER', '19MAR2009', 'MCX');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('283', 'GOLDM', '19MAR2009', 'MCX');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('284', 'NATURALGAS', '19MAR2009', 'MCX');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('285', 'ZINC', '19MAR2009', 'MCX');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('286', 'NICKEL', '19MAR2009', 'MCX');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('291', 'SILVER', '28MAR2009', 'MCX');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('299', 'LEAD', '28MAR2009', 'MCX');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('300', 'NIFTY', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('301', 'BANKNIFTY', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('302', 'RELIANCE', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('303', 'SBIN', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('304', 'BHEL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('305', 'RELCAPITAL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('306', 'RCOM', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('307', 'ADLABSFILM', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('308', 'ABAN', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('309', 'CENTURYTEX', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('310', 'MCDOWELL_N', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('311', 'JPASSOCIAT', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('312', 'WELGUJ', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('313', 'RELINFRA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('314', 'WIPRO', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('315', 'ICICIBANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('316', 'YESBANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('317', 'HDFC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('318', 'STER', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('319', 'BHARTIARTL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('320', 'HDFCBANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('321', 'CANBK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('322', 'LT', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('323', 'RPL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('324', 'RNRL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('325', 'ESSAROIL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('326', 'JINDALSTEL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('327', 'DLF', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('328', 'EDUCOMP', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('329', 'TATASTEEL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('330', 'TATAPOWER', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('331', 'NDTV', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('332', 'TATAMOTORS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('333', 'AXISBANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('334', 'UNITECH', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('335', 'NTPC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('336', 'IVRCLINFRA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('337', 'TCS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('338', 'PUNJLLOYD', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('339', 'SRF', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('340', 'CIPLA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('341', 'ONGC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('342', 'GSPL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('343', 'JSWSTEEL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('344', 'MARUTI', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('345', 'M_M', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('346', 'ABB', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('347', 'RENUKA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('348', 'LITL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('349', 'WOCKPHARMA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('350', 'DCB', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('351', 'VOLTAS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('352', 'VIJAYABANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('353', 'UTVSOF', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('354', 'UNIPHOS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('355', 'UNIONBANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('356', 'ULTRACEMCO', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('357', 'UCOBANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('358', 'TVSMOTOR', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('359', 'TV18', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('360', 'TULIP', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('361', 'TTML', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('362', 'TRIVENI', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('363', 'TORNTPOWER', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('364', 'TITAN', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('365', 'THERMAX', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('366', 'TECHM', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('367', 'TATATEA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('368', 'TATACOMM', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('369', 'TATACHEM', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('370', 'SYNDIBANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('371', 'SUZLON', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('372', 'SUNTV', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('373', 'SUNPHARMA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('374', 'STERLINBIO', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('375', 'STAR', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('376', 'SREINTFIN', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('377', 'SKUMARSYNF', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('378', 'SINTEX', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('379', 'SIEMENS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('380', 'SESAGOA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('381', 'SCI', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('382', 'SAIL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('383', 'RPOWER', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('384', 'ROLTA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('385', 'RIIL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('386', 'RECLTD', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('387', 'RANBAXY', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('388', 'RAJESHEXPO', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('389', 'PTC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('390', 'PRAJIND', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('391', 'POWERGRID', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('392', 'POLARIS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('393', 'PNB', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('394', 'PIRHEALTH', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('395', 'PFC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('396', 'PETRONET', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('397', 'PENINLAND', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('398', 'PATNI', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('399', 'PATELENG', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('400', 'PANTALOONR', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('401', 'ORIENTBANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('402', 'ORCHIDCHEM', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('403', 'OPTOCIRCUI', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('404', 'OFSS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('405', 'NOIDATOLL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('406', 'NIITLTD', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('407', 'NEYVELILIG', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('408', 'NETWORK18', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('409', 'NBVENTURES', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('410', 'NATIONALUM', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('411', 'NAGARFERT', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('412', 'NAGARCONST', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('413', 'MTNL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('414', 'MRPL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('415', 'MRF', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('416', 'MPHASIS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('417', 'MOSERBAER', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('418', 'MONNETISPA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('419', 'MLL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('420', 'MINDTREE', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('421', 'MAHSEAMLES', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('422', 'MAHLIFE', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('423', 'LUPIN', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('424', 'LICHSGFIN', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('425', 'LAXMIMACH', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('426', 'KTKBANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('427', 'KSOILS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('428', 'KSK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('429', 'KOTAKBANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('430', 'KFA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('431', 'KESORAMIND', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('432', 'JSL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('433', 'JPHYDRO', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('434', 'JINDALSAW', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('435', 'JETAIRWAYS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('436', 'ITC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('437', 'ISPATIND', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('438', 'IRB', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('439', 'IOC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('440', 'IOB', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('441', 'INFOSYSTCH', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('442', 'INDUSINDBK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('443', 'INDIANB', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('444', 'INDIAINFO', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('445', 'INDIACEM', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('446', 'INDHOTEL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('447', 'IFCI', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('448', 'IDFC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('449', 'IDEA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('450', 'IDBI', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('451', 'ICSA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('452', 'IBREALEST', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('453', 'HOTELEELA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('454', 'HINDZINC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('455', 'HINDUNILVR', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('456', 'HINDPETRO', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('457', 'HINDOILEXP', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('458', 'HINDALCO', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('459', 'HEROHONDA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('460', 'HDIL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('461', 'HCLTECH', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('462', 'HCLINSYS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('463', 'HCC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('464', 'HAVELLS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('465', 'GVKPIL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('466', 'GUJALKALI', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('467', 'GTOFFSHORE', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('468', 'GTLINFRA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('469', 'GTL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('470', 'GRASIM', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('471', 'GNFC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('472', 'GMRINFRA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('473', 'GLAXO', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('474', 'GITANJALI', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('475', 'GESHIP', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('476', 'GDL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('477', 'GAIL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('478', 'FSL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('479', 'FINANTECH', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('480', 'FEDERALBNK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('481', 'EVERONN', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('482', 'ESCORTS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('483', 'EKC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('484', 'EDELWEISS', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('485', 'DRREDDY', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('486', 'DIVISLAB', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('487', 'DISHTV', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('488', 'DENABANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('489', 'DCHL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('490', 'DABUR', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('491', 'CUMMINSIND', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('492', 'CROMPGREAV', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('493', 'CORPBANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('494', 'CONCOR', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('495', 'COLPAL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('496', 'CHENNPETRO', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('497', 'CHAMBLFERT', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('498', 'CESC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('499', 'CENTRALBK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('500', 'CAIRN', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('501', 'BRFL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('502', 'BPCL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('503', 'BOSCHLTD', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('504', 'BOMDYEING', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('505', 'BIRLACORPN', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('506', 'BIOCON', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('507', 'BHUSANSTL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('508', 'BHARATFORG', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('509', 'BEML', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('510', 'BEL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('511', 'BATAINDIA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('512', 'BANKINDIA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('513', 'BANKBARODA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('514', 'BALRAMCHIN', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('515', 'BALLARPUR', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('516', 'BALAJITELE', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('517', 'BAJAJHLDNG', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('518', 'BAJAJHIND', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('519', 'BAJAJAUTO', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('520', 'AUROPHARMA', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('521', 'ASIANPAINT', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('522', 'ASHOKLEY', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('523', 'ARVIND', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('524', 'APTECHT', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('525', 'APIL', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('526', 'ANDHRABANK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('527', 'AMTEKAUTO', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('528', 'AMBUJACEM', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('529', 'ALOKTEXT', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('530', 'ALBK', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('531', 'ACC', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('532', 'ABIRLANUVO', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('533', 'ABGSHIP', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('534', 'minifty', '31MAR2016', 'F_O');
insert into expiry (expiryId, itemId, expiryDate, exchange) values ('535', 'F_O', '31MAR2016', 'F_O');
drop table if exists general;
create table general (
  generalId int(6) not null auto_increment,
  filePath varchar(250) ,
  fileName varchar(200) ,
  PRIMARY KEY (generalId)
);

insert into general (generalId, filePath, fileName) values ('1', 'bhavcopies', 'MS20081227.csv');
drop table if exists incomemaster;
create table incomemaster (
  otherIncomeId tinyint(3) unsigned not null auto_increment,
  otherIncomName varchar(60) ,
  PRIMARY KEY (otherIncomeId)
);

drop table if exists item;
create table item (
  itemId varchar(50) not null ,
  item varchar(50) ,
  itemShort varchar(50) ,
  brok float ,
  brok2 float ,
  oneSideBrok float ,
  min int(6) ,
  priceOn int(6) ,
  mulAmount float default '1' ,
  rangeStart float ,
  rangeEnd float ,
  qtyInLots tinyint(1) ,
  exchangeId int(6) unsigned not null ,
  exchange varchar(20) ,
  multiply float not null 
);

insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SILVER', 'SILVER', 'SILVER', '150', '150', '150', '30', '1', '1', '15500', '30000', NULL, '1', 'MCX', '0');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GOLD', 'GOLD', 'GOLD', '300', '200', '500', '100', '10', '1', '7000', '15400', NULL, '1', 'MCX', '0');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('CRUDEOIL', 'CRUDEOIL', 'CRUDEOIL', '0', '0', '500', '100', '1', '1', '1400', '6500', NULL, '1', 'MCX', '0');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('COPPER', 'COPPER', 'COPPER', '0', '0', '500', '1000', '1', '1', '130', '420', NULL, '1', 'MCX', '0');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GOLDM', 'GOLDM', 'GOLDM', '100', '100', '100', '10', '1', '1', '0', '0', NULL, '1', 'MCX', '0');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('NATURALGAS', 'NATURALGAS', 'NATURALGAS', '100', '100', '0', '1250', '1', '1', '0', '0', NULL, '1', 'MCX', '0');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ZINC', 'ZINC', 'ZINC', '100', '100', '0', '5000', '1', '1', '30', '120', NULL, '1', 'MCX', '0');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('NICKEL', 'NICKEL', 'NICKEL', '100', '100', '0', '250', '1', '1', '600', '1200', NULL, '1', 'MCX', '0');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('LEAD', 'LEAD', 'LEAD', '0', '0', '1000', '5000', '1', '1', '0', '0', NULL, '1', 'MCX', '0');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('NIFTY', 'NIFTY', 'NIFTY', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BANKNIFTY', 'BANKNIFTY', 'BANKNIFTY', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('RELIANCE', 'RELIANCE', 'RELIANCE', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SBIN', 'SBIN', 'SBIN', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BHEL', 'BHEL', 'BHEL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('RELCAPITAL', 'RELCAPITAL', 'RELCAPITAL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('RCOM', 'RCOM', 'RCOM', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ADLABSFILM', 'ADLABSFILM', 'ADLABSFILM', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ABAN', 'ABAN', 'ABAN', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('CENTURYTEX', 'CENTURYTEX', 'CENTURYTEX', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('MCDOWELL_N', 'MCDOWELL_N', 'MCDOWELL_N', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('JPASSOCIAT', 'JPASSOCIAT', 'JPASSOCIAT', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('WELGUJ', 'WELGUJ', 'WELGUJ', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('RELINFRA', 'RELINFRA', 'RELINFRA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('WIPRO', 'WIPRO', 'WIPRO', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ICICIBANK', 'ICICIBANK', 'ICICIBANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('YESBANK', 'YESBANK', 'YESBANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HDFC', 'HDFC', 'HDFC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('STER', 'STER', 'STER', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BHARTIARTL', 'BHARTIARTL', 'BHARTIARTL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HDFCBANK', 'HDFCBANK', 'HDFCBANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('CANBK', 'CANBK', 'CANBK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('LT', 'LT', 'LT', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('RPL', 'RPL', 'RPL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('RNRL', 'RNRL', 'RNRL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ESSAROIL', 'ESSAROIL', 'ESSAROIL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('JINDALSTEL', 'JINDALSTEL', 'JINDALSTEL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('DLF', 'DLF', 'DLF', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('EDUCOMP', 'EDUCOMP', 'EDUCOMP', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TATASTEEL', 'TATASTEEL', 'TATASTEEL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TATAPOWER', 'TATAPOWER', 'TATAPOWER', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('NDTV', 'NDTV', 'NDTV', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TATAMOTORS', 'TATAMOTORS', 'TATAMOTORS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('AXISBANK', 'AXISBANK', 'AXISBANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('UNITECH', 'UNITECH', 'UNITECH', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('NTPC', 'NTPC', 'NTPC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('IVRCLINFRA', 'IVRCLINFRA', 'IVRCLINFRA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TCS', 'TCS', 'TCS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('PUNJLLOYD', 'PUNJLLOYD', 'PUNJLLOYD', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SRF', 'SRF', 'SRF', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('CIPLA', 'CIPLA', 'CIPLA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ONGC', 'ONGC', 'ONGC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GSPL', 'GSPL', 'GSPL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('JSWSTEEL', 'JSWSTEEL', 'JSWSTEEL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('MARUTI', 'MARUTI', 'MARUTI', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('M_M', 'M_M', 'M_M', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ABB', 'ABB', 'ABB', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('RENUKA', 'RENUKA', 'RENUKA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('LITL', 'LITL', 'LITL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('WOCKPHARMA', 'WOCKPHARMA', 'WOCKPHARMA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('DCB', 'DCB', 'DCB', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('VOLTAS', 'VOLTAS', 'VOLTAS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('VIJAYABANK', 'VIJAYABANK', 'VIJAYABANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('UTVSOF', 'UTVSOF', 'UTVSOF', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('UNIPHOS', 'UNIPHOS', 'UNIPHOS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('UNIONBANK', 'UNIONBANK', 'UNIONBANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ULTRACEMCO', 'ULTRACEMCO', 'ULTRACEMCO', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('UCOBANK', 'UCOBANK', 'UCOBANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TVSMOTOR', 'TVSMOTOR', 'TVSMOTOR', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TV18', 'TV18', 'TV18', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TULIP', 'TULIP', 'TULIP', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TTML', 'TTML', 'TTML', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TRIVENI', 'TRIVENI', 'TRIVENI', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TORNTPOWER', 'TORNTPOWER', 'TORNTPOWER', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TITAN', 'TITAN', 'TITAN', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('THERMAX', 'THERMAX', 'THERMAX', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TECHM', 'TECHM', 'TECHM', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TATATEA', 'TATATEA', 'TATATEA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TATACOMM', 'TATACOMM', 'TATACOMM', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('TATACHEM', 'TATACHEM', 'TATACHEM', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SYNDIBANK', 'SYNDIBANK', 'SYNDIBANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SUZLON', 'SUZLON', 'SUZLON', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SUNTV', 'SUNTV', 'SUNTV', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SUNPHARMA', 'SUNPHARMA', 'SUNPHARMA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('STERLINBIO', 'STERLINBIO', 'STERLINBIO', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('STAR', 'STAR', 'STAR', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SREINTFIN', 'SREINTFIN', 'SREINTFIN', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SKUMARSYNF', 'SKUMARSYNF', 'SKUMARSYNF', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SINTEX', 'SINTEX', 'SINTEX', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SIEMENS', 'SIEMENS', 'SIEMENS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SESAGOA', 'SESAGOA', 'SESAGOA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SCI', 'SCI', 'SCI', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('SAIL', 'SAIL', 'SAIL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('RPOWER', 'RPOWER', 'RPOWER', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ROLTA', 'ROLTA', 'ROLTA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('RIIL', 'RIIL', 'RIIL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('RECLTD', 'RECLTD', 'RECLTD', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('RANBAXY', 'RANBAXY', 'RANBAXY', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('RAJESHEXPO', 'RAJESHEXPO', 'RAJESHEXPO', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('PTC', 'PTC', 'PTC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('PRAJIND', 'PRAJIND', 'PRAJIND', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('POWERGRID', 'POWERGRID', 'POWERGRID', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('POLARIS', 'POLARIS', 'POLARIS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('PNB', 'PNB', 'PNB', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('PIRHEALTH', 'PIRHEALTH', 'PIRHEALTH', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('PFC', 'PFC', 'PFC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('PETRONET', 'PETRONET', 'PETRONET', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('PENINLAND', 'PENINLAND', 'PENINLAND', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('PATNI', 'PATNI', 'PATNI', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('PATELENG', 'PATELENG', 'PATELENG', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('PANTALOONR', 'PANTALOONR', 'PANTALOONR', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ORIENTBANK', 'ORIENTBANK', 'ORIENTBANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ORCHIDCHEM', 'ORCHIDCHEM', 'ORCHIDCHEM', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('OPTOCIRCUI', 'OPTOCIRCUI', 'OPTOCIRCUI', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('OFSS', 'OFSS', 'OFSS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('NOIDATOLL', 'NOIDATOLL', 'NOIDATOLL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('NIITLTD', 'NIITLTD', 'NIITLTD', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('NEYVELILIG', 'NEYVELILIG', 'NEYVELILIG', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('NETWORK18', 'NETWORK18', 'NETWORK18', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('NBVENTURES', 'NBVENTURES', 'NBVENTURES', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('NATIONALUM', 'NATIONALUM', 'NATIONALUM', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('NAGARFERT', 'NAGARFERT', 'NAGARFERT', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('NAGARCONST', 'NAGARCONST', 'NAGARCONST', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('MTNL', 'MTNL', 'MTNL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('MRPL', 'MRPL', 'MRPL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('MRF', 'MRF', 'MRF', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('MPHASIS', 'MPHASIS', 'MPHASIS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('MOSERBAER', 'MOSERBAER', 'MOSERBAER', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('MONNETISPA', 'MONNETISPA', 'MONNETISPA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('MLL', 'MLL', 'MLL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('MINDTREE', 'MINDTREE', 'MINDTREE', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('MAHSEAMLES', 'MAHSEAMLES', 'MAHSEAMLES', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('MAHLIFE', 'MAHLIFE', 'MAHLIFE', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('LUPIN', 'LUPIN', 'LUPIN', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('LICHSGFIN', 'LICHSGFIN', 'LICHSGFIN', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('LAXMIMACH', 'LAXMIMACH', 'LAXMIMACH', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('KTKBANK', 'KTKBANK', 'KTKBANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('KSOILS', 'KSOILS', 'KSOILS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('KSK', 'KSK', 'KSK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('KOTAKBANK', 'KOTAKBANK', 'KOTAKBANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('KFA', 'KFA', 'KFA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('KESORAMIND', 'KESORAMIND', 'KESORAMIND', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('JSL', 'JSL', 'JSL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('JPHYDRO', 'JPHYDRO', 'JPHYDRO', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('JINDALSAW', 'JINDALSAW', 'JINDALSAW', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('JETAIRWAYS', 'JETAIRWAYS', 'JETAIRWAYS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ITC', 'ITC', 'ITC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ISPATIND', 'ISPATIND', 'ISPATIND', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('IRB', 'IRB', 'IRB', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('IOC', 'IOC', 'IOC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('IOB', 'IOB', 'IOB', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('INFOSYSTCH', 'INFOSYSTCH', 'INFOSYSTCH', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('INDUSINDBK', 'INDUSINDBK', 'INDUSINDBK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('INDIANB', 'INDIANB', 'INDIANB', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('INDIAINFO', 'INDIAINFO', 'INDIAINFO', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('INDIACEM', 'INDIACEM', 'INDIACEM', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('INDHOTEL', 'INDHOTEL', 'INDHOTEL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('IFCI', 'IFCI', 'IFCI', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('IDFC', 'IDFC', 'IDFC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('IDEA', 'IDEA', 'IDEA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('IDBI', 'IDBI', 'IDBI', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ICSA', 'ICSA', 'ICSA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('IBREALEST', 'IBREALEST', 'IBREALEST', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HOTELEELA', 'HOTELEELA', 'HOTELEELA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HINDZINC', 'HINDZINC', 'HINDZINC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HINDUNILVR', 'HINDUNILVR', 'HINDUNILVR', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HINDPETRO', 'HINDPETRO', 'HINDPETRO', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HINDOILEXP', 'HINDOILEXP', 'HINDOILEXP', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HINDALCO', 'HINDALCO', 'HINDALCO', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HEROHONDA', 'HEROHONDA', 'HEROHONDA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HDIL', 'HDIL', 'HDIL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HCLTECH', 'HCLTECH', 'HCLTECH', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HCLINSYS', 'HCLINSYS', 'HCLINSYS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HCC', 'HCC', 'HCC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('HAVELLS', 'HAVELLS', 'HAVELLS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GVKPIL', 'GVKPIL', 'GVKPIL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GUJALKALI', 'GUJALKALI', 'GUJALKALI', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GTOFFSHORE', 'GTOFFSHORE', 'GTOFFSHORE', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GTLINFRA', 'GTLINFRA', 'GTLINFRA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GTL', 'GTL', 'GTL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GRASIM', 'GRASIM', 'GRASIM', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GNFC', 'GNFC', 'GNFC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GMRINFRA', 'GMRINFRA', 'GMRINFRA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GLAXO', 'GLAXO', 'GLAXO', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GITANJALI', 'GITANJALI', 'GITANJALI', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GESHIP', 'GESHIP', 'GESHIP', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GDL', 'GDL', 'GDL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('GAIL', 'GAIL', 'GAIL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('FSL', 'FSL', 'FSL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('FINANTECH', 'FINANTECH', 'FINANTECH', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('FEDERALBNK', 'FEDERALBNK', 'FEDERALBNK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('EVERONN', 'EVERONN', 'EVERONN', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ESCORTS', 'ESCORTS', 'ESCORTS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('EKC', 'EKC', 'EKC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('EDELWEISS', 'EDELWEISS', 'EDELWEISS', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('DRREDDY', 'DRREDDY', 'DRREDDY', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('DIVISLAB', 'DIVISLAB', 'DIVISLAB', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('DISHTV', 'DISHTV', 'DISHTV', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('DENABANK', 'DENABANK', 'DENABANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('DCHL', 'DCHL', 'DCHL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('DABUR', 'DABUR', 'DABUR', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('CUMMINSIND', 'CUMMINSIND', 'CUMMINSIND', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('CROMPGREAV', 'CROMPGREAV', 'CROMPGREAV', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('CORPBANK', 'CORPBANK', 'CORPBANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('CONCOR', 'CONCOR', 'CONCOR', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('COLPAL', 'COLPAL', 'COLPAL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('CHENNPETRO', 'CHENNPETRO', 'CHENNPETRO', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('CHAMBLFERT', 'CHAMBLFERT', 'CHAMBLFERT', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('CESC', 'CESC', 'CESC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('CENTRALBK', 'CENTRALBK', 'CENTRALBK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('CAIRN', 'CAIRN', 'CAIRN', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BRFL', 'BRFL', 'BRFL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BPCL', 'BPCL', 'BPCL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BOSCHLTD', 'BOSCHLTD', 'BOSCHLTD', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BOMDYEING', 'BOMDYEING', 'BOMDYEING', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BIRLACORPN', 'BIRLACORPN', 'BIRLACORPN', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BIOCON', 'BIOCON', 'BIOCON', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BHUSANSTL', 'BHUSANSTL', 'BHUSANSTL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BHARATFORG', 'BHARATFORG', 'BHARATFORG', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BEML', 'BEML', 'BEML', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BEL', 'BEL', 'BEL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BATAINDIA', 'BATAINDIA', 'BATAINDIA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BANKINDIA', 'BANKINDIA', 'BANKINDIA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BANKBARODA', 'BANKBARODA', 'BANKBARODA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BALRAMCHIN', 'BALRAMCHIN', 'BALRAMCHIN', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BALLARPUR', 'BALLARPUR', 'BALLARPUR', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BALAJITELE', 'BALAJITELE', 'BALAJITELE', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BAJAJHLDNG', 'BAJAJHLDNG', 'BAJAJHLDNG', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BAJAJHIND', 'BAJAJHIND', 'BAJAJHIND', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('BAJAJAUTO', 'BAJAJAUTO', 'BAJAJAUTO', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('AUROPHARMA', 'AUROPHARMA', 'AUROPHARMA', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ASIANPAINT', 'ASIANPAINT', 'ASIANPAINT', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ASHOKLEY', 'ASHOKLEY', 'ASHOKLEY', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ARVIND', 'ARVIND', 'ARVIND', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('APTECHT', 'APTECHT', 'APTECHT', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('APIL', 'APIL', 'APIL', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ANDHRABANK', 'ANDHRABANK', 'ANDHRABANK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('AMTEKAUTO', 'AMTEKAUTO', 'AMTEKAUTO', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('AMBUJACEM', 'AMBUJACEM', 'AMBUJACEM', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ALOKTEXT', 'ALOKTEXT', 'ALOKTEXT', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ALBK', 'ALBK', 'ALBK', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ACC', 'ACC', 'ACC', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ABIRLANUVO', 'ABIRLANUVO', 'ABIRLANUVO', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('ABGSHIP', 'ABGSHIP', 'ABGSHIP', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('minifty', 'minifty', 'minifty', '0', '0', '150', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
insert into item (itemId, item, itemShort, brok, brok2, oneSideBrok, min, priceOn, mulAmount, rangeStart, rangeEnd, qtyInLots, exchangeId, exchange, multiply) values ('F_O', 'F_O', 'F_O', '0', '0', '0', '1', '1', '1', '0', '0', NULL, '2', 'F_O', '1');
drop table if exists menu;
create table menu (
  menuId int(10) unsigned not null auto_increment,
  fileToOpen varchar(200) not null ,
  title varchar(55) not null ,
  displayToAdmin tinyint(1) unsigned default '0' not null ,
  displayToOperator tinyint(1) unsigned default '0' not null ,
  displayToClient tinyint(1) unsigned default '0' not null ,
  newWindow tinyint(1) unsigned default '0' not null ,
  newWindowName char(20) not null ,
  newWindowPerameter text not null ,
  PRIMARY KEY (menuId)
);

drop table if exists newexpmaster;
create table newexpmaster (
  newExpMasterId int(6) not null auto_increment,
  newExpName varchar(30) not null ,
  PRIMARY KEY (newExpMasterId)
);

drop table if exists orders;
create table orders (
  orderId int(6) not null auto_increment,
  clientId int(6) default '0' ,
  firstName varchar(35) ,
  middleName varchar(35) ,
  lastName varchar(35) ,
  clientId2 int(6) ,
  firstName2 varchar(35) ,
  middleName2 varchar(35) ,
  lastName2 varchar(35) ,
  buySell varchar(10) ,
  itemId varchar(50) ,
  orderDate date ,
  orderTime varchar(20) ,
  qty int(6) ,
  price float ,
  price2 float ,
  brok int(6) ,
  orderRefNo varchar(60) ,
  orderNote varchar(200) ,
  expiryDate varchar(20) ,
  vendor varchar(50) ,
  userRemarks varchar(50) ,
  ownClient varchar(50) ,
  orderType varchar(30) not null ,
  orderValidity varchar(15) not null ,
  orderValidTillDate date default '0000-00-00' not null ,
  orderStatus varchar(30) not null ,
  triggerPrice float ,
  exchange varchar(20) ,
  refOrderId int(11) ,
  PRIMARY KEY (orderId)
);

drop table if exists otherexp;
create table otherexp (
  otherexpId int(6) not null auto_increment,
  otherExpName varchar(50) ,
  otherExpDate date ,
  otherExpAmount float ,
  note varchar(60) ,
  otherExpMode varchar(60) ,
  PRIMARY KEY (otherexpId)
);

drop table if exists otherincome;
create table otherincome (
  otherIncomId int(11) unsigned not null auto_increment,
  otherIncomName varchar(50) ,
  otherIncomDate date ,
  otherIncomAmount double ,
  note varchar(60) ,
  otherIncomMode varchar(60) ,
  PRIMARY KEY (otherIncomId)
);

drop table if exists partybrokerage;
create table partybrokerage (
  partybrokerageId int(11) not null auto_increment,
  partyId int(11) ,
  brokerageDate date ,
  brokerage float not null ,
  PRIMARY KEY (partybrokerageId)
);

drop table if exists settings;
create table settings (
  settingsId int(6) not null auto_increment,
  settingsKey varchar(30) ,
  value varchar(60) ,
  PRIMARY KEY (settingsId)
);

insert into settings (settingsId, settingsKey, value) values ('1', 'uploadFileWorks', '1');
insert into settings (settingsId, settingsKey, value) values ('2', 'expiryDisplay', 'monthOnly');
insert into settings (settingsId, settingsKey, value) values ('3', 'profitBankRate', '44');
insert into settings (settingsId, settingsKey, value) values ('4', 'lossBankRate', '44.50');
insert into settings (settingsId, settingsKey, value) values ('5', 'clientFieldInTxt', 'ownClient');
insert into settings (settingsId, settingsKey, value) values ('6', 'clientFieldInTxt', 'userRemarks');
insert into settings (settingsId, settingsKey, value) values ('7', 'takeTimeEntry', '0');
insert into settings (settingsId, settingsKey, value) values ('8', 'takeTradeNote', '0');
insert into settings (settingsId, settingsKey, value) values ('9', 'qtyInLots', '0');
insert into settings (settingsId, settingsKey, value) values ('10', 'useItemPriceRange', '1');
insert into settings (settingsId, settingsKey, value) values ('11', 'odinTxtFilePath', 'C:\\ODIN\\DIET\\OnLineBackup\\MCX\\Trades');
insert into settings (settingsId, settingsKey, value) values ('12', 'billWithLedger', '1');
drop table if exists standing;
create table standing (
  standingId int(6) not null auto_increment,
  standingDtCurrent date ,
  standingDtNext date ,
  itemIdExpiryDate varchar(50) ,
  standingPrice float ,
  exchange varchar(20) ,
  UNIQUE standingId (standingId)
);

insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('1', '2009-04-02', '2009-04-04', 'NIFTY28MAR2009', '15', 'F_O');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('2', '2009-04-01', '2009-04-01', 'NIFTY28MAR2009', '1', 'F_O');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('3', '2009-04-01', '2009-04-01', 'NIFTY28MAR2009', '1', 'F_O');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('4', '2009-04-01', '2009-04-01', 'NIFTY28MAR2009', '1', 'F_O');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('5', '2009-04-01', '2009-04-01', 'REL28MAR2009', '2', 'F_O');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('6', '2009-04-01', '2009-04-01', 'COPPER19MAR2009', '3', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('7', '2009-04-01', '2009-04-01', 'NIFTY28MAR2009', '55', 'F_O');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('8', '2009-04-01', '2009-04-01', 'REL28MAR2009', '55', 'F_O');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('9', '2009-04-01', '2009-04-01', 'NIFTY28MAR2009', '3600', 'F_O');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('10', '2009-04-01', '2009-04-01', 'REL28MAR2009', '12500', 'F_O');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('11', '2009-04-01', '2009-04-01', 'COPPER19MAR2009', '1500', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('12', '2009-04-01', '2009-04-01', 'CRUDEOIL19MAR2009', '252', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('13', '2009-04-01', '2009-04-01', 'GOLD19MAR2009', '45885', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('14', '2009-04-01', '2009-04-01', 'GOLD28MAR2009', '3500', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('15', '2009-04-01', '2009-04-01', 'GOLDM19MAR2009', '45022', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('16', '2009-04-01', '2009-04-01', 'GOLDM28MAR2009', '1500', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('17', '2009-04-01', '2009-04-01', 'LEAD19MAR2009', '544', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('18', '2009-04-01', '2009-04-01', 'LEAD28MAR2009', '554', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('19', '2009-04-01', '2009-04-01', 'NATURALGAS19MAR2009', '444', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('20', '2009-04-01', '2009-04-01', 'NATURALGAS28MAR2009', '900', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('21', '2009-04-01', '2009-04-01', 'NICKEL19MAR2009', '888', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('22', '2009-04-01', '2009-04-01', 'NICKEL28MAR2009', '2200', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('23', '2009-04-01', '2009-04-01', 'SILVER21MAR2009', '200', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('24', '2009-04-01', '2009-04-01', 'SILVER28MAR2009', '300', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('25', '2009-04-01', '2009-04-01', 'ZINC19MAR2009', '4500', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('26', '2009-04-01', '2009-04-01', 'ZINC28MAR2009', '7800', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('27', '2009-04-14', '2009-04-15', 'NIFTY28MAR2009', '1515', 'F_O');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('28', '2009-04-14', '2009-04-15', 'REL28MAR2009', '1316', 'F_O');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('29', '2009-04-14', '2009-04-15', 'COPPER19MAR2009', '1111', 'MCX');
insert into standing (standingId, standingDtCurrent, standingDtNext, itemIdExpiryDate, standingPrice, exchange) values ('30', '2009-04-21', '2009-04-22', 'GOLD19MAR2009', '16000', 'MCX');
drop table if exists storedbhav;
create table storedbhav (
  stordId int(11) not null ,
  storDate varchar(20) not null ,
  status varchar(10) not null 
);

drop table if exists tradetxt;
create table tradetxt (
  tradeId int(6) not null auto_increment,
  standing tinyint(1) default '0' ,
  clientId int(6) default '0' ,
  firstName varchar(35) ,
  middleName varchar(35) ,
  lastName varchar(35) ,
  clientId2 int(6) ,
  firstName2 varchar(35) ,
  middleName2 varchar(35) ,
  lastName2 varchar(35) ,
  buySell varchar(10) ,
  itemId varchar(50) ,
  tradeDate date ,
  tradeTime varchar(20) ,
  qty int(6) ,
  price float ,
  price2 float ,
  brok int(6) ,
  tradeRefNo varchar(60) ,
  tradeNote varchar(200) ,
  expiryDate varchar(20) ,
  vendor varchar(50) ,
  userRemarks varchar(50) ,
  ownClient varchar(50) ,
  confirmed tinyint(4) default '0' ,
  exchange varchar(20) ,
  refTradeId int(10) ,
  selfRefId int(6) ,
  PRIMARY KEY (tradeId)
);

insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('1', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Buy', 'MCDOWELL_N', '2016-02-29', '10:48:36', '100', '2696', '2696', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '2', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('2', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Sell', 'MCDOWELL_N', '2016-02-29', '10:48:36', '100', '2696', '2696', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '1', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('3', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'DCHL', '2016-02-29', '11:00:51', '1000', '157.2', '157.2', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '4', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('4', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'DCHL', '2016-02-29', '11:00:51', '1000', '157.2', '157.2', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '3', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('5', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Buy', 'MCDOWELL_N', '2016-02-29', '11:01:11', '100', '2680', '2680', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '6', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('6', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Sell', 'MCDOWELL_N', '2016-02-29', '11:01:11', '100', '2680', '2680', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '5', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('7', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'LICHSGFIN', '2016-02-29', '11:01:37', '2000', '414.35', '414.35', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '8', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('8', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'LICHSGFIN', '2016-02-29', '11:01:37', '2000', '414.35', '414.35', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '7', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('9', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'ICICIBANK', '2016-02-29', '11:02:07', '1250', '188.7', '188.7', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '10', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('10', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'ICICIBANK', '2016-02-29', '11:02:07', '1250', '188.7', '188.7', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '9', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('11', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Buy', 'NIFTY', '2016-02-29', '11:02:24', '500', '7038', '7038', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '12', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('12', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Sell', 'NIFTY', '2016-02-29', '11:02:24', '500', '7038', '7038', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '11', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('13', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'COLPAL', '2016-02-29', '11:02:47', '2000', '822.3', '822.3', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '14', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('14', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'COLPAL', '2016-02-29', '11:02:47', '2000', '822.3', '822.3', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '13', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('15', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'LT', '2016-02-29', '11:03:12', '1000', '1121.1', '1121.1', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '16', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('16', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'LT', '2016-02-29', '11:03:12', '1000', '1121.1', '1121.1', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '15', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('17', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'RELCAPITAL', '2016-02-29', '11:03:50', '2000', '327.6', '327.6', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '18', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('18', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'RELCAPITAL', '2016-02-29', '11:03:50', '2000', '327.6', '327.6', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '17', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('19', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'RELINFRA', '2016-02-29', '11:04:27', '2000', '413.7', '413.7', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '20', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('20', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'RELINFRA', '2016-02-29', '11:04:27', '2000', '413.7', '413.7', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '19', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('21', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'TATAMOTORS', '2016-02-29', '11:05:03', '1000', '304.3', '304.3', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '22', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('22', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'TATAMOTORS', '2016-02-29', '11:05:03', '1000', '304.3', '304.3', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '21', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('23', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'RPOWER', '2016-02-29', '11:05:15', '2000', '44.65', '44.65', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '24', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('24', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'RPOWER', '2016-02-29', '11:05:15', '2000', '44.65', '44.65', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '23', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('25', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'HDFC', '2016-02-29', '11:05:37', '500', '1059.3', '1059.3', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '26', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('26', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'HDFC', '2016-02-29', '11:05:37', '500', '1059.3', '1059.3', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '25', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('27', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'HDFC', '2016-02-29', '11:05:51', '100', '1062.5', '1062.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '28', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('28', '0', '9999', 'zz', '', '', '10007', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'HDFC', '2016-02-29', '11:05:51', '100', '1062.5', '1062.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '27', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('29', '0', '10007', '', '', '', '9999', 'zz', '', '', 'Buy', 'NIFTY', '2016-02-29', '11:30:16', '200', '7052.5', '7052.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '30', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('30', '0', '9999', 'zz', '', '', '10007', '', '', '', 'Sell', 'NIFTY', '2016-02-29', '11:30:16', '200', '7052.5', '7052.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '29', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('31', '0', '10007', '', '', '', '9999', 'zz', '', '', 'Sell', 'NIFTY', '2016-02-29', '11:30:27', '1000', '7051', '7051', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '32', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('32', '0', '9999', 'zz', '', '', '10007', '', '', '', 'Buy', 'NIFTY', '2016-02-29', '11:30:27', '1000', '7051', '7051', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '31', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('33', '0', '10007', '', '', '', '9999', 'zz', '', '', 'Buy', 'NIFTY', '2016-02-29', '11:30:39', '1000', '7032', '7032', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '34', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('34', '0', '9999', 'zz', '', '', '10007', '', '', '', 'Sell', 'NIFTY', '2016-02-29', '11:30:39', '1000', '7032', '7032', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '33', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('35', '0', '3083', 'HITESHBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'ARVIND', '2016-02-29', '11:37:44', '200', '255.65', '255.65', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '36', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('36', '0', '9999', 'zz', '', '', '3083', 'HITESHBHAI', '', '', 'Buy', 'ARVIND', '2016-02-29', '11:37:44', '200', '255.65', '255.65', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '35', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('37', '0', '3083', 'HITESHBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'JSWSTEEL', '2016-02-29', '11:38:56', '100', '1104', '1104', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '38', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('38', '0', '9999', 'zz', '', '', '3083', 'HITESHBHAI', '', '', 'Buy', 'JSWSTEEL', '2016-02-29', '11:38:56', '100', '1104', '1104', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '37', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('39', '0', '3020', 'RAVIBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'AUROPHARMA', '2016-02-29', '11:39:09', '500', '639', '639', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '40', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('40', '0', '9999', 'zz', '', '', '3020', 'RAVIBHAI', '', '', 'Buy', 'AUROPHARMA', '2016-02-29', '11:39:09', '500', '639', '639', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '39', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('41', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Sell', 'ICICIBANK', '2016-02-29', '11:39:43', '5000', '186.15', '186.15', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '42', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('42', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Buy', 'ICICIBANK', '2016-02-29', '11:39:43', '5000', '186.15', '186.15', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '41', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('43', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Sell', 'AUROPHARMA', '2016-02-29', '11:40:10', '500', '638', '638', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '44', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('44', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Buy', 'AUROPHARMA', '2016-02-29', '11:40:10', '500', '638', '638', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '43', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('45', '0', '3083', 'HITESHBHAI', '', '', '9999', 'zz', '', '', 'Buy', 'AUROPHARMA', '2016-02-29', '11:40:43', '300', '877.5', '877.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '46', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('46', '0', '9999', 'zz', '', '', '3083', 'HITESHBHAI', '', '', 'Sell', 'AUROPHARMA', '2016-02-29', '11:40:43', '300', '877.5', '877.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '45', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('47', '0', '3020', 'RAVIBHAI', '', '', '9999', 'zz', '', '', 'Buy', 'YESBANK', '2016-02-29', '11:40:54', '1000', '688', '688', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '48', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('48', '0', '9999', 'zz', '', '', '3020', 'RAVIBHAI', '', '', 'Sell', 'YESBANK', '2016-02-29', '11:40:54', '1000', '688', '688', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '47', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('49', '0', '10006', 'NIRANJAN', 'VYAS', '', '9999', 'zz', '', '', 'Sell', 'TVSMOTOR', '2016-02-29', '11:41:18', '2000', '263.25', '263.25', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '50', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('50', '0', '9999', 'zz', '', '', '10006', 'NIRANJAN', 'VYAS', '', 'Buy', 'TVSMOTOR', '2016-02-29', '11:41:18', '2000', '263.25', '263.25', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '49', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('51', '0', '3012', 'MAYURBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'AUROPHARMA', '2016-02-29', '11:41:28', '1000', '649', '649', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '52', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('52', '0', '9999', 'zz', '', '', '3012', 'MAYURBHAI', '', '', 'Buy', 'AUROPHARMA', '2016-02-29', '11:41:28', '1000', '649', '649', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '51', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('53', '0', '3020', 'RAVIBHAI', '', '', '9999', 'zz', '', '', 'Buy', 'BANKNIFTY', '2016-02-29', '11:41:44', '25', '13870', '13870', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '54', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('54', '0', '9999', 'zz', '', '', '3020', 'RAVIBHAI', '', '', 'Sell', 'BANKNIFTY', '2016-02-29', '11:41:44', '25', '13870', '13870', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '53', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('55', '0', '3020', 'RAVIBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'DLF', '2016-02-29', '11:42:09', '2500', '88.4', '88.4', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '56', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('56', '0', '9999', 'zz', '', '', '3020', 'RAVIBHAI', '', '', 'Buy', 'DLF', '2016-02-29', '11:42:09', '2500', '88.4', '88.4', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '55', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('57', '0', '3020', 'RAVIBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'HINDALCO', '2016-02-29', '11:42:28', '2500', '70.7', '70.7', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '58', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('58', '0', '9999', 'zz', '', '', '3020', 'RAVIBHAI', '', '', 'Buy', 'HINDALCO', '2016-02-29', '11:42:28', '2500', '70.7', '70.7', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '57', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('59', '0', '3020', 'RAVIBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'AXISBANK', '2016-02-29', '11:43:01', '200', '389.95', '389.95', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '60', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('60', '0', '9999', 'zz', '', '', '3020', 'RAVIBHAI', '', '', 'Buy', 'AXISBANK', '2016-02-29', '11:43:01', '200', '389.95', '389.95', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '59', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('61', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Buy', 'AXISBANK', '2016-02-29', '11:43:12', '5000', '390.2', '390.2', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '62', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('62', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Sell', 'AXISBANK', '2016-02-29', '11:43:12', '5000', '390.2', '390.2', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '61', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('63', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Sell', 'ICICIBANK', '2016-02-29', '11:43:24', '5000', '188.5', '188.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '64', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('64', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Buy', 'ICICIBANK', '2016-02-29', '11:43:24', '5000', '188.5', '188.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '63', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('65', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'JETAIRWAYS', '2016-02-29', '11:43:35', '10000', '491.75', '491.75', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '66', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('66', '0', '9999', 'zz', '', '', '3095', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'JETAIRWAYS', '2016-02-29', '11:43:35', '10000', '491.75', '491.75', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '65', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('67', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'JETAIRWAYS', '2016-02-29', '11:43:49', '5000', '493.55', '493.55', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '68', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('68', '0', '9999', 'zz', '', '', '3095', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'JETAIRWAYS', '2016-02-29', '11:43:49', '5000', '493.55', '493.55', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '67', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('69', '0', '3042', 'LALABHAI', 'UNA', '', '9999', 'zz', '', '', 'Sell', 'TECHM', '2016-02-29', '11:44:00', '700', '416.2', '416.2', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '70', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('70', '0', '9999', 'zz', '', '', '3042', 'LALABHAI', 'UNA', '', 'Buy', 'TECHM', '2016-02-29', '11:44:00', '700', '416.2', '416.2', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '69', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('71', '0', '3083', 'HITESHBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'TATASTEEL', '2016-02-29', '11:44:26', '250', '255', '255', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '72', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('72', '0', '9999', 'zz', '', '', '3083', 'HITESHBHAI', '', '', 'Buy', 'TATASTEEL', '2016-02-29', '11:44:26', '250', '255', '255', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '71', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('73', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'ITC', '2016-02-29', '11:44:38', '500', '289.65', '289.65', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '74', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('74', '0', '9999', 'zz', '', '', '3095', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'ITC', '2016-02-29', '11:44:38', '500', '289.65', '289.65', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '73', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('75', '0', '3095', 'PINAKINBHAI', 'PHOPHANI', '', '9999', 'zz', '', '', 'Sell', 'JETAIRWAYS', '2016-02-29', '11:44:47', '500', '488.5', '488.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '76', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('76', '0', '9999', 'zz', '', '', '3095', 'PINAKINBHAI', 'PHOPHANI', '', 'Buy', 'JETAIRWAYS', '2016-02-29', '11:44:47', '500', '488.5', '488.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '75', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('77', '0', '3042', 'LALABHAI', 'UNA', '', '9999', 'zz', '', '', 'Sell', 'TECHM', '2016-02-29', '11:46:52', '300', '416', '416', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '78', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('78', '0', '9999', 'zz', '', '', '3042', 'LALABHAI', 'UNA', '', 'Buy', 'TECHM', '2016-02-29', '11:46:52', '300', '416', '416', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '77', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('79', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Sell', 'LT', '2016-02-29', '11:47:13', '700', '1117.5', '1117.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '80', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('80', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Buy', 'LT', '2016-02-29', '11:47:13', '700', '1117.5', '1117.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '79', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('81', '0', '3083', 'HITESHBHAI', '', '', '9999', 'zz', '', '', 'Buy', 'NIFTY', '2016-02-29', '11:47:42', '50', '7026', '7026', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '82', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('82', '0', '9999', 'zz', '', '', '3083', 'HITESHBHAI', '', '', 'Sell', 'NIFTY', '2016-02-29', '11:47:42', '50', '7026', '7026', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '81', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('83', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Sell', 'LT', '2016-02-29', '11:47:52', '200', '1120', '1120', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '84', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('84', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Buy', 'LT', '2016-02-29', '11:47:52', '200', '1120', '1120', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '83', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('85', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Sell', 'HDFC', '2016-02-29', '11:48:33', '1000', '1058', '1058', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '86', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('86', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Buy', 'HDFC', '2016-02-29', '11:48:33', '1000', '1058', '1058', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '85', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('87', '0', '10010', 'SANDYBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'RELCAPITAL', '2016-02-29', '11:49:41', '20000', '325.4', '325.4', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '88', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('88', '0', '9999', 'zz', '', '', '10010', 'SANDYBHAI', '', '', 'Buy', 'RELCAPITAL', '2016-02-29', '11:49:41', '20000', '325.4', '325.4', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '87', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('89', '0', '3020', 'RAVIBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'JSWSTEEL', '2016-02-29', '11:50:03', '100', '1116.85', '1116.85', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '90', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('90', '0', '9999', 'zz', '', '', '3020', 'RAVIBHAI', '', '', 'Buy', 'JSWSTEEL', '2016-02-29', '11:50:03', '100', '1116.85', '1116.85', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '89', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('91', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Sell', 'HDFC', '2016-02-29', '11:50:13', '500', '1060', '1060', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '92', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('92', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Buy', 'HDFC', '2016-02-29', '11:50:13', '500', '1060', '1060', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '91', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('93', '0', '3020', 'RAVIBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'BANKNIFTY', '2016-02-29', '11:50:37', '25', '13932', '13932', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '94', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('94', '0', '9999', 'zz', '', '', '3020', 'RAVIBHAI', '', '', 'Buy', 'BANKNIFTY', '2016-02-29', '11:50:37', '25', '13932', '13932', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '93', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('95', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Buy', 'INDUSINDBK', '2016-02-29', '11:51:23', '1500', '816.9', '816.9', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '96', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('96', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Sell', 'INDUSINDBK', '2016-02-29', '11:51:23', '1500', '816.9', '816.9', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '95', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('97', '0', '3020', 'RAVIBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'ICICIBANK', '2016-02-29', '11:51:33', '1000', '189', '189', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '98', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('98', '0', '9999', 'zz', '', '', '3020', 'RAVIBHAI', '', '', 'Buy', 'ICICIBANK', '2016-02-29', '11:51:33', '1000', '189', '189', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '97', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('99', '0', '3020', 'RAVIBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'BANKNIFTY', '2016-02-29', '11:51:45', '100', '13936.3', '13936.3', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '100', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('100', '0', '9999', 'zz', '', '', '3020', 'RAVIBHAI', '', '', 'Buy', 'BANKNIFTY', '2016-02-29', '11:51:45', '100', '13936.3', '13936.3', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '99', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('101', '0', '3042', 'LALABHAI', 'UNA', '', '9999', 'zz', '', '', 'Sell', 'BHEL', '2016-02-29', '11:51:59', '1000', '96.25', '96.25', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '102', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('102', '0', '9999', 'zz', '', '', '3042', 'LALABHAI', 'UNA', '', 'Buy', 'BHEL', '2016-02-29', '11:51:59', '1000', '96.25', '96.25', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '101', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('103', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Buy', 'INDUSINDBK', '2016-02-29', '11:52:09', '1000', '815.5', '815.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '104', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('104', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Sell', 'INDUSINDBK', '2016-02-29', '11:52:09', '1000', '815.5', '815.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '103', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('105', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Sell', 'YESBANK', '2016-02-29', '11:52:22', '1000', '685.6', '685.6', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '106', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('106', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Buy', 'YESBANK', '2016-02-29', '11:52:22', '1000', '685.6', '685.6', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '105', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('107', '0', '3042', 'LALABHAI', 'UNA', '', '9999', 'zz', '', '', 'Buy', 'NIFTY', '2016-02-29', '11:52:34', '500', '7037', '7037', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '108', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('108', '0', '9999', 'zz', '', '', '3042', 'LALABHAI', 'UNA', '', 'Sell', 'NIFTY', '2016-02-29', '11:52:34', '500', '7037', '7037', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '107', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('109', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Sell', 'RELIANCE', '2016-02-29', '11:52:46', '1000', '956.55', '956.55', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '110', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('110', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Buy', 'RELIANCE', '2016-02-29', '11:52:46', '1000', '956.55', '956.55', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '109', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('111', '0', '10008', 'MAHESHBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'PNB', '2016-02-29', '11:52:54', '500', '73.8', '73.8', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '112', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('112', '0', '9999', 'zz', '', '', '10008', 'MAHESHBHAI', '', '', 'Buy', 'PNB', '2016-02-29', '11:52:54', '500', '73.8', '73.8', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '111', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('113', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Sell', 'NIFTY', '2016-02-29', '11:53:03', '100', '7085.4', '7085.4', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '114', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('114', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Buy', 'NIFTY', '2016-02-29', '11:53:03', '100', '7085.4', '7085.4', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '113', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('115', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Sell', 'HDFC', '2016-02-29', '11:53:14', '200', '1064.75', '1064.75', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '116', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('116', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Buy', 'HDFC', '2016-02-29', '11:53:14', '200', '1064.75', '1064.75', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '115', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('117', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Sell', 'RELIANCE', '2016-02-29', '11:53:24', '500', '764.5', '764.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '118', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('118', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Buy', 'RELIANCE', '2016-02-29', '11:53:24', '500', '764.5', '764.5', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '117', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('119', '0', '10008', 'MAHESHBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'RELIANCE', '2016-02-29', '11:53:35', '100', '963.3', '963.3', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '120', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('120', '0', '9999', 'zz', '', '', '10008', 'MAHESHBHAI', '', '', 'Buy', 'RELIANCE', '2016-02-29', '11:53:35', '100', '963.3', '963.3', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '119', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('121', '0', '3083', 'HITESHBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'ONGC', '2016-02-29', '11:53:45', '100', '217', '217', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '122', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('122', '0', '9999', 'zz', '', '', '3083', 'HITESHBHAI', '', '', 'Buy', 'ONGC', '2016-02-29', '11:53:45', '100', '217', '217', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '121', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('123', '0', '3083', 'HITESHBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'SBIN', '2016-02-29', '11:53:53', '100', '162.95', '162.95', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '124', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('124', '0', '9999', 'zz', '', '', '3083', 'HITESHBHAI', '', '', 'Buy', 'SBIN', '2016-02-29', '11:53:53', '100', '162.95', '162.95', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '123', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('125', '0', '3038', 'KAMLESHBHAO ', 'SONI', '', '9999', 'zz', '', '', 'Sell', 'HDFC', '2016-02-29', '11:54:05', '1500', '1066.65', '1066.65', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '126', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('126', '0', '9999', 'zz', '', '', '3038', 'KAMLESHBHAO ', 'SONI', '', 'Buy', 'HDFC', '2016-02-29', '11:54:05', '1500', '1066.65', '1066.65', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '125', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('127', '0', '3091', 'DHRUBHAI', '', '', '9999', 'zz', '', '', 'Buy', 'BHARATFORG', '2016-02-29', '11:54:18', '2500', '744.2', '744.2', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '128', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('128', '0', '9999', 'zz', '', '', '3091', 'DHRUBHAI', '', '', 'Sell', 'BHARATFORG', '2016-02-29', '11:54:18', '2500', '744.2', '744.2', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '127', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('129', '0', '10008', 'MAHESHBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'PNB', '2016-02-29', '11:55:06', '1000', '73.05', '73.05', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '130', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('130', '0', '9999', 'zz', '', '', '10008', 'MAHESHBHAI', '', '', 'Buy', 'PNB', '2016-02-29', '11:55:06', '1000', '73.05', '73.05', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '129', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('131', '0', '3042', 'LALABHAI', 'UNA', '', '9999', 'zz', '', '', 'Sell', 'TECHM', '2016-02-29', '11:56:18', '500', '417', '417', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '132', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('132', '0', '9999', 'zz', '', '', '3042', 'LALABHAI', 'UNA', '', 'Buy', 'TECHM', '2016-02-29', '11:56:18', '500', '417', '417', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '131', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('133', '0', '10008', 'MAHESHBHAI', '', '', '9999', 'zz', '', '', 'Buy', 'NIFTY', '2016-02-29', '11:56:27', '50', '7009', '7009', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '134', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('134', '0', '9999', 'zz', '', '', '10008', 'MAHESHBHAI', '', '', 'Sell', 'NIFTY', '2016-02-29', '11:56:27', '50', '7009', '7009', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '133', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('135', '0', '3020', 'RAVIBHAI', '', '', '9999', 'zz', '', '', 'Buy', 'INFOSYSTCH', '2016-02-29', '11:56:43', '400', '1101.9', '1101.9', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '136', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('136', '0', '9999', 'zz', '', '', '3020', 'RAVIBHAI', '', '', 'Sell', 'INFOSYSTCH', '2016-02-29', '11:56:43', '400', '1101.9', '1101.9', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '135', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('137', '0', '3042', 'LALABHAI', 'UNA', '', '9999', 'zz', '', '', 'Sell', 'TECHM', '2016-02-29', '11:56:54', '500', '416.6', '416.6', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '138', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('138', '0', '9999', 'zz', '', '', '3042', 'LALABHAI', 'UNA', '', 'Buy', 'TECHM', '2016-02-29', '11:56:54', '500', '416.6', '416.6', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '137', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('139', '0', '10008', 'MAHESHBHAI', '', '', '9999', 'zz', '', '', 'Sell', 'NIFTY', '2016-02-29', '11:57:04', '50', '7053.1', '7053.1', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '140', '0');
insert into tradetxt (tradeId, standing, clientId, firstName, middleName, lastName, clientId2, firstName2, middleName2, lastName2, buySell, itemId, tradeDate, tradeTime, qty, price, price2, brok, tradeRefNo, tradeNote, expiryDate, vendor, userRemarks, ownClient, confirmed, exchange, refTradeId, selfRefId) values ('140', '0', '9999', 'zz', '', '', '10008', 'MAHESHBHAI', '', '', 'Buy', 'NIFTY', '2016-02-29', '11:57:04', '50', '7053.1', '7053.1', NULL, NULL, NULL, '31MAR2016', '_SELF', NULL, NULL, '0', 'F_O', '139', '0');
drop table if exists tradetxtv1;
create table tradetxtv1 (
  tradeId int(6) not null auto_increment,
  standing tinyint(1) default '0' ,
  clientId int(6) default '0' ,
  firstName varchar(35) ,
  middleName varchar(35) ,
  lastName varchar(35) ,
  buySell varchar(10) ,
  itemId varchar(50) ,
  tradeDate date ,
  tradeTime varchar(20) ,
  qty int(6) ,
  price float ,
  brok int(6) ,
  tradeRefNo varchar(60) ,
  tradeNote varchar(200) ,
  expiryDate varchar(20) ,
  vendor varchar(50) ,
  userRemarks varchar(50) ,
  ownClient varchar(50) ,
  PRIMARY KEY (tradeId)
);

drop table if exists user;
create table user (
  clientId varchar(50) not null ,
  name varchar(50) not null ,
  password varchar(50) not null ,
  deletepassword varchar(50) not null ,
  userType varchar(20) 
);

insert into user (clientId, name, password, deletepassword, userType) values ('', 'shree', '258', '', NULL);
drop table if exists vendor;
create table vendor (
  vendorId int(6) not null auto_increment,
  vendor varchar(50) ,
  firstName varchar(35) ,
  middleName varchar(35) ,
  lastName varchar(35) ,
  address text ,
  phone varchar(30) ,
  mobile varchar(22) ,
  fax varchar(30) ,
  email varchar(60) ,
  deposit float ,
  currentBal float ,
  PRIMARY KEY (vendorId)
);

insert into vendor (vendorId, vendor, firstName, middleName, lastName, address, phone, mobile, fax, email, deposit, currentBal) values ('6', '_SELF', '_SELF', '', '', '', '', '', '', '', '0', NULL);
drop table if exists vendorbrok;
create table vendorbrok (
  clientBrokId int(6) not null auto_increment,
  vendor varchar(50) ,
  itemId varchar(50) ,
  oneSideBrok int(6) ,
  brok1 float ,
  brok2 float ,
  PRIMARY KEY (clientBrokId)
);

insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('96', '_SELF', 'SILVER', '0', NULL, NULL);
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('97', '_SELF', 'GOLD', '0', NULL, NULL);
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('98', '_SELF', 'CRUDEOIL', '0', NULL, NULL);
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('99', '_SELF', 'COPPER', '0', NULL, NULL);
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('100', '_SELF', 'GOLDM', '0', NULL, NULL);
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('101', '_SELF', 'SILVERM', '0', NULL, NULL);
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('102', '_SELF', 'NATURALGAS', '0', NULL, NULL);
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('103', '_SELF', 'ZINC', '0', NULL, NULL);
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('104', '_SELF', 'NICKEL', '0', NULL, NULL);
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('105', '_SELF', 'MENTHAOIL', '0', NULL, NULL);
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('106', '_SELF', 'POTATO', '0', NULL, NULL);
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('107', '_SELF', 'NIFTY', '0', NULL, NULL);
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('108', '_SELF', 'LEAD', '0', NULL, NULL);
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('109', '_SELF', 'NIFTY', '10', '0', '0');
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('110', '_SELF', 'NIFTY', '10', '0', '0');
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('111', '_SELF', 'NIFTY', '10', '0', '0');
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('112', '_SELF', 'NIFTY', '10', '0', '0');
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('113', '_SELF', 'REL', '1', '0', '0');
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('114', '_SELF', 'REL', '1', '0', '0');
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('115', '_SELF', 'qwert', '12', '0', '0');
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('116', '_SELF', 'qwert', '12', '0', '0');
insert into vendorbrok (clientBrokId, vendor, itemId, oneSideBrok, brok1, brok2) values ('117', '_SELF', 'GGG', '300', '0', '0');
drop table if exists vendortemp;
create table vendortemp (
  tradeId int(6) not null auto_increment,
  standing tinyint(1) default '0' ,
  clientId int(6) default '0' ,
  firstName varchar(35) ,
  middleName varchar(35) ,
  lastName varchar(35) ,
  buySell varchar(10) ,
  itemId varchar(50) ,
  tradeDate date ,
  tradeTime varchar(20) ,
  qty int(6) ,
  price float ,
  brok int(6) ,
  tradeRefNo varchar(60) ,
  tradeNote varchar(200) ,
  expiryDate varchar(20) ,
  vendor varchar(50) ,
  userRemarks varchar(50) ,
  ownClient varchar(50) ,
  confirmed tinyint(4) default '0' not null ,
  PRIMARY KEY (tradeId)
);

drop table if exists vendortrades;
create table vendortrades (
  tradeId int(6) not null auto_increment,
  standing tinyint(1) default '0' ,
  clientId int(6) default '0' ,
  firstName varchar(35) ,
  middleName varchar(35) ,
  lastName varchar(35) ,
  buySell varchar(10) ,
  itemId varchar(50) ,
  tradeDate date ,
  tradeTime varchar(20) ,
  qty int(6) ,
  price float ,
  brok int(6) ,
  tradeRefNo varchar(60) ,
  tradeNote varchar(200) ,
  expiryDate varchar(20) ,
  vendor varchar(50) ,
  userRemarks varchar(50) ,
  ownClient varchar(50) ,
  confirmed tinyint(4) default '0' not null ,
  PRIMARY KEY (tradeId)
);

drop table if exists zcxexpiry;
create table zcxexpiry (
  expiryId int(6) not null auto_increment,
  itemId varchar(50) ,
  expiryDate varchar(20) ,
  PRIMARY KEY (expiryId)
);

drop table if exists zcxitem;
create table zcxitem (
  itemId varchar(10) not null ,
  item varchar(50) ,
  oneSideBrok float default '0' ,
  mulAmount float default '0' ,
  minQty float ,
  brok int(6) default '1' ,
  brok2 int(6) default '1' ,
  per int(6) default '1' ,
  unit int(6) default '1' ,
  min int(6) default '1' ,
  priceOn int(6) default '1' ,
  priceUnit int(6) default '1' ,
  PRIMARY KEY (itemId)
);

drop table if exists zcxmember;
create table zcxmember (
  zCxMemberId int(6) not null auto_increment,
  userId varchar(10) ,
  memberId varchar(10) ,
  PRIMARY KEY (zCxMemberId)
);

drop table if exists zcxstanding;
create table zcxstanding (
  standingId int(6) not null auto_increment,
  standingDtCurrent date ,
  standingDtNext date ,
  itemIdExpiryDate varchar(50) ,
  standingPrice float ,
  UNIQUE standingId (standingId)
);

drop table if exists zcxtrades;
create table zcxtrades (
  tradeId int(6) not null auto_increment,
  standing tinyint(1) ,
  clientId int(6) default '0' ,
  firstName varchar(35) ,
  middleName varchar(35) ,
  lastName varchar(35) ,
  buySell varchar(10) ,
  itemId varchar(50) ,
  tradeDate date ,
  tradeTime varchar(20) ,
  qty int(6) ,
  price float ,
  brok int(6) ,
  tradeRefNo varchar(60) ,
  tradeNote varchar(200) ,
  expiryDate varchar(20) ,
  vendor varchar(10) ,
  removeFromAccount tinyint(4) ,
  PRIMARY KEY (tradeId)
);

