<?php
  if(isset($_GET['fromDateYear']))
  {
    session_register('toDate');
    $_SESSION['fromDate']       = $_GET['fromDateYear']."-".$_GET['fromDateMonth']."-".$_GET['fromDateDay'];
    $_SESSION['toDate']         = $_GET['toDateYear']."-".$_GET['toDateMonth']."-".$_GET['toDateDay'];
  }
  else
  {
    if($cfgWeekStartDay == 6)
    {
      $day = date("w");
      
      if($day == 6)
        $fromDateMinus = 7;
      elseif($day == 0)
        $fromDateMinus = 8;
      else
        $fromDateMinus =  $day + 1;
      $fromDate = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")-$fromDateMinus, date("Y")));
      
      if($day == 6)
        $toDateMinus = 1;
      elseif($day == 0)
        $toDateMinus = 2;
      else
        $toDateMinus = 0;
      
      $today = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")-$toDateMinus, date("Y")));
      $_SESSION['fromDate']       = $fromDate;
      $_SESSION['toDate']         = $today;
    }
    else
    {
      $day = date("w");
      
      if($day == 0)
        $fromDateMinus =  6;
      else
        $fromDateMinus =  $day - 1;
      $fromDate = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")-$fromDateMinus, date("Y")));
      
      if($day == 0)
        $toDateMinus = 1;
      else
        $toDateMinus = 0;
      
      $today = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")-$toDateMinus, date("Y")));
      $_SESSION['fromDate']       = $fromDate;
      $_SESSION['toDate']         = $today;
    }
  }
?>