<?php
  include "./etc/om_config.inc";
  include "./etc/functions.inc";
  $smarty=new SmartyWWW();
  $selectExp = "SELECT * FROM otherincome";
  $resultExp = mysql_query($selectExp);
  $i=0;
  $totalExp =0;
  while($rowExp = mysql_fetch_array($resultExp))
  {
  	$otherIncomId[$i]   = $rowExp['otherIncomId'];
  	$otherIncomName[$i] = $rowExp['otherIncomName'];
  	$otherIncomDate[$i] = mysqlToDDMMYY($rowExp['otherIncomDate']);
  	$otherIncomAmount[$i] = $rowExp['otherIncomAmount'];
  	$totalExp += $rowExp['otherIncomAmount'];
  	$otherIncomMode[$i] = $rowExp['otherIncomMode'];
  	$otherIncomNote[$i] = $rowExp['note'];
  	$i++;
  }
  $smarty->assign("otherIncomId",$otherIncomId);
  $smarty->assign("otherIncomMode",$otherIncomMode);
  $smarty->assign("otherIncomName",$otherIncomName);
  $smarty->assign("otherIncomDate",$otherIncomDate);
  $smarty->assign("otherIncomAmount",$otherIncomAmount);
  $smarty->assign("otherIncomNote",$otherIncomNote);
  $smarty->assign("totalExp",$totalExp);
  $smarty->assign("i",$i);
  
  $smarty->display("otherIncomList.tpl");
?>