<?php
include "etc/om_config.inc";
session_start();

$goTo = "commisionInCalc";
if(!isset($_SESSION['toDate']))
  header("Location: selectDtSession.php?goTo=".$goTo);
else
{
  
}

//Client Selection :Start
$clientIdSelected = isset($_REQUEST['clientId']) ? $_REQUEST['clientId'] : 0;
$client   = array();
$display  = array();
$clientTotal = 0;
$grandTotal  = 0;
$totalBrok   = 0;
$clientName  = "";

$clientQuery = "SELECT clientId, firstName, middleName, lastName FROM client
                 ORDER BY firstName, middleName, lastName";
$clientResult = mysql_query($clientQuery);
$i = 0;
while($clientRow = mysql_fetch_assoc($clientResult))
{
  if($i == 0 && !isset($_REQUEST['clientId']))
    $clientIdSelected = $clientRow['clientId'];
  $client['id'][$i]    = $clientRow['clientId'];
  $client['name'][$i]  = $clientRow['firstName']." ".$clientRow['middleName']." ".$clientRow['lastName'];
  $i++;
}
//Client Selection :End

$selectClientExchange = "SELECT clientId,exchange,commissionIn,commission FROM clientexchange
                          WHERE parentClientId = ".$clientIdSelected."
                          ORDER BY clientId";
$selectClientExchangeRes = mysql_query($selectClientExchange);
$a = 0;
while($clientExchangeRow = mysql_fetch_assoc($selectClientExchangeRes))
{
  $selectClient = "SELECT firstName,middleName,lastName FROM client
                    WHERE clientId = ".$clientExchangeRow['clientId'];
  $selectClientRes = mysql_query($selectClient);
  $userRow = mysql_fetch_assoc($selectClientRes);
  $clientName = $userRow['firstName']." ".$userRow['middleName']." ".$userRow['lastName'];
  
  $selectBrokRecieved = "SELECT brokRecievedId,brokDate,clientId,exchange,brok,lot FROM brokrecieved
                          WHERE brokDate <= CURDATE()
                            AND clientId = ".$clientExchangeRow['clientId']."
                            AND exchange= '".$clientExchangeRow['exchange']."'";
  $selectBrokRecievedRes = mysql_query($selectBrokRecieved);
  while($brokReciveRow = mysql_fetch_assoc($selectBrokRecievedRes))
  {
    if($clientExchangeRow['commissionIn'] == 'P')
      $totalBrok = ($brokReciveRow['brok'] * $clientExchangeRow['commission'])/100;
    elseif($clientExchangeRow['commissionIn'] == 'L')
      $totalBrok = $brokReciveRow['lot'] * $clientExchangeRow['commission'];

    $display[$a]['client']   = $clientName;
    $display[$a]['exchange'] = $brokReciveRow['exchange'];
    $display[$a]['total']    = $totalBrok;
    $a++;
  }
  $grandTotal += $totalBrok;
}

$smarty = new SmartyWWW();
$smarty->assign("clientIdSelected",$clientIdSelected);
$smarty->assign("client",$client);
$smarty->assign("clientTotal",$clientTotal);
$smarty->assign("display",$display);
$smarty->assign("grandTotal",$grandTotal);
$smarty->display("commisionInCalc.tpl");
?>