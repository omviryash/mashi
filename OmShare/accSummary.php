<?php
session_start();
if(!isset($_SESSION['toDate']))
{
  header("Location: selectDtSession.php?goTo=accSummary");
}
else
{
  include "./etc/om_config.inc";

  $smarty=new SmartyWWW();
  $clientArray = array();
  $clientQuery = "SELECT * FROM client
                   ORDER BY firstName,middleName,lastName";
  $clientResult = mysql_query($clientQuery);

  $creditTotal      = 0;
  $debitTotal       = 0;
  $grandTotal       = 0;
  $billAmountTotal  = 0;
  $openingTotal = 0;
  $crDrSelected     = "All";
  if(isset($_SESSION['ses_decidePackFor']) && $_SESSION['ses_decidePackFor'] == 7 )
    $selectedExchange = "F_O";
  else if(isset($_SESSION['ses_decidePackFor']) && $_SESSION['ses_decidePackFor'] == 6 )
    $selectedExchange = "MCX";
  else
    $selectedExchange = "All";
  $exchange           = array();
  if(isset($_POST['exchange']))
  {
    $selectedExchange = $_POST['exchange'];
    $crDrSelected     = $_POST['crDr'];
  }
  $i = 0;
  while($clientRow = mysql_fetch_assoc($clientResult))
  {
    $clientArray[$i]['dispThisClient'] = 0;
    $clientArray[$i]['clientId']   = $clientRow['clientId'];
    $clientArray[$i]['firstName']  = $clientRow['firstName'];
    $clientArray[$i]['middleName'] = $clientRow['middleName'];
    $clientArray[$i]['lastName']   = $clientRow['lastName'];
    $clientArray[$i]['balance']    = 0;
    $clientArray[$i]['opening']    = 0;
    $clientArray[$i]['billAmount'] = 0;
    $clientArray[$i]['billAmountToDisp'] = "";
    $clientArray[$i]['dwStatus']   = "";

    // For Opening :Start
    $openingQuery = "SELECT * FROM cashflow
                       WHERE clientId = ".$clientRow['clientId']."
                         AND transactionDate < '".$_SESSION['fromDate']."'";
    if($selectedExchange != "All")
      $openingQuery .= " AND exchange = '".$selectedExchange."';";
    $openingQueryResult = mysql_query($openingQuery);
    while($openingRow = mysql_fetch_assoc($openingQueryResult))
    {
      if($openingRow['dwStatus'] != 'd')
        $clientArray[$i]['opening'] = number_format($clientArray[$i]['opening'] - $openingRow['dwAmount'],2,".","");
      else
        $clientArray[$i]['opening'] = number_format($clientArray[$i]['opening'] + $openingRow['dwAmount'],2,".","");
      if($openingRow['plStatus'] != 'p')
        $clientArray[$i]['opening'] = number_format($clientArray[$i]['opening'] + $openingRow['plAmount'],2,".","");
      else
        $clientArray[$i]['opening'] = number_format($clientArray[$i]['opening'] - $openingRow['plAmount'],2,".","");
    }
    if($crDrSelected == "Cr" && $clientArray[$i]['opening'] >= 0)
      $openingTotal += $clientArray[$i]['opening'];
    if($crDrSelected == "Dr" && $clientArray[$i]['opening'] < 0)
      $openingTotal += $clientArray[$i]['opening'];
    if($crDrSelected == "All")
      $openingTotal += $clientArray[$i]['opening'];
    // For Opening :End
    if($clientArray[$i]['opening'] != 0)//we want display client if any amount is pending 
      $clientArray[$i]['dispThisClient'] = 1;

    $cashFlowQuery = "SELECT * FROM cashflow
                       WHERE clientId = ".$clientRow['clientId']."
                         AND transactionDate <= '".$_SESSION['toDate']."'";
    if($selectedExchange != "All")
      $cashFlowQuery .= " AND exchange = '".$selectedExchange."'";
    $cashFlowQuery .= " ORDER BY transactionDate;";
    $cashFlowResult = mysql_query($cashFlowQuery);
    while($cashFlowRow = mysql_fetch_assoc($cashFlowResult))
    {
      if($cashFlowRow['transactionDate'] >= $_SESSION['fromDate'])//we want display client if any transaction done between current date range
        $clientArray[$i]['dispThisClient'] = 1;
      $clientArray[$i]['dwStatus'] = $cashFlowRow['dwStatus'];
      if($cashFlowRow['dwStatus'] != 'd')
        $clientArray[$i]['balance'] = number_format($clientArray[$i]['balance'] - $cashFlowRow['dwAmount'],2,".","");
      else
        $clientArray[$i]['balance'] = number_format($clientArray[$i]['balance'] + $cashFlowRow['dwAmount'],2,".","");
      if($cashFlowRow['plStatus'] != 'p')
        $clientArray[$i]['balance'] = number_format($clientArray[$i]['balance'] + $cashFlowRow['plAmount'],2,".","");
      else
        $clientArray[$i]['balance'] = number_format($clientArray[$i]['balance'] - $cashFlowRow['plAmount'],2,".","");
        
      // Overwrite each bill... because we want to display only last bill
      if($cashFlowRow['transMode'] = "Bill" && $cashFlowRow['transactionDate'] >= $_SESSION['fromDate'])
      {
        $clientArray[$i]['billAmount']       = number_format(($cashFlowRow['dwStatus'] == "d" ? "" : "-").$cashFlowRow['dwAmount'],2,".","");
        $clientArray[$i]['billAmountToDisp'] = number_format($clientArray[$i]['billAmount'],2,".","");
      }
    }
    if($clientArray[$i]['balance'] >= 0)
      $creditTotal                += $clientArray[$i]['balance'];
    else
      $debitTotal                 += $clientArray[$i]['balance'];

    if($crDrSelected == "Cr" && $clientArray[$i]['billAmount'] >= 0)
      $billAmountTotal += $clientArray[$i]['billAmount'];
    if($crDrSelected == "Dr" && $clientArray[$i]['billAmount'] < 0)
      $billAmountTotal += $clientArray[$i]['billAmount'];
    if($crDrSelected == "All")
      $billAmountTotal += $clientArray[$i]['billAmount'];
    $i++;
  }
  $grandTotal = $creditTotal + $debitTotal;

  $selectExchange = "SELECT exchangeId,exchange FROM exchange
                      ORDER BY exchange";
  $selectExchangeRes = mysql_query($selectExchange);
  $a = 0;
  $exchange['id'][$a] = 0;
  $exchange['name'][$a] = "All";
  $a++;
  while($exchangeRow = mysql_fetch_object($selectExchangeRes))
  {
    $exchange['id'][$a]   = $exchangeRow->exchangeId;
    $exchange['name'][$a] = $exchangeRow->exchange;
    $a++;
  }

  $smarty->assign("toDate",$_SESSION['toDate']);
  $smarty->assign("fromDate",$_SESSION['fromDate']);
  $smarty->assign("PHP_SELF",$_SERVER['PHP_SELF']);
  $smarty->assign("cfgDispOpeningAccSmry",$cfgDispOpeningAccSmry);
  $smarty->assign("cfgDispCurBillAccSmry",$cfgDispCurBillAccSmry);
  $smarty->assign("clientArray",$clientArray);
  $smarty->assign("creditTotal",$creditTotal);
  $smarty->assign("debitTotal",$debitTotal);
  $smarty->assign("grandTotal",$grandTotal);
  $smarty->assign("openingTotal",$openingTotal);
  $smarty->assign("billAmountTotal",$billAmountTotal);
  $smarty->assign("exchange",$exchange);
  $smarty->assign("crDrValues",array("All","Cr","Dr"));
  $smarty->assign("crDrOptions",array("All","Credit","Debit"));
  $smarty->assign("crDrSelected",$crDrSelected);
  $smarty->assign("selectedExchange",$selectedExchange);
  $smarty->display("accSummary.tpl");
}
?>