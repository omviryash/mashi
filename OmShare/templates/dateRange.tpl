<table border="0" cellPadding="2" cellSpacing="0">
<tr>
  <td>From : </td>
  <td>{html_select_date time="$fromDate" prefix="fromDate" start_year="-1" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
  </td>
  <td>To : </td>
  <td>
    {html_select_date time="$toDate" prefix="toDate" start_year="-1" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
  </td>
  <td>
    <input type="submit" value="Go" />
  </td>
  </TR>
</table>