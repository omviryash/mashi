<HTML>
<HEAD><TITLE>MCX Order Entry</TITLE>
<STYLE>
{literal}
td {  font-color="white";FONT-SIZE: 12px;}
INPUT {FONT-SIZE: 9px; }
SELECT {FONT-SIZE: 9Px; }
{/literal}
</STYLE>
<SCRIPT language="javascript">
{literal}
window.name = 'displayAll';
function change()
 {
  var select1value= document.form1.itemId.value;
  var select2value=document.form1.expiryDate;
  select2value.options.length=0;
{/literal}
   {section name=sec1 loop=$k}
    if( select1value=="{$itemId[sec1]}")
  	{literal}{{/literal}
  		{section name=sec2 loop=$l+10}
        {if $expiryDate[sec1][sec2] neq ""}
          select2value.options[{$smarty.section.sec2.index}]=new Option("{$expiryDate[sec1][sec2]}"); 
        {/if}
         {if $expiryDate[sec1][sec2] eq "$orderExpiryDateSelected"}
          select2value.options[{$smarty.section.sec2.index}].selected=true; 
        {/if}
      {/section}
    {literal}
    }
    {/literal}
  {/section}
  }
{literal}
function askConfirm()
{
  if(confirm("Are You Sure You want to Save Record?"))
  {
    document.form1.makeTrade.value=1;
    return true;
  }
  else
    return false;
}
function changeName()
{
  document.form1.changedField.value = "clientId";
  document.form1.submit();
}
function changeItem()
{
  document.form1.changedField.value = "itemId";
  document.form1.submit();
}

function changePrice()
{
  var price;
  price = parseFloat(document.form1.priceValue.value);
  if(parseFloat(price) || price==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      price=price+1;
    if(event.keyCode==33)
      price=price+10;
    if(event.keyCode==40)
      price=price-1;
    if(event.keyCode==34)
      price=price-10;
    if(document.form1.priceValue.value != price)
    {
      document.form1.priceValue.value = price;
      
    }
  }
  document.form1.price.value=price;
  //alert(document.form1.price.value);
}
function itemFromPrice()
{
  var price;
  price = parseFloat(document.form1.price.value);
{/literal}
{$itemFromPriceJS}
{literal}
}

function changeQty()
{
  {/literal}
  var minQty = {$minQty};
  {literal}
  var qty;
  qty = parseFloat(document.form1.qty.value);
  if(parseFloat(qty) || qty==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      qty=qty+minQty;
    if(event.keyCode==33)
      qty=qty+minQty*5;
    if(event.keyCode==40)
      qty=qty-minQty;
    if(event.keyCode==34)
      qty=qty-minQty*5;
    if(document.form1.qty.value != qty)
      document.form1.qty.value = qty;
  }
}
function buySellChange(buySell)
{
  if(buySell=='Buy')
  {
    window.document.bgColor="blue";
    document.form1.buySellText.value = "Buy";
    document.form1.buySell.value = "Buy";
  }
  else
  {
    window.document.bgColor="red";
    document.form1.buySellText.value = "Sell";
    document.form1.buySell.value = "Sell";
  }
}
function bodyKeyPress()
{
  //alert(event.keyCode);
  if(event.keyCode==112)
  {
    return false;
  }
  if(event.keyCode==107)
  {
    buySellChange('Buy');
    return false;
  }
  if(event.keyCode==109)
  {
    buySellChange('Sell');
    return false;
  }
  if(event.keyCode==117)
  {
    orderListWindow=window.open('orderList.php?listOnly=1', 'orderListWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
  }
  if(event.keyCode==119)
  {
    tradeListWindow=window.open('clientTrades.php', 'tradeListWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
  }
  if(event.keyCode==120)
  {
  	var price=parseFloat(document.form1.price.value);
  	if(document.form1.buySell.value=='Buy')
  	  price=price-1;
  	else
  		price=price+1;
    document.form1.price.value=price;  
    //alert(document.form1.price.value);
  }
  if(event.keyCode==121)
  {
  	var price=parseFloat(document.form1.price.value);
  	if(document.form1.buySell.value=='Buy')
  	  price=price-2;
  	else
  		price=price+2;
    document.form1.price.value=price; 
    //alert(document.form1.price.value); 
  }
  
}
function orderTypeChanged()
{
  if(document.form1.orderType.value == "SL")
    document.form1.triggerPrice.disabled = 0;
  else
    document.form1.triggerPrice.disabled = 1;
}
function orderValidityChange()
{
  if(document.form1.orderValidity.value == "GTD")
  {
    document.form1.gtdDateDay.disabled = 0;
    document.form1.gtdDateMonth.disabled = 0;
    document.form1.gtdDateYear.disabled = 0;
    
  }
  else
  {
    document.form1.gtdDateDay.disabled = 1;
    document.form1.gtdDateMonth.disabled = 1;
    document.form1.gtdDateYear.disabled = 1;
    
  }
}

{/literal}
</SCRIPT>
</HEAD>
<BODY bgColor="blue" onKeyDown=" return bodyKeyPress();" onLoad="changePrice();change();buySellChange('{$orderBuySellSelected}');orderValidityChange();">
  <FORM name="form1" action="{$PHP_SELF}" METHOD="post">
  <INPUT type="hidden" name="changedField" value="">
  <INPUT type="hidden" name="makeTrade" value="0">
  <INPUT type="hidden" name="firstName" value="{$firstName}">
  <INPUT type="hidden" name="middleName" value="{$middleName}">
  <INPUT type="hidden" name="lastName" value="{$lastName}">
  <INPUT type="hidden" name="forStand" value="{$forStand}">
  <INPUT type="hidden" name="price">
<FONT color="white">
      <SELECT name="orderType" onChange="orderTypeChanged();">
        {html_options selected="$orderTypeSelected" values="$orderTypeValues" output="$orderTypeOutput"}
      </SELECT>
    Date : 
      {html_select_date time="$tradeDateDisplay" prefix="trade" time=$orderDateSelected start_year="-1" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
      <SELECT name="itemId" onChange="change();">
      {html_options selected="$orderItemIdSelected" values="$itemIdValues" output="$itemIdOutput"}
      </SELECT>
      &nbsp;&nbsp;
      <SELECT name="expiryDate">
      {html_options selected="$orderExpiryDateSelected" values="$expiryDateValues" output="$expiryDateOutput"}
      </SELECT>
      &nbsp;
<!-- onBlur="itemFromPrice();"-->
      Qty : <INPUT type="text" name="qty" value="{$orderQtySelected}" onKeyDown="changeQty();" size="5">
      &nbsp;
      Price : <INPUT size="10" type="text" name="priceValue" value="{$orderPriceSelected}" onKeydown="changePrice();">&nbsp;&nbsp;
      <BR>
      TrigPrice : <INPUT size="10" type="text" name="triggerPrice" value="{$orderTriggerPriceSelected}" DISABLED>&nbsp;&nbsp;
{if $forStand == 1}
      <INPUT type="radio" name="standing" value="-1"> Open Standing
      <INPUT type="radio" name="standing" value="1"> Close Standing
{/if}
      <SELECT name="orderValidity" onChange="orderValidityChange();">
        {html_options selected=$orderValiditySelected values=$orderValidityValues output=$orderValidityOutput}
      </SELECT>
      
      {html_select_date prefix="gtdDate" start_year=-2 end_year=+2 time=$orderValidTillDateSelected field_order="DMY"}
      
      <INPUT type="hidden" name="vendor" value="_SELF">
      
      User Remarks : 
      <INPUT type="text" name="clientId" value="{$orderUserRemarkSelected}" size="9">
      <INPUT type="text" name="vendorId" value="{$vendorId}" size="3">
      <INPUT type="submit" name="tradeBtn" value="Trade" onClick="return askConfirm();">
      &nbsp;
      <INPUT type="submit" name="limitBtn" value="Limit" onClick="return askConfirm();">
      &nbsp;
      <BR>
      <INPUT DISABLED type="text" name="buySellText" value={$orderBuySellSelected} size="5">
      <INPUT type="hidden" name="buySell" value={$orderBuySellSelected}>
      <INPUT type="hidden" name="orderId" value={$orderIdSelected}>
<!--       <B>{$clientWholeName} : </B>
       Deposit : {$deposit}&nbsp;&nbsp;&nbsp;&nbsp;
       CurrentBal : {$currentBal}&nbsp;&nbsp;&nbsp;&nbsp;
       Total : {$total}&nbsp;&nbsp;&nbsp;&nbsp;  
       Phone   : {$phone}&nbsp;&nbsp;&nbsp;&nbsp;
       Mobile  : {$mobile}&nbsp;
      <BR>-->
<TABLE>
    <TR bgcolor="#D3D3D3">
      <TD>Last trade : </TD>
      <TH>{$lastTradeInfoVar}</TH>
    </TR>
</TABLE>
  {$focusScript}
  <SCRIPT language="javascript">orderTypeChanged();</SCRIPT>
  </FORM>
</BODY>
</HTML>