<HTML>
<HEAD><TITLE>Om !!! Brokerage Detail</TITLE>
{literal}
<SCRIPT language = "javascript">
function conf(expiryId)
{
	if(confirm("Are You Sure You want to Delete Record?"))
			location.href="expiryDelete.php?expiryId="+expiryId;
	else
  		location.href="expiryList.php";
}
function confEdit(expiryId,id)
{
	price_h = document.getElementById("price_high"+id).value;
	price_l = document.getElementById("price_low"+id).value;
	if(confirm("Are You Sure You want to Update Record?"))
			location.href="expiryEdit.php?expiryId="+expiryId+"&price_high="+price_h+"&price_low="+price_l;
	else
  		location.href="expiryList.php";
}
//////////////////////// CHECK BOX START //////////
function checkAll(checkid, exby) 
{
  for (i = 0; i < checkid.length; i++)
  checkid[i].checked = exby.checked? true:false
}
function pageSubmit() 
{
  document.mylist.submit();
}


//////////////////////// CHECK BOX STOP ///////////
</SCRIPT>
{/literal}
</HEAD>
<BODY bgColor="#FFCEE7">
<FORM name="mylist" method="post">  
<A href="./index.php">Home</A>&nbsp;&nbsp;&nbsp;&nbsp;
<A href="./expiryEntry.php">Add Expiry</A>
<TABLE BORDER=1 cellSpacing="0" cellPadding="3">
	<TR>
  <TD colspan="7" align="center">
  	<b>Exchange :</b>
    <SELECT name="exchange" onchange="pageSubmit();" >
    	<option name="all">All</option>
      {html_options values=$exchangeName output=$exchangeName selected=$postExchangeName}
    </SELECT>
  </TD>
</TR>
  <TR>
    <TH><input type="checkbox" name="all" id="checkGroup" onClick="checkAll(document.mylist.checkGroup,this)">Check/Uncheck All<br></TH>
    <TH>Item</TH>
    <TH>ExpiryDate</TH>
	<TH>Price High</TH>
	<TH>Price Low</TH>
	<TH>Exchange</TH>
    <TH>Delete</TH>
  </TR>
  {counter assign=t start=0 print=false}
  {section name=sec1 loop=$i}
    <TR>
      <TD><input type="checkbox" name="checkGroup[]" id="checkGroup" value="{$expiryId[sec1]}"></TD>
      <TD>{$itemName[sec1]}</TD>
      <TD>{$expiryDate[sec1]}</TD>
	  <TD><input type="text" id="price_high{$t}" name="price_high" value={$price_high[sec1]} size="10"></TD>
	  <TD><input type="text" id="price_low{$t}" name="price_low" value={$price_low[sec1]} size="10"></TD>
      <TD>{$exchange[sec1]}&nbsp;</TD>
      <TD><A href="javascript: confEdit({$expiryId[sec1]},{$t});">Update</A></TD>
	  <TD><A href="javascript: conf({$expiryId[sec1]});">Delete</A></TD>
    </TR>
	{counter}
  {/section}
  <TR>
    <TH><input type="checkbox" name="all" id="checkGroup" onClick="checkAll(document.mylist.checkGroup,this)">Check/Uncheck All<br></TH>
  </TR>
  <TR>
    <TD colspan="5" align="center"><input type="submit" name="submitBtn" value="Delete All" ></TD>
  </TR>
</TABLE>
</FONT>
</BODY>
</HTML>