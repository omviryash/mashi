<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
  <script type="text/javascript" src="./js/jquery.js"></script>
<STYLE>
{literal}
td{font-weight: BOLD}
{/literal}
</STYLE>
</HEAD>
<BODY>
  <A href="index.php">Home</A>&nbsp;&nbsp;<BR><BR>
<FORM name="form1" method="get" action="{$PHP_SELF}">
<INPUT type="hidden" name="display" value="{$display}">
<INPUT type="hidden" name="itemIdChanged" value="0">
<TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
<TR>
  <TD>Client :
    <SELECT name="clientId" onChange="document.form1.submit();">
    {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOptions"}
    </SELECT>
  </TD>
  <TD>Item :
    <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
    {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOptions"}
    </SELECT>
  </TD>
  <TD>Expiry :
    <SELECT name="expiryDate" onChange="document.form1.submit();">
    {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOptions"}
    </SELECT>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    <A href="selectDtSession.php?goTo=clientTrades">Date range</A> : {$fromDate} To : {$toDate}</CENTER>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    {$message}
  </TD>
</TR>
</TABLE>

</FORM>
<br/>
<FORM name="form2" method="get" action="{$PHP_SELF}">
<TABLE cellPadding="2" cellSpacing="0">
{section name="expSec" loop=$countExpiryArr}
  <TD>{$expiryForTxtBox[expSec].itemId}</TD>
  <TD>{$expiryForTxtBox[expSec].ExpDate}</TD>
  <TD><INPUT type="text" size="5" name="{$expiryForTxtBox[expSec].itemId}_{$expiryForTxtBox[expSec].ExpDate}" onblur="setToTextBox(this)" /></TD>
    {if $smarty.section.expSec.rownum%3 == 0}
      </TR>
    {/if}
{/section}
</TABLE><br/>
<TABLE border="1" cellPadding="2" cellSpacing="0">
<TR>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Buy</TD>
  <TD colspan="2" align="center">Sell</TD>
  <TD colspan="6">&nbsp;</TD>
</TR>
<TR>
  <TD>&nbsp;</TD>
  <TD align="center">Net</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD colspan="5" align="center">Item</TD>
  <TD>ProfitLoss</TD>
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].dispGross != 0}
  <TR>
    <TD>{$trades[sec1].clientId} : {$trades[sec1].clientName}</TD>
    <TD align="right" NOWRAP>
        {math equation="totBuyQty-totSellQty" totBuyQty=$trades[sec1].totBuyQty totSellQty=$trades[sec1].totSellQty}
    </TD>
    <TD align="right">{$trades[sec1].totBuyQty}</TD>
    <TD align="right">
      {if $trades[sec1].totBuyQty == $trades[sec1].totSellQty}
        &nbsp;
      {else}
        <INPUT type="text" size="5" name="net_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" value="{math equation="totBuyQty-totSellQty" totBuyQty=$trades[sec1].totBuyQty totSellQty=$trades[sec1].totSellQty}" />
        :
        <INPUT type="text" size="5" name="{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" />
        <INPUT type="text" size="5" name="buyAmount_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" value="{$trades[sec1].totBuyAmountNoFormat}" />
        <INPUT type="text" size="5" name="pendingBuyAmount_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" value="0" />
        <INPUT type="text" size="5" name="totBuyAmount_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" />
      {/if}
    </TD>
    <TD align="right">{$trades[sec1].totSellQty}</TD>
    <TD align="right">
      {if $trades[sec1].totBuyQty == $trades[sec1].totSellQty}
        &nbsp;
      {else}
        <INPUT type="text" size="5" name="{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" />
        <INPUT type="text" size="5" name="sellAmount_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" value="{$trades[sec1].totSellAmountNoFormat}" />
        <INPUT type="text" size="5" name="pendingSellAmount_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" value="0" />
        <INPUT type="text" size="5" name="totSellAmount_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" />
      {/if}
    </TD>
  {if $trades[sec1].totBuyQty == $trades[sec1].totSellQty}
    <TD colspan="2">{$trades[sec1].itemIdExpiry}</TD>
    <TD align="right" NOWRAP>{$trades[sec1].profitLoss}</TD>
    <TD colspan="2" align="right" NOWRAP>{$trades[sec1].oneSideBrok}</TD>
    <TD align="right" NOWRAP>
      <input type="text" name="profitLoss_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" id="profitLoss_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" size="10" style="text-align: right;" value="{$trades[sec1].netProfitLossNotFormatted}" />
    </TD>
  {else}
    <TD colspan="5" align="right">{$trades[sec1].itemIdExpiry} :
    </TD>
    <TD>
      <input type="text" name="profitLoss_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" id="profitLoss_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" size="10" style="text-align: right;" value="0" />
      <input type="hidden" name="temp_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" id="temp_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_{$trades[sec1].clientId}" size="10" style="text-align: right;" value="0" />
    </TD>
  {/if}
  </TR>
  {if $trades[sec1].dispClientWhole != 0}
  <TR>
    <TD colspan="6" align="right">
      <U>{$trades[sec1].clientId} : {$trades[sec1].clientName}</U>
    </TD>
    <TD colspan="6" align="right"><U> : Total : </U>
       <input type="text" name="total_{$trades[sec1].clientId}" value="0" size="10" style="text-align: right;" />
    </TD>
  </TR>
  {/if}
{/if}
{/section}
</TABLE>
</FORM>
{literal}
<script type="text/javascript">
	setToTextBox("dummyText");//just to display total when user just opens the window... then on change of textbox, it will be called properly
function setToTextBox(textBoxObject)
{
  $("input[name^='"+textBoxObject.name+"']").val(textBoxObject.value);
  var loopLength = $("input[name^='temp_"+textBoxObject.name+"']").length;
  for(i = 0; i < loopLength; i++)
  {
    if($("input[name^='net_"+textBoxObject.name+"']")[i].value < 0 )
      $("input[name^='pendingBuyAmount_"+textBoxObject.name+"']")[i].value = (-$("input[name^='net_"+textBoxObject.name+"']")[i].value) * textBoxObject.value;
    else
      $("input[name^='pendingSellAmount_"+textBoxObject.name+"']")[i].value = $("input[name^='net_"+textBoxObject.name+"']")[i].value * textBoxObject.value;
    $("input[name^='totBuyAmount_"+textBoxObject.name+"']")[i].value = parseFloat($("input[name^='buyAmount_"+textBoxObject.name+"']")[i].value)
      + parseFloat($("input[name^='pendingBuyAmount_"+textBoxObject.name+"']")[i].value) ;
    $("input[name^='totSellAmount_"+textBoxObject.name+"']")[i].value = parseFloat($("input[name^='sellAmount_"+textBoxObject.name+"']")[i].value)
      + parseFloat($("input[name^='pendingSellAmount_"+textBoxObject.name+"']")[i].value);

    $("input[name^='profitLoss_"+textBoxObject.name+"']")[i].value = parseFloat($("input[name^='totSellAmount_"+textBoxObject.name+"']")[i].value)
      - parseFloat($("input[name^='totBuyAmount_"+textBoxObject.name+"']")[i].value) ;
    if($("input[name^='profitLoss_"+textBoxObject.name+"']")[i].value > 0)
    {
      theObject = $("input[name^='profitLoss_"+textBoxObject.name+"']")[i].name;
      $("#"+theObject).css("color","blue");
    }
    else if($("input[name^='profitLoss_"+textBoxObject.name+"']")[i].value < 0)
    {
      theObject = $("input[name^='profitLoss_"+textBoxObject.name+"']")[i].name;
      $("#"+theObject).css("color","red");
    }
  }
  totalLength = $("input[name^='total_']").length;
  for(j = 0; j < totalLength; j++)
  {
    stringManipulate = ($("input[name^='total_']")[j].name).split("_");
    profitLossLoop = $("input[id$='"+stringManipulate[1]+"']").length;
    varPlusInto = 0;
    for(k = 0; k < profitLossLoop; k++)
    {
      profitLossName = parseFloat($("input[id$='"+stringManipulate[1]+"']")[k].value);
      varPlusInto   += profitLossName;
    }
    $("input[name^='total_']")[j].value = varPlusInto;
    if($("input[name^='total_']")[j].value > 0)
      $("input[name^='total_']").css("color","blue");
    else if($("input[name^='total_']")[j].value < 0)
      $("input[name^='total_']").css("color","red");
  }
}

{/literal}
</script>

</BODY>
</HTML>