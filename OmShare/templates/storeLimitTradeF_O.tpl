<HTML>
<HEAD><TITLE>Om !!! Add Trade</TITLE>
<SCRIPT language="javascript">
{literal}
window.name = 'displayAll';

function changeName()
{
  document.form1.changedField.value = "clientId";
  document.form1.submit();
}
function changeItem()
{
  document.form1.changedField.value = "itemId";
  document.form1.submit();
}

function changePrice(thePriceObject)
{
  var price;
  price = parseFloat(thePriceObject.value);
  if(parseFloat(price) || price==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      price=price+1;
    if(event.keyCode==33)
      price=price+10;
    if(event.keyCode==40)
      price=price-1;
    if(event.keyCode==34)
      price=price-10;
    if(thePriceObject.value != price)
      thePriceObject.value = price;
  }
}
function itemFromPrice()
{
  var price;
  price = parseFloat(document.form1.price.value);
{/literal}
{$itemFromPriceJS}
{literal}
}

function changeQty()
{
  {/literal}
  var minQty = {$minQty};
  {literal}
  var qty;
  qty = parseFloat(document.form1.qty.value);
  if(parseFloat(qty) || qty==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      qty=qty+minQty;
    if(event.keyCode==33)
      qty=qty+minQty*5;
    if(event.keyCode==40)
      qty=qty-minQty;
    if(event.keyCode==34)
      qty=qty-minQty*5;
    if(document.form1.qty.value != qty)
      document.form1.qty.value = qty;
  }
}
function changeBuySale()
{
  if(event.keyCode == 119 || event.keyCode==107)
  {
    document.form1.buySell.value = "Buy";
    return false;
  }
  if(event.keyCode == 120 || event.keyCode==109)
  {
    document.form1.buySell.value = "Sell";
    return false;
  }
  if(event.keyCode == 13)
  {
    if(!(event.shiftKey == true) && !(event.ctrlKey == true))
      document.form1.submitBtnRl.click();
    else if(event.shiftKey)
      document.form1.submitBtnSl.click();
    else if(event.ctrlKey)
      document.form1.submitBtn.click();
  }
  return true;
}
function checkBuySell()
{
  if(document.form1.buySell.value == "Buy" || document.form1.buySell.value == "Sell" )
  {}
  else
    document.form1.buySell.value = document.form1.beforeBuySell.value;
}
{/literal}
</SCRIPT>
</HEAD>
{literal}
<BODY bgColor="#FFFF80" onKeyDown="if(event.keyCode==10)
                                   { document.form1.makeTrade.value=1; document.form1.submit();  } 
                                   return changeBuySale();">
{/literal}
  <FORM name="form1" action="{$PHP_SELF}" METHOD="post">
  <INPUT type="hidden" name="changedField" value="">
  <INPUT type="hidden" name="makeTrade" value="0">
  <INPUT type="hidden" name="firstName" value="{$firstName}">
  <INPUT type="hidden" name="middleName" value="{$middleName}">
  <INPUT type="hidden" name="lastName" value="{$lastName}">
  <INPUT type="hidden" name="forStand" value="{$forStand}">
  <TABLE BORDER=1 width="100%" cellPadding="2" cellSpacing="0">
  <TR>
    <TD>
      <SELECT name="clientId" onChange="changeName();">
      {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOutput" }
      </SELECT>
    </TD>
    <TD>Date : 
      {html_select_date time="$tradeDateDisplay" prefix="trade" start_year="-1" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
    </TD>
    <TD colspan="2" NOWRAP>
      Price : <INPUT size="10" type="text" name="price" value="{$lastPrice}" onKeydown="changePrice(this);" onBlur="document.form1.price2.value=this.value;">&nbsp;&nbsp;&nbsp;
    </TD>
    <TD>
      Price2 : <INPUT size="10" type="text" name="price2" value="{$lastPrice2}" onKeydown="changePrice(this);" onBlur="itemFromPrice();">&nbsp;&nbsp;&nbsp;
    </TD>
  </TR>
  <TR>
    <TD>
      <SELECT name="itemId" onChange="changeItem();">
      {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOutput"}
      </SELECT>
      &nbsp;&nbsp;
      <SELECT name="expiryDate">
      {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOutput"}
      </SELECT>
      &nbsp;&nbsp;&nbsp;
      <INPUT type="hidden" name="beforeBuySell" value="Buy">
      <INPUT type="text" name="buySell" value="Buy" size="8" 
        onFocus="document.form1.beforeBuySell.value=document.form1.buySell.value;"
        onChange="checkBuySell();">
      </TD>
    <TD>
      Quantity : <INPUT type="text" name="qty" value="{$minQty}" onKeyDown="changeQty();">&nbsp;&nbsp;&nbsp;
    </TD>
    <TD colspan="2">
    &nbsp;
    </TD>
  </TR>
  <TR>
{if $forStand == 1}
    <TD>
      <INPUT type="radio" name="standing" value="-1"> Open Standing
      <INPUT type="radio" name="standing" value="1"> Close Standing
    </TD>
{/if}
    <TD colspan="3">
    	Client 2 : 
      <SELECT name="clientId2">
      {html_options selected="$clientId2Selected" values="$clientId2Values" output="$clientId2Output" }
      </SELECT>
      Vendor : 
      <INPUT type="hidden" name="vendor" value="_SELF">
    </TD>
  </TR>
  <TR>
    <TD colspan="4">
      <INPUT type="submit" name="submitBtnRl" value="RL" onClick="document.form1.makeTrade.value=2;">
      <INPUT type="submit" name="submitBtnSl" value="SL" onClick="document.form1.makeTrade.value=3;">
      <INPUT type="submit" name="submitBtn" value="Trade" onClick="document.form1.makeTrade.value=1;">
       &nbsp;&nbsp;&nbsp;&nbsp;<B>Enter => RL ## Shift + Enter => SL ## Ctrl + Enter => Trade
       <!--{$clientWholeName} : </B>
       Deposit : {$deposit}&nbsp;&nbsp;&nbsp;&nbsp;
       CurrentBal : {$currentBal}&nbsp;&nbsp;&nbsp;&nbsp;
       Total : {$total}&nbsp;&nbsp;&nbsp;&nbsp;
      ho Pne   : {$phone}&nbsp;&nbsp;&nbsp;&nbsp;
       Mobile  : {$mobile}&nbsp;-->
    </TD>
  </TR>
  <TR>
    <TD colspan="4"><B>Last trade : </B>{$lastTradeInfoVar}
    </TD>
  </TR>
  </TABLE>
  {$focusScript}
  </FORM>
</BODY>
</HTML>
