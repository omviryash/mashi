<HTML>
<HEAD><TITLE></TITLE>
<script type="text/javascript" src="./js/jquery.js"></script>
{literal}
<SCRIPT type="text/javascript">
  function changeExchange(exchange)
  {
    $.ajax(
    {
      type:"POST",
      url:"setOption.php",
      data:
      {
        message  : 'changeExchange',
        exchange : exchange
      },
      success:function(response)
      {
        $('#itemDiv').html(response);
        $('#expiryDiv').html('<SELECT name="expiry"></SELECT>');
      }
    });
  }
  
  function changeItem(itemVal,exchange)
  {
    $.ajax(
    {
      type:"POST",
      url:"setOption.php",
      data:
      {
        message  : 'changeItem',
        exchange : exchange,
        itemVal  : itemVal
      },
      success:function(response)
      {
        $('#expiryDiv').html(response);
      }
    });
  }
  $(document).ready(function()
  {
    $('#checkAll').click(function()
    {
      var checkid = document.forTradeId.tradeId;
      var exby = this;
      
      if(typeof(checkid.length) == 'undefined')
    	  checkid.checked = exby.checked? true:false
    	else
    	{
    	  for (i = 0; i < checkid.length; i++)
    	  {
    	    checkid[i].checked = exby.checked? true:false
    	  }
    	}
    });
  });
    
</SCRIPT>
{/literal}
</HEAD>
<BODY>
<a href="./index.php">Home</a>
<FORM name="formSort" action="" method="post">
<TABLE border="1" width="100%" cellPadding="2" cellSpacing="0">
<TR>
  <TD colspan="8" align="center">
    <B>Client :</B>
    <SELECT name="clientForSort" onChange="document.formSort.submit();">
      <OPTION value="0">All </OPTION>
      {html_options values=$clientName.id output=$clientName.name selected=$selectClient}
    </SELECT>&nbsp;&nbsp;
    <A href="selectDtSession.php?goTo={$goTo}">Date range</A> : {$smarty.session.fromDate|date_format:"%d-%m-%Y"} To : {$smarty.session.toDate|date_format:"%d-%m-%Y"}</CENTER>
  </TD>
</TR>
</TABLE>
</FORM>
<FORM name="forTradeId" action="" method="post">
<TABLE border="1" width="100%" cellPadding="2" cellSpacing="0">
  <TR>
    <TD colspan="8">
      <B>Client :</B>
      <SELECT name="clientIdForTradeId">
        <OPTION value="0">As It Is</OPTION>
        {html_options values=$clientName.id output=$clientName.name}
      </SELECT>&nbsp;&nbsp;
      <B>Date :</B>
      {html_select_date prefix='changeDate' start_year='+0' time='0000-00-00' end_year='+5' year_empty="Is" month_empty="As" day_empty="It"}
      &nbsp;&nbsp;
      <B>Buy/Sell :</B>
      <SELECT name="buySell">
        <OPTION value="0">As It Is</OPTION>
        <OPTION value="Buy">Buy</OPTION>
        <OPTION value="Sell">Sell</OPTION>
      </SELECT>
    </TD>
  </TR>
  <TR>
    <TD colspan="8">
      <B>Exchange :</B>
      <SELECT name="exchange" onchange="changeExchange(this.value)">
        {html_options values=$exchange.exchange output=$exchange.exchange}
      </SELECT>&nbsp;&nbsp;
      <B>Item :</B>
      <DIV id="itemDiv" style="display:inline;">
        <SELECT name="item" onchange="changeItem(this.value,document.forTradeId.exchange.value)">
          <OPTION value="0">As It Is</OPTION>
        </SELECT>
      </DIV>&nbsp;&nbsp;
      <B>Expiry :</B>
      <DIV id="expiryDiv" style="display:inline;">
        <SELECT name="expiry"></SELECT>
      </DIV>&nbsp;&nbsp;
      <B>Price :</B>
      <input type="text" name="price" size="9"/>
    </TD>
  </TR>
  <TR>
    <TD align="center" width="30%">Client</TD>
    <TD align="center" width="30%">Date</TD>
    <TD align="center">Buy/Sell</TD>
    <TD align="center">ItemId</TD>
    <TD align="center">Qty</TD>
    <TD align="center">Price</TD>
    <TD align="center">ExpiryDate</TD>
    <TD align="center">TradeRefNo</TD>
  </TR>
  <TR>
    <TD colspan="8"><INPUT type="checkbox" name="checkAll" id="checkAll" value="checkAll"/>Check/UnCheck All</TD>
  </TR>
  {section name="sec" loop=$client|@count}
    {if $client[sec].buySell == 'Buy'}
      <TR style="color:blue;">
    {else}
      <TR style="color:red;">
    {/if}
      <TD>
        <INPUT type="checkbox" id="tradeId" name="tradeId[]" value="{$client[sec].id}" />
        &nbsp;&nbsp;<INPUT type = "submit" name="submitBtn" value="Go!" />&nbsp;
        {$client[sec].name}
      </TD>
      <TD>{$client[sec].dateTime|date_format:"%d-%m-%Y %I:%M:%S %p"}</TD>
      <TD>{$client[sec].buySell}</TD>
      <TD>{$client[sec].itemId}</TD>
      <TD><A href="qtyDevide.php?tradeIdToDevide={$client[sec].id}">{$client[sec].qty}</A></TD>
      <TD>{$client[sec].price}</TD>
      <TD>{$client[sec].expiryDate}</TD>
      <TD>{$client[sec].tradeRefNo}</TD>
    </TR>
  {/section}
</TABLE>
</FORM>
<SCRIPT type="text/javascript">
changeExchange(document.forTradeId.exchange.value);
</SCRIPT>
</BODY>
</HTML>