<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
{literal}
td{font-weight: BOLD}
{/literal}
</STYLE>  
</HEAD>
<BODY>
  <A href="index.php">Home</A>&nbsp;&nbsp;<BR><BR>
<FORM name="form1" method="get" action="{$PHP_SELF}">
<INPUT type="hidden" name="display" value="{$display}">
<INPUT type="hidden" name="itemIdChanged" value="0">
<TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
<TR>
  <TD align="center">
    Client : 
    <SELECT name="clientId" onChange="document.form1.submit();">
    {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOptions"}
    </SELECT>
    &nbsp;&nbsp;&nbsp;
    Item : 
    <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
    {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOptions"}
    </SELECT>
    &nbsp;&nbsp;&nbsp;
    Expiry : 
    <SELECT name="expiryDate" onChange="document.form1.submit();">
    {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOptions"}
    </SELECT>  </TD>
</TR>
<TR><TD colspan="3"><BR></TD></TR>
<TR>
  <TD colspan="3" align="center">
    <A href="selectDtSession.php?goTo=clientTrades">Date range</A><BR>{$fromDate} To : {$toDate}</CENTER>
    <BR>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    {$message}
  </TD>
</TR>
</TABLE>
<TABLE border="1" cellPadding="3" cellSpacing="0" width="100%">
<TR>
  <TD>&nbsp;</TD>
  <TD colspan="3" align="center">Buy</TD>
  <TD colspan="3" align="center">Sell</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="center">Item</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Amount</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Amount</TD>
  <TD align="center">Net</TD>
  <TD align="center">AvgRate</TD>
  <TD align="center">NetAmount</TD>
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].clientId != $trades[sec1].prevClientId }
    <TR bgColor="#A6FFFF">
      <TD colspan="10" align="center">{$trades[sec1].clientId} : {$trades[sec1].clientName}</TD>
    </TR>
{/if}
{if $trades[sec1].dispGross != 0}
  {if $trades[sec1].totBuyQty != $trades[sec1].totSellQty}
    <TR>
      <TD align="center" NOWRAP>{$trades[sec1].itemIdExpiry}</TD>
      <TD align="right">{$trades[sec1].totBuyQty}</TD>
      <TD align="right">{$trades[sec1].buyRash}</TD>
      <TD align="right">{$trades[sec1].totBuyAmount}</TD>
      <TD align="right">{$trades[sec1].totSellQty}</TD>
      <TD align="right">{$trades[sec1].sellRash}</TD>
      <TD align="right">{$trades[sec1].totSellAmount}</TD>
      <TD align="right" NOWRAP>
        {math equation="totBuyQty-totSellQty" totBuyQty=$trades[sec1].totBuyQty totSellQty=$trades[sec1].totSellQty}
      </TD>
      <TD align="right">{$trades[sec1].totAvgRate}</TD>
      <TD align="right">{$trades[sec1].totNetAmount}</TD>
    </TR>
  {/if}
{/if}
  {if $trades[sec1].dispClientWhole != 0}
  <TR bgColor="#FFFF9F">
    <TD>&nbsp;</TD>
    <TD>&nbsp;</TD>
    <TD>&nbsp;</TD>
    <TD align="right">{$trades[sec1].clientTotBuyAmount}</TD>
    <TD>&nbsp;</TD>
    <TD>&nbsp;</TD>
    <TD align="right">{$trades[sec1].clientTotSellAmount}</TD>
    <TD>&nbsp;</TD>
    <TD>&nbsp;</TD>
    <TD align="right">{$trades[sec1].clientTotNetAmount}</TD>
  </TR>
  <TR><TD colspan="10">&nbsp;</TD>
  </TR>
  {/if}
{/section}
</TABLE>
</FORM>
</BODY>
</HTML>
