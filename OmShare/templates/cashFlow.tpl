<BODY>
<center><A href="./index.php">Home</A>&nbsp;&nbsp;
<A href="./accTransOtherAdd.php">Add</A>&nbsp;&nbsp;
<A href="./accTransList.php">List</A>&nbsp;&nbsp;
<A href="./accSummary.php">Summary</A>&nbsp;&nbsp;
<A href="./mnuAccount.php">Account Menu</A>
</center><br />
<FORM name=Form1 action="{$PHP_SELF}" method=get>
<TABLE border="0" cellPadding="2" cellSpacing="0">
<TR>
 	<TD>&nbsp;&nbsp;Transaction In&nbsp;</TD>
 	<TD>
 		<select name="transModeOpt" onChange="document.Form1.submit();">
	    <OPTION value="0">All </OPTION>
	    {html_options values=$bank.name output=$bank.name selected=$transModeOptSelected}
    </select>
  </Td>
  <td>
    {include file = "./dateRange.tpl"}
  </td>
</TR>
</TABLE>
</FORM>
  <TABLE BORDER=1 cellspacing="0" cellpadding="3">
  <TR>
    <TH colspan="8">&nbsp;</TH>
  </TR>
  <TR>
    <TH ALIGN="center">Delete</TH>
    <TH ALIGN="center">Date</TH>
    <TH ALIGN="center">Client</TH>
    <TH align="center">Transaction Type</TH>
    <TH ALIGN="center">Note</TH>
    <TH ALIGN="center"><font color="blue">
      ! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Credit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; !</font>
    </TH>
    <TH ALIGN="center" NOWRAP><font color="red">
    ! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Debit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; !</font>
    </TH>
    <TH>Total</TH>
  </TR>
    {section name=sec2 loop=$cashFlowCount}
      <TR>
        <TD>
          <A onclick="return confirm('Are you sure?');"
          href="accCashFlowDelete.php?cashFlowId={$cashFlow[sec2].id}&amp;page=cashFlow">Delete</A>
        </TD>
        <TD NOWRAP>{$cashFlow[sec2].date|date_format:"%d-%m-%Y"}</TD>
        <TD NOWRAP>
          <a href="./accTransList.php?clientId={$cashFlow[sec2].clientId}" >
          {$cashFlow[sec2].clientId} : {$cashFlow[sec2].firstName} {$cashFlow[sec2].middleName} {$cashFlow[sec2].lastName}
          </a>
        </TD>
        <TD NOWRAP>{$cashFlow[sec2].transtype}</TD>
        <TD>&nbsp; {$cashFlow[sec2].notes}</TD>
        {if $cashFlow[sec2].dwstatus == 'd'}  
          <TD NOWRAP align="right"><font color="blue">&nbsp;{$cashFlow[sec2].amount}</font></TD>
          <TD NOWRAP align="right">&nbsp;</TD>
        {else}
          <TD NOWRAP align="right">&nbsp;</TD>
          <TD NOWRAP align="right"><font color="red">&nbsp;{$cashFlow[sec2].amount}</font></TD>
        {/if}
        <TD align="right">
        	{if $cashFlow[sec2].totalAmount >= 0}<FONT color="blue">{else}<FONT color="red">{/if}
        	<B>&nbsp;{math equation="x" x=$cashFlow[sec2].totalAmount format="%.2f"}</B>
        	</FONT>
        </TD>
      </TR>
    {/section}
    {section name=sec3 loop=$otherExpCount}
      <TR>
        <TD>
            <A onclick="return confirm('Are you sure?');"
            href="accCashFlowDelete.php?expId={$otherExp[sec3].id}&amp;page=cashFlow">Delete</A>
        </TD>
        <TD NOWRAP>{$otherExp[sec3].date|date_format:"%d-%m-%Y"}</TD>
        <TD NOWRAP>Expences : {$otherExp[sec3].name}</TD>
        <TD>&nbsp;{$otherExp[sec3].note}</TD>
          <TD NOWRAP align="right">&nbsp;</TD>
          <TD NOWRAP align="right">&nbsp;</TD>
          <TD NOWRAP align="right"><FONT color="red">&nbsp;{$otherExp[sec3].amount}</FONT></TD>
        <TD align="right">
        	{if $otherExp[sec3].totalAmount >= 0}<FONT color="blue">{else}<FONT color="red">{/if}
        	<B>&nbsp;{math equation="x" x=$otherExp[sec3].totalAmount format="%.2f"}</B></TD>
        	</FONT>
      </TR>
    {/section}

  </TABLE>
</BODY>