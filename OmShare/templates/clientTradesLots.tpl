<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
  "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE type="text/css">
{literal}
td{font-weight: BOLD}
.lossStyle   {color: red}
.profitStyle {color: blue}
{/literal}
</STYLE>
<script type="text/javascript" src="./js/jquery.js"></script>
<script type="text/javascript">
{literal}
  function changeCheckBox()
  {
    $(".subCheckBox").each(function(e)
    {
      trades = this.name;
      if(this.checked == true)
        thisIsChecked = 1;
      else
        thisIsChecked = 0;
      $.ajax(
      {
        type:"POST",
        url:'updateStatus.php',
        data:
        {
          trades:trades,
          confirmed:thisIsChecked
        },
        success: function(response)
        {
        }
      });
    });
  }
  $(document).ready(function()
  {
    $(".mainCheckBox").change(function()
    {
      if($('.mainCheckBox').is(':checked'))
        $('input:checkbox').attr('checked', true);
      else
        $('input:checkbox').attr('checked', false);
      changeCheckBox();
    });
  });
{/literal}
</script>
</HEAD>
<BODY onKeyPress="if(event.keyCode==27)  window.close();">
  <A href="index.php">Home</A>&nbsp;&nbsp;
<FORM name="form1" method="get" action="{$PHP_SELF}">
<INPUT type="hidden" name="display" value="{$display}">
<INPUT type="hidden" name="itemIdChanged" value="0">
<TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
<TR>
{if $userType != 'client'}
  <TD>Client : 
    <SELECT name="clientId" onChange="document.form1.submit();">
    {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOptions"}
    </SELECT>
  </TD>
{/if}
  <TD>Item : 
    <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
    {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOptions"}
    </SELECT>
  </TD>
  <TD>Expiry : 
    <SELECT name="expiryDate" onChange="document.form1.submit();">
    {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOptions"}
    </SELECT>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    <A href="selectDtSession.php?goTo={$goTo}">Date range</A> : {$fromDate} To : {$toDate}</CENTER>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    {$message}
  </TD>
</TR>
</FORM>
</TABLE>
<FORM name="form2" method="post" action="./acStorePl.php">
<TABLE border="1" cellPadding="2" cellSpacing="0">
<TR>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Buy</TD>
  <TD colspan="2" align="center">Sell</TD>
  <TD colspan="6">&nbsp;</TD>
</TR>
<TR>
  <TD align="center"><input type="checkbox" class="mainCheckBox" /> BuySell</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Date</TD>
  <TD align="center">Stand</TD>
  <TD align="center">Item</TD>
  <TD align="center">NetProfitLoss</TD>
{if $userType != 'client'}
  <TD align="center">Client2</TD>
  <TD align="center">Delete</TD>
{/if}
  {if $display == "detailed"} 
    <TD align="center">UserRemarks</TD>
    <TD align="center">OwnClient</TD>
    <TD align="center" NOWRAP>TradeRefNo</TD>
  {/if}
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].clientId != $trades[sec1].prevClientId or $trades[sec1].itemId != $trades[sec1].prevItemId or $trades[sec1].expiryDate != $trades[sec1].prevExpiryDate}
  <TR>
    <TD colspan="5"><U>{$trades[sec1].clientId} : {$trades[sec1].clientName}</U>&nbsp;:&nbsp;({$trades[sec1].clientDeposit})</TD>
    <TD colspan="6" align="center">{$trades[sec1].itemIdExpiry}</TD>
  </TR>
{/if}
<TR style="color:{$trades[sec1].fontColor}">
  <TD>
    <input type="checkbox" {if $trades[sec1].confirmed == 1} checked {/if}name="trade_{$trades[sec1].tradeId}" class="subCheckBox" onchange="changeCheckBox();" />
    {$trades[sec1].buySell}</TD>
  <TD align="right">
    {if $trades[sec1].buyQty != "&nbsp;"}
    {math equation="qty/min" qty=$trades[sec1].buyQty min=$trades[sec1].min}
    {else}
    {$trades[sec1].buyQty}
    {/if}
  </TD>
  <TD align="right">{$trades[sec1].price}</TD>
  <TD align="right">
    {if $trades[sec1].sellQty != "&nbsp;"}
    {math equation="qty/min" qty=$trades[sec1].sellQty min=$trades[sec1].min}
    {else}
    {$trades[sec1].sellQty}
    {/if}
  </TD>
  <TD align="right">{$trades[sec1].sellPrice}</TD>
  <TD align="center" NOWRAP>{$trades[sec1].tradeDate}
    {if $display == "detailed"} {$trades[sec1].tradeTime} {/if}
  </TD>
  <TD align="center">{$trades[sec1].standing}</TD>
  <TD align="center" NOWRAP>{$trades[sec1].itemIdExpiry}</TD>
  <TD>&nbsp;</TD>
{if $userType != 'client'}
  <TD align="center">{$trades[sec1].clientId2} : {$trades[sec1].client2Name}</TD>
  <TD>
    <A onclick="return confirm('Are you sure?');" href="deleteTxt.php?goTo={$goTo}&tradeId={$trades[sec1].tradeId}">
    {if $trades[sec1].standing != "Close"}
      Delete
    {else}
      Delete Stand
    {/if}
    </A>
    &nbsp;
    <A href="{$edit2File}?tradeId={$trades[sec1].tradeId}&exchange={$exchange}&goTo={$goTo}">Edit2</A>
  </TD>
{/if}
  {if $display == "detailed"} 
    <TD align="center">{$trades[sec1].userRemarks}</TD>
    <TD align="center">{$trades[sec1].ownClient}</TD>
    <TD align="center" NOWRAP>{$trades[sec1].tradeRefNo}</TD>
  {/if}
  {if $displayProfitLossUpToThis == 1}
    <td>{$trades[sec1].profitLossUpToThis}</td>
  {/if}
</TR>
{if $trades[sec1].dispGross != 0}
  <TR>
    <TD align="right" NOWRAP>
    	{if ($trades[sec1].totBuyQty - $trades[sec1].totSellQty) >= 0}
    	  <FONT color="blue">
    	{else}
    	  <FONT color="red">
    	{/if}
      Net: {math equation="(totBuyQty-totSellQty)/min" totBuyQty=$trades[sec1].totBuyQty totSellQty=$trades[sec1].totSellQty min=$trades[sec1].min}
        </FONT>
    </TD>
    <TD align="right"><FONT color="blue">{math equation="qty/min" qty=$trades[sec1].totBuyQty min=$trades[sec1].min}</FONT></TD>
    <TD align="right">{$trades[sec1].buyRash}</TD>
    <TD align="right"><FONT color="red">{math equation="qty/min" qty=$trades[sec1].totSellQty min=$trades[sec1].min}</FONT></TD>
    <TD align="right">{$trades[sec1].sellRash}</TD>
  {if $trades[sec1].totBuyQty == $trades[sec1].totSellQty} 
    <TD colspan="3" align="right" NOWRAP>
      {if $trades[sec1].profitLoss < 0}
        <FONT class="lossStyle">Loss : 
      {else}
        <FONT class="profitStyle">Profit : 
      {/if}
      {$trades[sec1].profitLoss}</FONT>
      Brok       : {$trades[sec1].oneSideBrok}</TD>
    <TD align="right" NOWRAP>
      {if $trades[sec1].netProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
      {$trades[sec1].netProfitLoss}</FONT>
    </TD>
    <TD colspan="2">&nbsp;</TD>
  {else}
    <TD colspan="6">{$trades[sec1].itemIdExpiry} : Buy Sell Qty Not Same</TD>
  {/if}
  </TR>
  {if $trades[sec1].dispClientWhole != 0}
  <TR>
    <TD colspan="5" align="right">
      : Total : {$trades[sec1].clientId} : {$trades[sec1].clientName}
    </TD>
    <TD colspan="3" align="right"><U>
      {if $trades[sec1].clientTotProfitLoss < 0}
        <FONT class="lossStyle">Loss : 
      {else}
        <FONT class="profitStyle">Profit : 
      {/if}
      {$trades[sec1].clientTotProfitLoss}</FONT></U>
      Brok       : {$trades[sec1].clientTotBrok}</U>
    </TD>
    <TD align="right"><U>
      {if $trades[sec1].clientTotNetProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
      {$trades[sec1].clientTotNetProfitLoss}</FONT></U></TD>
    <TD align="center" colspan="2">
      &nbsp;</TD>
  </TR>
  {/if}
{/if}
{/section}
<TR><TD colspan="11">&nbsp;</TD></TR>
<TR>
  <TD align="center">Net</TD>
  <TD>Buy</TD>
  <TD>Average</TD>
  <TD>Sell</TD>
  <TD>Average</TD>
  <TD colspan="2" align="center">Item</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD align="center">NetProfitLoss</TD>
</TR>
{section name="sec2" loop="$wholeItemArr"}
<TR>
    <TD align="right" NOWRAP>
    	{if ($wholeItemArr[sec2].buyQty - $wholeItemArr[sec2].sellQty) >= 0}
    	  <FONT color="blue">
    	{else}
    	  <FONT color="red">
    	{/if}
      {math equation="(buyQty-sellQty)/min" buyQty=$wholeItemArr[sec2].buyQty sellQty=$wholeItemArr[sec2].sellQty min=$wholeItemArr[sec2].min}
        </FONT>
    </TD>
  <TD align="right"><FONT color="blue">{math equation="qty/min" qty=$wholeItemArr[sec2].buyQty min=$wholeItemArr[sec2].min}</FONT></TD>
  <TD align="right">{$wholeItemArr[sec2].buyRash}</TD>
  <TD align="right"><FONT color="red">{math equation="qty/min" qty=$wholeItemArr[sec2].sellQty min=$wholeItemArr[sec2].min}</FONT></TD>
  <TD align="right">{$wholeItemArr[sec2].sellRash}</TD>
  <TD align="right" colspan="2" NOWRAP>{$wholeItemArr[sec2].itemIdExpiry}</TD>
  <TD align="right" NOWRAP>
    {if $wholeItemArr[sec2].profitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeItemArr[sec2].profitLoss}</FONT></TD>
  <TD align="right" colspan="2">{$wholeItemArr[sec2].oneSideBrok}</TD>
  <TD align="right" NOWRAP>
    {if $wholeItemArr[sec2].netProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeItemArr[sec2].netProfitLoss}</FONT></TD>
</TR>
{/section}
<TR>
    <TD align="right" NOWRAP>
      &nbsp;
    </TD>
  <TD align="right">&nbsp;</TD>
  <TD align="right">{$wholeBuyRash}</TD>
  <TD align="right">&nbsp;</TD>
  <TD align="right">{$wholeSellRash}</TD>
  <TD align="right" colspan="2" NOWRAP> : Total : </TD>
  <TD align="right" NOWRAP>
    {if $wholeProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeProfitLoss}</FONT></TD>
  <TD align="right" colspan="2">{$wholeOneSideBrok}</TD>
  <TD align="right" NOWRAP>
    {if $wholeNetProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeNetProfitLoss}</FONT></TD>
</TR>
</TABLE>
</BODY>
</HTML>
