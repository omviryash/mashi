<HTML>
<HEAD>
  {literal}
  <script type="text/javascript">
    function conf(otherIncomId)
    {
      if(confirm("Are you sure to delete this Expense ?"))
        location.href = "./otherIncomDelete.php?otherIncomId="+otherIncomId;
    }
  </script>
  {/literal}
  <TITLE>Om !!!</TITLE>
  <STYLE src="./templates/styles.css"></STYLE>
</HEAD>
<A href="index.php">Home</A>&nbsp;&nbsp;
<A href="otherIncomAdd.php">Add</A>&nbsp;&nbsp;
<A href="mnuAccount.php">Menu</A>
<BODY bgColor="TAN" >
	<FORM name="form1" action="otherIncomList.php" method="POST">
	<TABLE border="1" cellpadding="2" cellspacing="0">
    <TR>
      <TH>Expense Name</TH>
      <TH>Date</TH>
      <TH>Amount</TH>
      <TH>Mode</TH>
      <TH>Note</TH>
      <TH>&nbsp;</TH>
    </TR>
    <TR>
      {section name="sec1" loop=$i}
		    <TD>{$otherIncomName[sec1]}</TD>
		    <TD>{$otherIncomDate[sec1]}</TD>
		    <TD align="right">{$otherIncomAmount[sec1]}</TD>
		    <TD>{$otherIncomMode[sec1]}</TD>
		    {if $otherIncomNote[sec1] eq ""}
		    <TD>-</TD>
		    {else}
		    <TD>{$otherIncomNote[sec1]}</TD>
		    {/if}
		    <TD><a href="javascript:conf({$otherIncomId[sec1]});" >Delete</a></TD>
		    </TR>
      {/section}
    <TR>
    	<TH colspan="2">Total --></TH>
    	<TH align="right">{$totalExp}</TH>
    	<TH>&nbsp;</TH>
    	<TH>&nbsp;</TH>
    	<TH>&nbsp;</TH>
    	
    </TR>
    	
  </TABLE>
</FORM>
</BODY>
</HTML>