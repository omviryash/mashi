<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
{literal}
td{font-weight: BOLD}
{/literal}
</STYLE>  
</HEAD>
<BODY>
<TABLE border="1" cellPadding="2" cellSpacing="0">
<TR>
  <TD colspan="5" align="center">Name</TD>
  <TD colspan="2" align="center">&nbsp;</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD colspan="2" align="center">NetProfitLoss</TD>
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].dispGross != 0}
  {if $trades[sec1].dispClientWhole != 0}
  <TR>
    <TD colspan="5">
      {$trades[sec1].clientId} : {$trades[sec1].clientName}
    </TD>
    <TD colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
    <TD align="right">
      {$trades[sec1].clientTotProfitLoss}
    </TD>
    <TD colspan="2" align="right">
      {$trades[sec1].clientTotBrok}
    </TD>
    <TD align="right">
      {if $trades[sec1].clientTotNetProfitLoss < 0}
        {$trades[sec1].clientTotNetProfitLoss}
      {else}
        &nbsp;
      {/if}
    </TD>
    <TD align="right">
      {if $trades[sec1].clientTotNetProfitLoss >= 0}
        {$trades[sec1].clientTotNetProfitLoss}
      {else}
        &nbsp;
      {/if}
    </TD>
  </TR>
  {/if}
{/if}
{/section}
</TABLE>
</BODY>
</HTML>
