<LINK rel="stylesheet" type="text/css" href="./menuFiles/ddsmoothmenu.css" />
<SCRIPT type="text/javascript" src="./menuFiles/jquery.min.js"></SCRIPT>
<SCRIPT type="text/javascript" src="./menuFiles/ddsmoothmenu.js"></SCRIPT>
<form name="decideForm" action="./index.php" method="POST">
<DIV id="smoothmenu1" class="ddsmoothmenu">
<UL>

<LI><A href="#">Masters</A>
  <UL>
    <LI><A href="brokerage.php">Items</A></LI>
    <LI><A href="expiryList.php">Expiry Dates</A></LI>
    <LI><A href="clientList.php">Clients and Brokers</A></LI>
    <LI><A href="bankMasterList.php">Bank Masters</A></LI>
    <LI><A href="otherExpAdd.php">Other Expense</A></LI>
    <LI><A href="clientCommision.php">Client Commision Setting</A></LI>
  </UL>
</LI>

<LI><A href="#">Trades</A>
  <UL>
    <?php  if($packfor == 5 || $packfor == 6)  {  ?>
    <LI><A href="#">Mcx Trades</A>
    	<UL>
    	  <?php if($mcxFileDisplay[0] == 1) { ?><LI><A href="clientTradesMcx.php?display=tradesLots"                 >Client Trades</A></LI><?php } ?>
		    <?php if($mcxFileDisplay[1] == 1) { ?><LI><A href="brokerTradesMcx.php?display=tradesLots"                 >Broker Trades</A></LI><?php } ?>
    	  <?php if($mcxFileDisplay[2] == 1) { ?><LI><A href="clientTradesPer1sideMcx.php?display=tradesLots"         >Client Trades in % 1 Side</A></LI><?php } ?>
    	  <?php if($mcxFileDisplay[3] == 1) { ?><LI><A href="brokerTradesPer1sideMcx.php?display=tradesLots"         >Broker Trades in % 1 Side</A></LI><?php } ?>
    	  <?php if($mcxFileDisplay[4] == 1) { ?><LI><A href="clientTradesPer2side2mcx.php?display=tradesLots"        >Client Trades in % 2 Side</A></LI><?php } ?>
    	  <?php if($mcxFileDisplay[5] == 1) { ?><LI><A href="clientTradesMcx2side.php?display=tradesLots"            >Client Trades in Rs 2 Side</A></LI><?php } ?>
    	  <?php if($mcxFileDisplay[6] == 1) { ?><LI><A href="clientTradesMcx.php?display=detailed">Client Detailed Trades</A></LI><?php } ?>
		    <?php if($mcxFileDisplay[7] == 1) { ?><LI><A href="clientTradesMcx.php?display=detailed">Broker Detailed Trades</A></LI><?php } ?>
    	</UL>
    </LI>
    <?php  }  ?>
    <?php  if($packfor == 5 || $packfor == 7)  {  ?>
	    <LI><A href="#">F_O Trades</A>
	    	<UL>
	        <LI><A href="clientTradesPer2side2fo.php?display=tradesLots">Client F_O Trades</A></LI>
			    <LI><A href="brokerTradesPer2side2fo.php?display=tradesLots">Broker F_O Trades</A></LI>
	        <LI><A href="clientTradesPer2sideDifferent.php?exchange=F_O&display=tradesLots">Client F_O Trades Diff Brok</A></LI>
	        <LI><A href="brokerTradesPer2sideDifferent.php?exchange=F_O&display=tradesLots">Broker F_O Trades Diff Brok</A></LI>
	<!--        <LI><A href="clientTradesPer2side2BothF_O.php">Client F_O Trades 2side2Both</A></LI>
			    <LI><A href="brokerTradesPer2side2BothF_O.php">Broker F_O Trades 2side2Both</A></LI>
	        <LI><A href="clientTradesPer2side2foHigh.php">Client F_O Trades High</A></LI>
	        <LI><A href="clientTradesPer1sideF_O.php">Client F_O Trades 1 Side</A></LI>
			    <LI><A href="brokerTradesPer1sideF_O.php">Broker F_O Trades 1 Side</A></LI>
	    	  <LI><A href="clientTradesF_O.php"        >Client F_O Trades In Rs</A></LI>-->
	    	</UL>
	    </LI>
    <?php  }  ?>
    <LI><A href="txtFile.php">Store TWS Trades</A></LI>
    <LI><A href="txtFileOdin.php">Store ODIN Trades</A></LI>
    <LI><A href="#">&nbsp;</A></LI>
    <?php  if($packfor == 5 || $packfor == 6)  {  ?>
	    <?php
	      if($tradeInNewWindow == 1)
	      {
	    ?>
	        <LI><A href="" onClick="tradeWindow=window.open('ajxStoreLimitTradeMcx1.php?useTpl=txtbx&exchange=MCX', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=950, height=165, left=1, top=300'); return false;">Data Entry Mcx ClientId</A></LI>
	        <LI><A href="" onClick="tradeWindow=window.open('addTrade.php?exchange=MCX', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=120, left=1, top=300'); return false;">Data Entry MCX New</A></LI>
	    <?php
	      }
	      else
	      {
	    ?>
	        <LI><A href="ajxStoreLimitTradeMcx1.php?useTpl=txtbx&exchange=MCX">Data Entry Mcx ClientId</A></LI>
	        <LI><A href="addTrade.php?exchange=MCX">Data Entry MCX New</A></LI>
	    <?php
	      }
	    ?>
    <?php  }  ?>
    <?php  if($packfor == 5 || $packfor == 7)  {  ?>
	    <?php
	      if($tradeInNewWindow == 1)
	      {
	    ?>
	        <LI><A href="" onClick="tradeWindow=window.open('ajxStoreLimitTradeMcx1.php?useTpl=txtbx&exchange=F_O', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=120, left=1, top=300'); return false;">Data Entry F_O ClientId</A></LI>
	        <LI><A href="" onClick="tradeWindow=window.open('addTrade.php?exchange=F_O', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=120, left=1, top=300'); return false;">Data Entry F_O New</A></LI>
	    <?php
	      }
	      else
	      {
	    ?>
	        <LI><A href="ajxStoreLimitTradeMcx1.php?useTpl=txtbx&exchange=F_O">Data Entry F_O ClientId</A></LI>
	        <LI><A href="addTrade.php?exchange=F_O">Data Entry F_O New</A></LI>
	    <?php
	      }
	    ?>
    <?php  }  ?>
  </UL>
</LI>

<LI><A href="#">Other Options</A>
  <UL>
    <LI><A href="tradeListForCrossChecking.php">Confirm</A></LI>
    <LI><A href="orderList.php">Order List</A></LI>
    <LI><A href="attachNameToTxtData.php">Pending Trades</A></LI>
    <LI><A href="attachNameToTxtDataAll.php">Change Name</A></LI>
    <LI><A href="bulkChange.php">Bulk Change</A></LI>
    <LI><A href="deleteAll.php">Delete Trades</A></LI>
  </UL>
</LI>

<LI><A href="#">Settlement</A>
  <UL>
    <LI><A href="mnuStand.php"                                   >Standing</A></LI>
    <?php  if($packfor == 5 || $packfor == 6)  {  ?>
	    <?php if($mcxFileDisplay[0] == 1) { ?><LI><A href="clientTradesMcx.php?display=tradesPrint"        >Bill Print MCX In Rs</A></LI><?php } ?>
	    <?php if($mcxFileDisplay[2] == 1) { ?><LI><A href="clientTradesPer1sideMcx.php?display=tradesPrint">Bill Print MCX In % 1 Side</A></LI><?php } ?>
	    <?php if($mcxFileDisplay[2] == 1) { ?><LI><A href="clientTradesPer1sideMcx.php?display=tradesPrintOnlyNet">Bill Print MCX In % 1 Side Only Net</A></LI><?php } ?>
	    <?php if($mcxFileDisplay[4] == 1) { ?><LI><A href="clientTradesPer2side2mcx.php?display=tradesPrint">Bill Print MCX In % 2 Side</A></LI><?php } ?>
	    <?php if($mcxFileDisplay[4] == 1) { ?><LI><A href="clientTradesPer2side2mcx.php?display=tradesPrintOnlyNet">Bill Print MCX In % 2 Side Only Net</A></LI><?php } ?>
	    <?php if($mcxFileDisplay[0] == 1) { ?><LI><A href="clientTradesMcx.php?display=gross"              >Bill Gross MCX In Rs Client</A></LI><?php } ?>
	    <?php if($mcxFileDisplay[1] == 1) { ?><LI><A href="brokerTradesMcx.php?display=gross"              >Bill Gross MCX In Rs Broker</A></LI><?php } ?>
	    <?php if($mcxFileDisplay[2] == 1) { ?><LI><A href="clientTradesPer1sideMcx.php?display=gross"      >Bill Gross MCX In % 1 Side</A></LI><?php } ?>
	    <?php if($mcxFileDisplay[4] == 1) { ?><LI><A href="clientTradesPer2side2mcx.php?display=gross"     >Bill Gross MCX In % 2 Side</A></LI><?php } ?>
    <?php  }  ?>
    <?php  if($packfor == 5 || $packfor == 7)  {  ?>
	    <LI><A href="clientTradesPer2side2fo.php?display=gross"      >Bill Gross F_O Client</A></LI>
	    <LI><A href="brokerTradesPer2side2fo.php?display=gross"      >Bill Gross F_O Broker</A></LI>
	    <LI><A href="clientTradesPer2side2fo.php?display=tradesPrint">Bill Print F_O</A></LI>
	    <LI><A href="clientTradesPer2side2fo.php?display=tradesPrintOnlyNet">Bill Print F_O Only Net</A></LI>
    <?php  }  ?>
<!--    <LI><A href="clientTradesF_O.php?display=tradesPrint"        >Bill Print F_O In Rs</A></LI>
    <LI><A href="clientTradesPer1sideF_O.php?display=tradesPrint">Bill Print F_O 1 side In %</A></LI>
    <LI><A href="clientTradesPer1sideF_O.php?display=tradesPrintOnlyNet">Bill Print F_O 1 side In % Only Net</A></LI>-->
  </UL>
</LI>

<LI><A href="#">Reports</A>
  <UL>
    <?php  if($packfor == 5 || $packfor == 6)  {  ?>
	    <LI><A href="clientTradesMcx.php?display=itemPending">Script Outstanding MCX</A></LI>
	    <LI><A href="iframe.php">Script Outstanding MCX Both</A></LI>
	    <LI><A href="clientTradesMcx.php?display=itemPending2">Average Outstanding MCX</A></LI>
	    <?php if($mcxFileDisplay[0] == 1) { ?><LI><A href="clientTradesMcx.php?display=itemWiseGross"        >Script Gross MCX In Rs</A></LI><?php } ?>
	    <?php if($mcxFileDisplay[2] == 1) { ?><LI><A href="clientTradesPer1sideMcx.php?display=itemWiseGross">Script Gross MCX In % 1 Side</A></LI><?php } ?>
	    <?php if($mcxFileDisplay[4] == 1) { ?><LI><A href="clientTradesPer2side2mcx.php?display=itemWiseGross">Script Gross MCX In % 2 Side</A></LI><?php } ?>
	    <LI><A href="clientTradesMcx.php?display=itemWisePL">Script wise PL MCX</A></LI>
    <?php  }  ?>
    <?php  if($packfor == 5 || $packfor == 7)  {  ?>
	    <LI><A href="clientTradesPer2side2fo.php?display=itemPending">Script Outstanding F_O</A></LI>
	    <LI><A href="clientTradesPer2side2fo.php?display=itemPending2">Average Outstanding F_O</A></LI>
	    <LI><A href="clientTradesPer2side2fo.php?display=itemWiseGross">Script Gross F_O</A></LI>
	    <LI><A href="clientTradesPer2side2fo.php?display=itemWisePL">Script wise PL F_O</A></LI>
    <?php  }  ?>
<!--    <LI><A href="clientTradesF_O.php?display=gross"              >Bill Gross F_O In Rs</A></LI>
    <LI><A href="clientTradesPer1sideF_O.php?display=gross"      >Bill Gross F_O 1 side In %</A></LI>-->
      <LI><A href="commisionInCalc.php">Display Client's Commission</A></LI>
  </UL>
</LI>

<LI><A href="#">Accounts</A>
  <UL>
    <LI><A href="accSummary.php">Account Last Balance</A></LI>
    <LI><A href="mnuAccount.php">Money Transaction</A></LI>
  </UL>
</LI>

<LI><A href="#">Utilities</A>
  <UL>
    <LI><A href="writeTradesFile.php">Online Backup</A></LI>
    <LI><A href="backup.php">Backup</A></LI>
    <LI><A href="links.php">Useful Links</A></LI>
    <LI><A href="calc.php">Calculator</A></LI>
    <LI><A href="changePassword.php">Change Password</A></LI>
    <LI><A href="#">ExtraMenu</A>
      <UL>
		    <LI><A href="ajxStoreLimitTradeMcx1.php">Data Entry Mcx Fast</A></LI>
		    <LI><A href="ajxStoreLimitTradeFO.php">Data Entry FO Fast</A></LI>
        <LI><A href="storeLimitTradeMcx.php">Data Entry Mcx Slow</A></LI>
        <LI><A href="storeLimitTradeF_O.php">Data Entry F_O Slow</A></LI>
        <LI><A href="exchangeAdd.php">Exchange</A></LI>
        <LI><A href="#">Rollback</A></LI>
      </UL>
    </LI>
    <LI><A href="addTradeData.php">Trade File</A></LI>
  </UL>
</LI>

<LI><A href="selectDtSession.php">Date Range</A>
</LI>
<LI><A href="logout.php">Logout</A>
</LI>

</UL>
&nbsp;&nbsp;
<select name="decidePackFor" onChange="document.decideForm.submit();">
<?php  if($origPackFor == 5) { ?>
<option value="5" <?php echo $packForAllSelected; ?>>All</option>
<?php  } ?>
<?php  if($origPackFor == 5 || $origPackFor == 7) { ?>
<option value="7" <?php echo $packForF_OSelected; ?>>F_O</option>
<?php  } ?>
<?php  if($origPackFor == 5 || $origPackFor == 6) { ?>
<option value="6" <?php echo $packForMCXSelected; ?>>MCX</option>
<?php  } ?>
</select>
</form>
	
<BR style="clear: left" />
</DIV>
<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>