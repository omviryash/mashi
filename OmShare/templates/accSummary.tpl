<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title>Account Summary</title>
</head>
<BODY >
<center><A href="./index.php">Home</A>&nbsp;&nbsp;
<A href="./accTransOtherAdd.php">Add</A>&nbsp;&nbsp;
<A href="./accTransList.php">List</A>&nbsp;&nbsp;
<A href="./mnuAccount.php">Account Menu</A>
</center><br /><br />
<FORM name="Form1" action="{$PHP_SELF}" method="post">
  <TABLE BORDER=1 cellspacing="0" cellpadding="3" align="center">
  <TR>
    <TD colspan="2">
      Exchange :
      <SELECT name=exchange onChange="document.Form1.submit();">
        {html_options values=$exchange.name output=$exchange.name selected=$selectedExchange}
      </SELECT>
      <select name="crDr" onChange="document.Form1.submit();">
        {html_options values=$crDrValues output=$crDrOptions selected=$crDrSelected}
      </select>
    </TD>
    <TD vAlign="top" colspan="4">
      <A href="selectDtSession.php?goTo=accSummary">Date range</A> : From {$fromDate|date_format:"%d-%m-%Y"} To {$toDate|date_format:"%d-%m-%Y"}
    </TD>
    <TD>&nbsp;</TD>
    <TD>&nbsp;</TD>
  </TR>
  <TR>
    <TH align="center">Name</TH>
    <TH align="center">{if $cfgDispOpeningAccSmry == 1}Opening{/if}</TH>
    <TH align="center">{if $cfgDispCurBillAccSmry == 1}Last Bill{/if}</TH>
    {if $crDrSelected == "Cr"}
    <TH align="center">Credit</TH>
    <TH align="center">&nbsp;</TH>
    {elseif $crDrSelected == "Dr"}
    <TH align="center">&nbsp;</TH>
    <TH align="center">Debit</TH>
    {else}
    <TH align="center">Credit</TH>
    <TH align="center">Debit</TH>
    {/if}
    <TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
    <TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
    <TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
  </TR>
   {section name=sec1 loop=$clientArray}
   {if ($crDrSelected == "All" || ($crDrSelected == "Cr" && $clientArray[sec1].balance >= 0)
                              || ($crDrSelected == "Dr" && $clientArray[sec1].balance < 0 ))
                              && $clientArray[sec1].dispThisClient == 1 }
  <TR>
    <TD align="right" NOWRAP>
      <a href="./accTransList.php?clientId={$clientArray[sec1].clientId}" >
      {$clientArray[sec1].firstName} {$clientArray[sec1].middleName} {$clientArray[sec1].lastName}
      </a>
    </TD>
    <TD align="right" NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      {if $cfgDispOpeningAccSmry == 1}
      {if $clientArray[sec1].opening >= 0}<font color="blue">+{else}<font color="red">{/if}{$clientArray[sec1].opening}</font>
      {/if}
      &nbsp;
    </TD>
      <TD align="right" NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      {if $cfgDispCurBillAccSmry == 1}
      {if $clientArray[sec1].billAmount >= 0}<font color="blue">+{else}<font color="red">{/if} {$clientArray[sec1].billAmountToDisp}</font>
      {/if}
      &nbsp;
    </TD>
   <TD align="right" NOWRAP><FONT color="blue">
      {if $clientArray[sec1].balance >= 0}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+{$clientArray[sec1].balance}
      {else}&nbsp;{/if}
      </FONT>
    </TD>
    <TD align="right" NOWRAP><FONT color="red">
      {if $clientArray[sec1].balance < 0}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$clientArray[sec1].balance}
      {else}&nbsp;{/if}
      </FONT>
    </TD>
    <TD>&nbsp;</TD>
    <TD>&nbsp;</TD>
    <TD>&nbsp;</TD>
    {/if}
  </TR>
    {/section}
   <TR>
    <TD Align="right" colspan="1">
      <b>Total :</b>
    </TD>
    <TD align="right">
      <b>{if $openingTotal >= 0} <FONT color="blue"> +{else}<font color="red">{/if}{$openingTotal}</font></b> &nbsp;
    </TD>
    <TD align="right">
      <b>{if $billAmountTotal >= 0} <font color="blue"> +{else}<font color="red">{/if}{$billAmountTotal}</font></b> &nbsp;
    </TD>

   {if $crDrSelected == "Cr"}
    <TD Align="right">
      <FONT color="blue"><b>+{$creditTotal}</b></FONT>
    </TD>
    <td align="center">&nbsp;</td>
    {elseif $crDrSelected == "Dr"}
    <td align="center">&nbsp;</td>
    <TD Align="right">
      <FONT color="red"><b>{$debitTotal}</b></FONT>
    </TD>
    {else}
    <TD Align="right">
      <FONT color="blue"><b>+{$creditTotal}</b></FONT>
    </TD>
    <TD Align="right">
      <FONT color="red"><b>{$debitTotal}</b></FONT>
    </TD>
    {/if}
    <TD>&nbsp;</TD>
    <TD>&nbsp;</TD>
    <TD>&nbsp;</TD>
   </TR>
   {if $crDrSelected == "All"}
   <TR>
    <TD align="right" colspan="3">
      <b>Total Balance : </b>
    </TD>
    <TD align="right" colspan="2">
       {if $grandTotal >= 0} <FONT color="blue">+{else}<FONT color="red">{/if}
      <b>{$grandTotal}</b>
      </FONT>
    </TD>
    <TD>&nbsp;</TD>
    <TD>&nbsp;</TD>
    <TD>&nbsp;</TD>
   </TR>
   {/if}
</TABLE>
   <!--Balance as on date {$toDate} = <B>{$totalBalOnDate}</B>-->
</form>
</BODY>
</html>