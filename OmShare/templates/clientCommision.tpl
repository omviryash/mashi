<HTML>
<HEAD><TITLE></TITLE>
<script type="text/javascript" src="./js/jquery.js"></script>
</HEAD>
<BODY>
<FORM name="form1" action="" method="post">
<a href="./index.php">Home</a>&nbsp;&nbsp;
<a href="./clientAdd.php">Add Party</a>&nbsp;&nbsp;
<a href="./commisionInCalc.php">Client's Commision</a>
<br/><br/>
<INPUT type="hidden" name="isUpdate" value="{$isUpdate}"/>
<TABLE border="1" width="100%" cellPadding="2" cellSpacing="0">
<TR>
  <TD>
    <B>Client :</B>
    <SELECT name="client" onChange="document.form1.submit();">
      <OPTION value="0">Select</OPTION>
      {html_options values=$client.id output=$client.name selected=$selectClient}
    </SELECT>
  </TD>
  <TD>
    <B>Exchange :</B>
    <SELECT name="exchange" onChange="document.form1.submit();">
      {html_options values=$exchange.name output=$exchange.name selected=$selExchange}
    </SELECT>
  </TD>
  <TD>
    <B>Parent Client :</B>
    <SELECT name="parentclient">
      <OPTION value="0">Select</OPTION>
      {html_options values=$parentClient.id output=$parentClient.name selected=$selParentClient}
    </SELECT>
  </TD>
  <TD>
    <B>Commission In :</B>
    <SELECT name="commissionIn">
      <OPTION value="L">Per Lot</OPTION>
      <OPTION value="P">Brok %</OPTION>
    </SELECT>
  </TD>
  <TD>
    <B>Commission :</B>
    <INPUT type="text" name="commission" value="{$selCommission}"/>
  </TD>
</TR>
<TR>
  <TD colspan="5">
  <INPUT type="submit" name="saveBtn" value="Save"/>
  </TD>
</TR>
</TABLE>
</FORM>
<SCRIPT type="text/javascript">
{if $selCommissionIn != ''}
  {literal}
  for(i = 0; i < document.form1.commissionIn.length; i++)
  {
    {/literal}
    if(document.form1.commissionIn[i].value == '{$selCommissionIn}')
      document.form1.commissionIn[i].selected = true;
    {literal}
  }
  {/literal}
{/if}
</SCRIPT>
</BODY>
</HTML>