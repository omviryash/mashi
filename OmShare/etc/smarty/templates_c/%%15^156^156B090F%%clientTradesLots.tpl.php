<?php /* Smarty version 2.6.10, created on 2016-02-29 05:55:56
         compiled from clientTradesLots.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'clientTradesLots.tpl', 63, false),array('function', 'math', 'clientTradesLots.tpl', 131, false),)), $this); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
  "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE type="text/css">
<?php echo '
td{font-weight: BOLD}
.lossStyle   {color: red}
.profitStyle {color: blue}
'; ?>

</STYLE>
<script type="text/javascript" src="./js/jquery.js"></script>
<script type="text/javascript">
<?php echo '
  function changeCheckBox()
  {
    $(".subCheckBox").each(function(e)
    {
      trades = this.name;
      if(this.checked == true)
        thisIsChecked = 1;
      else
        thisIsChecked = 0;
      $.ajax(
      {
        type:"POST",
        url:\'updateStatus.php\',
        data:
        {
          trades:trades,
          confirmed:thisIsChecked
        },
        success: function(response)
        {
        }
      });
    });
  }
  $(document).ready(function()
  {
    $(".mainCheckBox").change(function()
    {
      if($(\'.mainCheckBox\').is(\':checked\'))
        $(\'input:checkbox\').attr(\'checked\', true);
      else
        $(\'input:checkbox\').attr(\'checked\', false);
      changeCheckBox();
    });
  });
'; ?>

</script>
</HEAD>
<BODY onKeyPress="if(event.keyCode==27)  window.close();">
  <A href="index.php">Home</A>&nbsp;&nbsp;
<FORM name="form1" method="get" action="<?php echo $this->_tpl_vars['PHP_SELF']; ?>
">
<INPUT type="hidden" name="display" value="<?php echo $this->_tpl_vars['display']; ?>
">
<INPUT type="hidden" name="itemIdChanged" value="0">
<TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
<TR>
<?php if ($this->_tpl_vars['userType'] != 'client'): ?>
  <TD>Client : 
    <SELECT name="clientId" onChange="document.form1.submit();">
    <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['clientIdSelected']),'values' => ($this->_tpl_vars['clientIdValues']),'output' => ($this->_tpl_vars['clientIdOptions'])), $this);?>

    </SELECT>
  </TD>
<?php endif; ?>
  <TD>Item : 
    <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
    <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['itemIdSelected']),'values' => ($this->_tpl_vars['itemIdValues']),'output' => ($this->_tpl_vars['itemIdOptions'])), $this);?>

    </SELECT>
  </TD>
  <TD>Expiry : 
    <SELECT name="expiryDate" onChange="document.form1.submit();">
    <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['expiryDateSelected']),'values' => ($this->_tpl_vars['expiryDateValues']),'output' => ($this->_tpl_vars['expiryDateOptions'])), $this);?>

    </SELECT>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    <A href="selectDtSession.php?goTo=<?php echo $this->_tpl_vars['goTo']; ?>
">Date range</A> : <?php echo $this->_tpl_vars['fromDate']; ?>
 To : <?php echo $this->_tpl_vars['toDate']; ?>
</CENTER>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    <?php echo $this->_tpl_vars['message']; ?>

  </TD>
</TR>
</FORM>
</TABLE>
<FORM name="form2" method="post" action="./acStorePl.php">
<TABLE border="1" cellPadding="2" cellSpacing="0">
<TR>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Buy</TD>
  <TD colspan="2" align="center">Sell</TD>
  <TD colspan="6">&nbsp;</TD>
</TR>
<TR>
  <TD align="center"><input type="checkbox" class="mainCheckBox" /> BuySell</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Date</TD>
  <TD align="center">Stand</TD>
  <TD align="center">Item</TD>
  <TD align="center">NetProfitLoss</TD>
<?php if ($this->_tpl_vars['userType'] != 'client'): ?>
  <TD align="center">Client2</TD>
  <TD align="center">Delete</TD>
<?php endif; ?>
  <?php if ($this->_tpl_vars['display'] == 'detailed'): ?> 
    <TD align="center">UserRemarks</TD>
    <TD align="center">OwnClient</TD>
    <TD align="center" NOWRAP>TradeRefNo</TD>
  <?php endif; ?>
</TR>
<?php unset($this->_sections['sec1']);
$this->_sections['sec1']['name'] = 'sec1';
$this->_sections['sec1']['loop'] = is_array($_loop=($this->_tpl_vars['trades'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec1']['show'] = true;
$this->_sections['sec1']['max'] = $this->_sections['sec1']['loop'];
$this->_sections['sec1']['step'] = 1;
$this->_sections['sec1']['start'] = $this->_sections['sec1']['step'] > 0 ? 0 : $this->_sections['sec1']['loop']-1;
if ($this->_sections['sec1']['show']) {
    $this->_sections['sec1']['total'] = $this->_sections['sec1']['loop'];
    if ($this->_sections['sec1']['total'] == 0)
        $this->_sections['sec1']['show'] = false;
} else
    $this->_sections['sec1']['total'] = 0;
if ($this->_sections['sec1']['show']):

            for ($this->_sections['sec1']['index'] = $this->_sections['sec1']['start'], $this->_sections['sec1']['iteration'] = 1;
                 $this->_sections['sec1']['iteration'] <= $this->_sections['sec1']['total'];
                 $this->_sections['sec1']['index'] += $this->_sections['sec1']['step'], $this->_sections['sec1']['iteration']++):
$this->_sections['sec1']['rownum'] = $this->_sections['sec1']['iteration'];
$this->_sections['sec1']['index_prev'] = $this->_sections['sec1']['index'] - $this->_sections['sec1']['step'];
$this->_sections['sec1']['index_next'] = $this->_sections['sec1']['index'] + $this->_sections['sec1']['step'];
$this->_sections['sec1']['first']      = ($this->_sections['sec1']['iteration'] == 1);
$this->_sections['sec1']['last']       = ($this->_sections['sec1']['iteration'] == $this->_sections['sec1']['total']);
?>
<?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId'] != $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['prevClientId'] || $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemId'] != $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['prevItemId'] || $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['expiryDate'] != $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['prevExpiryDate']): ?>
  <TR>
    <TD colspan="5"><U><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
 : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientName']; ?>
</U>&nbsp;:&nbsp;(<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientDeposit']; ?>
)</TD>
    <TD colspan="6" align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemIdExpiry']; ?>
</TD>
  </TR>
<?php endif; ?>
<TR style="color:<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['fontColor']; ?>
">
  <TD>
    <input type="checkbox" <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['confirmed'] == 1): ?> checked <?php endif; ?>name="trade_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
" class="subCheckBox" onchange="changeCheckBox();" />
    <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buySell']; ?>
</TD>
  <TD align="right">
    <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buyQty'] != "&nbsp;"): ?>
    <?php echo smarty_function_math(array('equation' => "qty/min",'qty' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buyQty'],'min' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['min']), $this);?>

    <?php else: ?>
    <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buyQty']; ?>

    <?php endif; ?>
  </TD>
  <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['price']; ?>
</TD>
  <TD align="right">
    <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['sellQty'] != "&nbsp;"): ?>
    <?php echo smarty_function_math(array('equation' => "qty/min",'qty' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['sellQty'],'min' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['min']), $this);?>

    <?php else: ?>
    <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['sellQty']; ?>

    <?php endif; ?>
  </TD>
  <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['sellPrice']; ?>
</TD>
  <TD align="center" NOWRAP><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeDate']; ?>

    <?php if ($this->_tpl_vars['display'] == 'detailed'): ?> <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeTime']; ?>
 <?php endif; ?>
  </TD>
  <TD align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['standing']; ?>
</TD>
  <TD align="center" NOWRAP><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemIdExpiry']; ?>
</TD>
  <TD>&nbsp;</TD>
<?php if ($this->_tpl_vars['userType'] != 'client'): ?>
  <TD align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId2']; ?>
 : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['client2Name']; ?>
</TD>
  <TD>
    <A onclick="return confirm('Are you sure?');" href="deleteTxt.php?goTo=<?php echo $this->_tpl_vars['goTo']; ?>
&tradeId=<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
">
    <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['standing'] != 'Close'): ?>
      Delete
    <?php else: ?>
      Delete Stand
    <?php endif; ?>
    </A>
    &nbsp;
    <A href="<?php echo $this->_tpl_vars['edit2File']; ?>
?tradeId=<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
&exchange=<?php echo $this->_tpl_vars['exchange']; ?>
&goTo=<?php echo $this->_tpl_vars['goTo']; ?>
">Edit2</A>
  </TD>
<?php endif; ?>
  <?php if ($this->_tpl_vars['display'] == 'detailed'): ?> 
    <TD align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['userRemarks']; ?>
</TD>
    <TD align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['ownClient']; ?>
</TD>
    <TD align="center" NOWRAP><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeRefNo']; ?>
</TD>
  <?php endif; ?>
  <?php if ($this->_tpl_vars['displayProfitLossUpToThis'] == 1): ?>
    <td><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['profitLossUpToThis']; ?>
</td>
  <?php endif; ?>
</TR>
<?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['dispGross'] != 0): ?>
  <TR>
    <TD align="right" NOWRAP>
    	<?php if (( $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totBuyQty'] - $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totSellQty'] ) >= 0): ?>
    	  <FONT color="blue">
    	<?php else: ?>
    	  <FONT color="red">
    	<?php endif; ?>
      Net: <?php echo smarty_function_math(array('equation' => "(totBuyQty-totSellQty)/min",'totBuyQty' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totBuyQty'],'totSellQty' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totSellQty'],'min' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['min']), $this);?>

        </FONT>
    </TD>
    <TD align="right"><FONT color="blue"><?php echo smarty_function_math(array('equation' => "qty/min",'qty' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totBuyQty'],'min' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['min']), $this);?>
</FONT></TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buyRash']; ?>
</TD>
    <TD align="right"><FONT color="red"><?php echo smarty_function_math(array('equation' => "qty/min",'qty' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totSellQty'],'min' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['min']), $this);?>
</FONT></TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['sellRash']; ?>
</TD>
  <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totBuyQty'] == $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totSellQty']): ?> 
    <TD colspan="3" align="right" NOWRAP>
      <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['profitLoss'] < 0): ?>
        <FONT class="lossStyle">Loss : 
      <?php else: ?>
        <FONT class="profitStyle">Profit : 
      <?php endif; ?>
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['profitLoss']; ?>
</FONT>
      Brok       : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['oneSideBrok']; ?>
</TD>
    <TD align="right" NOWRAP>
      <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['netProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['netProfitLoss']; ?>
</FONT>
    </TD>
    <TD colspan="2">&nbsp;</TD>
  <?php else: ?>
    <TD colspan="6"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemIdExpiry']; ?>
 : Buy Sell Qty Not Same</TD>
  <?php endif; ?>
  </TR>
  <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['dispClientWhole'] != 0): ?>
  <TR>
    <TD colspan="5" align="right">
      : Total : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
 : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientName']; ?>

    </TD>
    <TD colspan="3" align="right"><U>
      <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotProfitLoss'] < 0): ?>
        <FONT class="lossStyle">Loss : 
      <?php else: ?>
        <FONT class="profitStyle">Profit : 
      <?php endif; ?>
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotProfitLoss']; ?>
</FONT></U>
      Brok       : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotBrok']; ?>
</U>
    </TD>
    <TD align="right"><U>
      <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotNetProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotNetProfitLoss']; ?>
</FONT></U></TD>
    <TD align="center" colspan="2">
      &nbsp;</TD>
  </TR>
  <?php endif; ?>
<?php endif; ?>
<?php endfor; endif; ?>
<TR><TD colspan="11">&nbsp;</TD></TR>
<TR>
  <TD align="center">Net</TD>
  <TD>Buy</TD>
  <TD>Average</TD>
  <TD>Sell</TD>
  <TD>Average</TD>
  <TD colspan="2" align="center">Item</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD align="center">NetProfitLoss</TD>
</TR>
<?php unset($this->_sections['sec2']);
$this->_sections['sec2']['name'] = 'sec2';
$this->_sections['sec2']['loop'] = is_array($_loop=($this->_tpl_vars['wholeItemArr'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec2']['show'] = true;
$this->_sections['sec2']['max'] = $this->_sections['sec2']['loop'];
$this->_sections['sec2']['step'] = 1;
$this->_sections['sec2']['start'] = $this->_sections['sec2']['step'] > 0 ? 0 : $this->_sections['sec2']['loop']-1;
if ($this->_sections['sec2']['show']) {
    $this->_sections['sec2']['total'] = $this->_sections['sec2']['loop'];
    if ($this->_sections['sec2']['total'] == 0)
        $this->_sections['sec2']['show'] = false;
} else
    $this->_sections['sec2']['total'] = 0;
if ($this->_sections['sec2']['show']):

            for ($this->_sections['sec2']['index'] = $this->_sections['sec2']['start'], $this->_sections['sec2']['iteration'] = 1;
                 $this->_sections['sec2']['iteration'] <= $this->_sections['sec2']['total'];
                 $this->_sections['sec2']['index'] += $this->_sections['sec2']['step'], $this->_sections['sec2']['iteration']++):
$this->_sections['sec2']['rownum'] = $this->_sections['sec2']['iteration'];
$this->_sections['sec2']['index_prev'] = $this->_sections['sec2']['index'] - $this->_sections['sec2']['step'];
$this->_sections['sec2']['index_next'] = $this->_sections['sec2']['index'] + $this->_sections['sec2']['step'];
$this->_sections['sec2']['first']      = ($this->_sections['sec2']['iteration'] == 1);
$this->_sections['sec2']['last']       = ($this->_sections['sec2']['iteration'] == $this->_sections['sec2']['total']);
?>
<TR>
    <TD align="right" NOWRAP>
    	<?php if (( $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['buyQty'] - $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['sellQty'] ) >= 0): ?>
    	  <FONT color="blue">
    	<?php else: ?>
    	  <FONT color="red">
    	<?php endif; ?>
      <?php echo smarty_function_math(array('equation' => "(buyQty-sellQty)/min",'buyQty' => $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['buyQty'],'sellQty' => $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['sellQty'],'min' => $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['min']), $this);?>

        </FONT>
    </TD>
  <TD align="right"><FONT color="blue"><?php echo smarty_function_math(array('equation' => "qty/min",'qty' => $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['buyQty'],'min' => $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['min']), $this);?>
</FONT></TD>
  <TD align="right"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['buyRash']; ?>
</TD>
  <TD align="right"><FONT color="red"><?php echo smarty_function_math(array('equation' => "qty/min",'qty' => $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['sellQty'],'min' => $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['min']), $this);?>
</FONT></TD>
  <TD align="right"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['sellRash']; ?>
</TD>
  <TD align="right" colspan="2" NOWRAP><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['itemIdExpiry']; ?>
</TD>
  <TD align="right" NOWRAP>
    <?php if ($this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['profitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
    <?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['profitLoss']; ?>
</FONT></TD>
  <TD align="right" colspan="2"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['oneSideBrok']; ?>
</TD>
  <TD align="right" NOWRAP>
    <?php if ($this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['netProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
    <?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['netProfitLoss']; ?>
</FONT></TD>
</TR>
<?php endfor; endif; ?>
<TR>
    <TD align="right" NOWRAP>
      &nbsp;
    </TD>
  <TD align="right">&nbsp;</TD>
  <TD align="right"><?php echo $this->_tpl_vars['wholeBuyRash']; ?>
</TD>
  <TD align="right">&nbsp;</TD>
  <TD align="right"><?php echo $this->_tpl_vars['wholeSellRash']; ?>
</TD>
  <TD align="right" colspan="2" NOWRAP> : Total : </TD>
  <TD align="right" NOWRAP>
    <?php if ($this->_tpl_vars['wholeProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
    <?php echo $this->_tpl_vars['wholeProfitLoss']; ?>
</FONT></TD>
  <TD align="right" colspan="2"><?php echo $this->_tpl_vars['wholeOneSideBrok']; ?>
</TD>
  <TD align="right" NOWRAP>
    <?php if ($this->_tpl_vars['wholeNetProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
    <?php echo $this->_tpl_vars['wholeNetProfitLoss']; ?>
</FONT></TD>
</TR>
</TABLE>
</BODY>
</HTML>