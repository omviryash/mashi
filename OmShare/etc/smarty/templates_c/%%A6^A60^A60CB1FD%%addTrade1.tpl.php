<?php /* Smarty version 2.6.10, created on 2016-02-28 16:42:33
         compiled from addTrade1.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_select_time', 'addTrade1.tpl', 52, false),array('function', 'html_select_date', 'addTrade1.tpl', 56, false),array('function', 'html_options', 'addTrade1.tpl', 62, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <script type="text/javascript" src="./js/jquery.js"></script>
  <script type='text/javascript' src="./js/jquery.autocomplete.js"></script>
  <link rel="stylesheet" type="text/css" href="./css/jquery.autocomplete.css" />
  <?php echo '
  <style type="text/css">
    span  {
      color:white;
    }
    a  {
      color:white;
    }
    div#storeOrPendig1  {
      float:left;
      display:inline;
      width:49%;
      border:1px solid red;
    }
    div#storeOrPendig2  {
      float:right;
      display:inline;
      width:49%;
      border:1px dotted red;
    }
  </style>
  '; ?>

  <title>!! MCX !! Add Trade</title>
</head>
<body <?php if ($this->_tpl_vars['buyOrSell'] == 'Buy'): ?> bgColor="blue" <?php else: ?> bgColor="red" <?php endif; ?> id="body" >
<form name="form1" action="<?php echo $this->_tpl_vars['PHP_SELF']; ?>
" >
<input type="hidden" name="tradeHidden" value="trade" />
  <span>
    <a href="index.php">Home</a> 
    <?php if ($this->_tpl_vars['packfor'] == 5 && $this->_tpl_vars['exchange'] == 'MCX'): ?>
      <a href="addTrade.php?exchange=F_O">F_O</a>
    <?php endif; ?>
    <?php if ($this->_tpl_vars['packfor'] == 5 && $this->_tpl_vars['exchange'] == 'F_O'): ?>
      <a href="addTrade.php?exchange=MCX">MCX</a>
    <?php endif; ?>
  </span>
  <?php if ($this->_tpl_vars['clientIdAskInTextBox'] == 1): ?>
  [ <span>Client Id 1: </span  ><span id="clientIdDiv" style="color:yellow;font-weight:bold;"></span> ]
  <?php if ($this->_tpl_vars['twoClientTextBox'] == 1): ?>
  [ <span>Client Id 2: </span  ><span id="clientIdDiv2" style="color:yellow;font-weight:bold;"></span> ]
  <?php endif; ?>
  <span id="errorMsg" style="color:red;font-weight:bold"></span>
  <br />
  <?php endif; ?>
  <?php if ($this->_tpl_vars['askTime'] == 1): ?>
    <?php echo smarty_function_html_select_time(array('prefix' => 'timeHidden','use_24_hours' => true,'time' => $this->_tpl_vars['trade']['tradeTime']), $this);?>

  <?php else: ?>
    <input type="hidden" name="timeHidden" />
  <?php endif; ?>
  <?php echo smarty_function_html_select_date(array('day_value_format' => "%02d",'month_value_format' => "%m",'day_format' => "%d",'month_format' => "%m",'field_order' => 'DMY','time' => $this->_tpl_vars['trade']['tradeDate']), $this);?>

  <span>Client 1:</span>
  <?php if ($this->_tpl_vars['clientIdAskInTextBox'] == 1): ?>
  <input type="text" name="clientId" id="clientId" size=4 value="<?php echo $this->_tpl_vars['trade']['clientId']; ?>
" />
  <?php else: ?>
  <select name="clientId" id="clientId" >
    <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['clientIdValues'],'output' => $this->_tpl_vars['clientIdOutput'],'selected' => $this->_tpl_vars['trade']['clientId']), $this);?>

  </select>
  <?php endif; ?>
  <?php if ($this->_tpl_vars['twoClientTextBox'] == 1): ?>
  <span>Client 2:</span>
  <?php if ($this->_tpl_vars['clientIdAskInTextBox'] == 1): ?>
  <input type="text" name="clientId2" id="clientId2" size=4 value="<?php echo $this->_tpl_vars['defaultClientId2']; ?>
" />
  <?php else: ?>
  <select name="clientId2" id="clientId2" >
    <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['clientId2Values'],'output' => $this->_tpl_vars['clientId2Output'],'selected' => $this->_tpl_vars['defaultClientId2']), $this);?>

  </select>
  <?php endif; ?>
  <?php else: ?>
    <input type="hidden" name="clientId2" id="clientId2" size=4 value="<?php echo $this->_tpl_vars['defaultClientId2']; ?>
" />
  <?php endif; ?>
  <span>Price 1: <input type="text" name="price1" id="price1" size="6" onkeydown="changePrice(this,event);" value="<?php echo $this->_tpl_vars['trade']['price']; ?>
" /></span>
  <?php if ($this->_tpl_vars['twoClientTextBox'] == 1): ?>
  <span>Price 2: <input type="text" name="price2" id="price2" size="6" onkeydown="changePrice(this,event);" value="<?php echo $this->_tpl_vars['trade']['price2']; ?>
"/></span><br />
  <?php else: ?>
  <input type="hidden" name="price2" id="price2" /><br />
  <?php endif; ?>
  <input type="text" name="buySell" id="buySell" value="<?php echo $this->_tpl_vars['buyOrSell']; ?>
" disabled size="10"/>
  <input type="hidden" name="buySellHidden" id="buySellHidden" value="<?php echo $this->_tpl_vars['buyOrSell']; ?>
" />
  <?php if ($this->_tpl_vars['exchange'] == 'MCX' || $this->_tpl_vars['exchange'] == 'CX'): ?>
  <select name="itemId" onchange="itemChange(this);">
    <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['item']['itemId'],'output' => $this->_tpl_vars['item']['item'],'selected' => $this->_tpl_vars['selectedItem']), $this);?>

  </select>
  <?php else: ?>
  <input type="text" name="itemId" id="itemId" onkeydown="itemChange(this);" value="<?php echo $this->_tpl_vars['selectedItem']; ?>
" />
  <?php endif; ?>
  <select name="expiryDate" id="expiryDate">
  </select> &nbsp;
  <span>Qty.by Lot : <input type="text" name="lot" id="lot" size="3" value="<?php echo $this->_tpl_vars['lot']; ?>
" onkeydown="changePrice(this,event);" /> &nbsp;</span>
  <span>Quantity : <input type="text" name="quantity" id="quantity" size="5" disabled  /></span>
  <input type="hidden" name="quantityHidden" id="quantityHidden" />
  <input type="hidden" name="min" id="min" />
  <input type="hidden" name="exchange" id="exchange" value="<?php echo $this->_tpl_vars['exchange']; ?>
" />
  <input type="hidden" name="tradeId" id="tradeId" value="<?php echo $this->_tpl_vars['tradeId']; ?>
" />
  <input type="hidden" name="refTradeId" id="refTradeId" value="<?php echo $this->_tpl_vars['refTradeId']; ?>
" />
  <?php if ($this->_tpl_vars['forStand'] == 1): ?>
    <select name="standing">
      <option name="open" value="-1">Open Standing</option>
      <option name="close" value="1">Close Standing</option>
    </select>
  <?php else: ?>
    <input type="hidden" name="standing" value="0" />
  <?php endif; ?>
  <input type="button" name="button1" id="trade" value="Trade" />
  <input type="button" name="button2" id="rl" value="RL" />
  <input type="button" name="button3" id="sl" value="SL" />
  <span><a href="javascript:void(0);" style="color:white;" title="(Shift + Enter => RL) (Ctrl + Shift + Enter => SL) (Ctrl + Enter => Trade)" >Help</a></span>
<div id="storeOrPendig1"></div>
<div id="storeOrPendig2"></div>
</form>
<script type="text/javascript">
  var item = new Array();
  var rangeStart = new Array();
  var rangeEnd = new Array();
  <?php $_from = $this->_tpl_vars['item']['item']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['itemName']):
?>
  item[<?php echo $this->_tpl_vars['id']; ?>
] = '<?php echo $this->_tpl_vars['itemName']; ?>
';
  <?php endforeach; endif; unset($_from); ?>
  <?php $_from = $this->_tpl_vars['item']['rangeStart']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rangeStartId'] => $this->_tpl_vars['rangeStart']):
?>
  rangeStart[<?php echo $this->_tpl_vars['rangeStartId']; ?>
] = <?php echo $this->_tpl_vars['rangeStart']; ?>
;
  <?php endforeach; endif; unset($_from); ?>
  <?php $_from = $this->_tpl_vars['item']['rangeEnd']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rangeEndId'] => $this->_tpl_vars['rangeEnd']):
?>
  rangeEnd[<?php echo $this->_tpl_vars['rangeEndId']; ?>
] = <?php echo $this->_tpl_vars['rangeEnd']; ?>
;
  <?php endforeach; endif; unset($_from); ?>

  <?php echo '
  $(document).ready(function()
  {
    $("#rl").click(function()
    {
      document.form1.tradeHidden.value = "rl";
      $("#trade").click();
      return false;
    });
    $("#sl").click(function()
    {
      document.form1.tradeHidden.value = "sl";
      $("#trade").click();
      return false;
    });
    holdingArray = new Array();
    var holdingArrayLength = holdingArray.length;

    var now = new Date();
    var hour = now.getHours();
    if (hour < 10)
      hour = \'0\'+hour;
    var minute = now.getMinutes();
    if (minute < 10)
      minute = \'0\'+minute;
    var second = now.getSeconds();
    if (second < 10)
      second = \'0\'+second;
    var sTime = hour + ":" + minute + ":" + second;
    //document.form1.timeHidden.value= sTime;
    $("#trade").click(function()
    {
      '; ?>

      <?php if ($this->_tpl_vars['twoClientTextBox'] != 1): ?>
      document.form1.price2.value = document.form1.price1.value;
      <?php endif; ?>
      <?php echo '
      var confirmMessage;
      if(document.form1.tradeHidden.value == "trade")
        confirmMessage = "Are you sure to Trade?";
      if(document.form1.tradeHidden.value == "rl")
        confirmMessage = "Are you sure to RL?";
      if(document.form1.tradeHidden.value == "sl")
        confirmMessage = "Are you sure to SL?";
      if(document.form1.clientId.value == document.form1.clientId2.value)
        alert("Client 1 amd client 2 same not allowed");
      else
      {
        if(confirm(confirmMessage))
        {
          var storeOrPendigInnerHtml1 = "";
          var storeOrPendigInnerHtml2 = "";
          '; ?>

          <?php if ($this->_tpl_vars['askTime'] != 1): ?>
          var now = new Date();
          var hour = now.getHours();
          if (hour < 10)
            hour = '0'+hour;
          var minute = now.getMinutes();
          if (minute < 10)
            minute = '0'+minute;
          var second = now.getSeconds();
          if (second < 10)
            second = '0'+second;
          var sTime = hour + ":" + minute + ":" + second;
          document.form1.timeHidden.value = sTime;
          <?php else: ?>
          sTime = document.form1.timeHiddenHour.value + ":" + document.form1.timeHiddenMinute.value + ":" + document.form1.timeHiddenSecond.value;
          <?php endif; ?>
          <?php echo '
          holdingArray[holdingArrayLength] = new Array();
          holdingArray[holdingArrayLength][\'dateObjectArray\'] = document.form1.Date_Year.value +"-"+ document.form1.Date_Month.value +"-"+ document.form1.Date_Day.value +" "+ sTime;
          '; ?>

          <?php if ($this->_tpl_vars['clientIdAskInTextBox'] != 1): ?>
          holdingArray[holdingArrayLength]['clientIdArray']   = document.form1.clientId.options[document.form1.clientId.selectedIndex].text;
          <?php if ($this->_tpl_vars['twoClientTextBox'] == 1): ?>
          holdingArray[holdingArrayLength]['clientId2Array']  = document.form1.clientId2.options[document.form1.clientId2.selectedIndex].text;
          <?php else: ?>
          holdingArray[holdingArrayLength]['clientId2Array']  = "Self";
          <?php endif; ?>
          <?php else: ?>
          holdingArray[holdingArrayLength]['clientIdArray']   = document.getElementById("clientIdDiv").innerHTML;
          <?php if ($this->_tpl_vars['twoClientTextBox'] == 1): ?>
          holdingArray[holdingArrayLength]['clientId2Array']  = document.getElementById("clientIdDiv2").innerHTML;
          <?php else: ?>
          holdingArray[holdingArrayLength]['clientId2Array']  = "Self";
          <?php endif; ?>
          <?php endif; ?>
          holdingArray[holdingArrayLength]['price1Array']     = document.form1.price1.value;
          holdingArray[holdingArrayLength]['price2Array']     = document.form1.price2.value;
          holdingArray[holdingArrayLength]['lotArray']        = document.form1.lot.value;
          holdingArray[holdingArrayLength]['quantityArray']   = document.form1.quantity.value;
          holdingArray[holdingArrayLength]['buySellHidden']   = document.form1.buySellHidden.value;
          <?php if ($this->_tpl_vars['exchange'] == 'MCX' || $this->_tpl_vars['exchange'] == 'CX'): ?>
          holdingArray[holdingArrayLength]['itemIdArray']     = document.form1.itemId.options[document.form1.itemId.selectedIndex].text;
          <?php else: ?>
          holdingArray[holdingArrayLength]['itemIdArray']     = document.form1.itemId.value;
          <?php endif; ?>
            <?php echo '
          holdingArray[holdingArrayLength][\'tradeHiddenArray\']= document.form1.tradeHidden.value
          holdingArray[holdingArrayLength][\'elementNoArray\']  = holdingArray.length;
          holdingArray[holdingArrayLength][\'status\']          = "Pending";
          holdingArrayLength +=1;
          if(document.form1.tradeId.value == 0)
            toDo = "Insert";
          else
            toDo = "Update";
  
          $.ajax(
          {
            type:"POST",'; ?>

            url :'saveRecord.php',<?php echo '
            data:
            {
              dateObject    :document.form1.Date_Year.value +"-"+ document.form1.Date_Month.value +"-"+ document.form1.Date_Day.value,
              clientId      :document.form1.clientId.value,
              clientId2     :document.form1.clientId2.value,
              price1        :document.form1.price1.value,
              price2        :document.form1.price2.value,
              itemId        :document.form1.itemId.value,
              buySellHidden :document.form1.buySellHidden.value,
              expiryDate    :document.form1.expiryDate.value,
              standing      :document.form1.standing.value,
              quantityHidden:document.form1.quantityHidden.value,
              exchange      :document.form1.exchange.value,
              trade         :document.form1.tradeHidden.value,
              tradeId       :document.form1.tradeId.value,
              refTradeId    :document.form1.refTradeId.value,
              selfRefId     :0,
              sTime         :sTime,
              toDo          :toDo,
              status        :"Pending",
              elementNo     :(holdingArray.length)-1
            },
            success: function(response)
            {
              var handleResponce = response.split(",");
              if(handleResponce[0] != "I")
              {
                if(handleResponce[0] == "Stored")
                  holdingArray[handleResponce[1]][\'status\'] = "Stored";
                else if(handleResponce[0] == "Pending")
                  holdingArray[handleResponce[1]][\'status\'] = "Pending";
                var storeOrPendigInnerHtml1 = "";
                var storeOrPendigInnerHtml2 = "";
                for(i = (holdingArray.length - 1) ; i >= 0 ; i--)
                {
                  if(holdingArray[i][\'status\'] == "Pending")
                  {
                    storeOrPendigInnerHtml1 += holdingArray[i][\'tradeHiddenArray\']+"&nbsp;";
                    storeOrPendigInnerHtml1 += holdingArray[i][\'dateObjectArray\']+"&nbsp;";
                    storeOrPendigInnerHtml1 += holdingArray[i][\'buySellHidden\']+"&nbsp;";
                    storeOrPendigInnerHtml1 += holdingArray[i][\'clientIdArray\']+"&nbsp;";
                    storeOrPendigInnerHtml1 += holdingArray[i][\'clientId2Array\']+"&nbsp;";
                    storeOrPendigInnerHtml1 += holdingArray[i][\'price1Array\']+"&nbsp;";
                    storeOrPendigInnerHtml1 += holdingArray[i][\'price2Array\']+"&nbsp;";
                    storeOrPendigInnerHtml1 += holdingArray[i][\'itemIdArray\']+"&nbsp;";
                    storeOrPendigInnerHtml1 += holdingArray[i][\'lotArray\']+"&nbsp;";
                    storeOrPendigInnerHtml1 += "("+holdingArray[i][\'quantityArray\']+")&nbsp;";
                    storeOrPendigInnerHtml1 += holdingArray[i][\'status\']+"<hr />";
                  }
                  else if(holdingArray[i][\'status\'] == "Stored")
                  {
                    storeOrPendigInnerHtml2 += holdingArray[i][\'tradeHiddenArray\']+"&nbsp;";
                    storeOrPendigInnerHtml2 += holdingArray[i][\'dateObjectArray\']+"&nbsp;";
                    storeOrPendigInnerHtml2 += holdingArray[i][\'buySellHidden\']+"&nbsp;";
                    storeOrPendigInnerHtml2 += holdingArray[i][\'clientIdArray\']+"&nbsp;";
                    storeOrPendigInnerHtml2 += holdingArray[i][\'clientId2Array\']+"&nbsp;";
                    storeOrPendigInnerHtml2 += holdingArray[i][\'price1Array\']+"&nbsp;";
                    storeOrPendigInnerHtml2 += holdingArray[i][\'price2Array\']+"&nbsp;";
                    storeOrPendigInnerHtml2 += holdingArray[i][\'itemIdArray\']+"&nbsp;";
                    storeOrPendigInnerHtml2 += holdingArray[i][\'lotArray\']+"&nbsp;";
                    storeOrPendigInnerHtml2 += "("+holdingArray[i][\'quantityArray\']+")&nbsp;";
                    storeOrPendigInnerHtml2 += holdingArray[i][\'status\']+"<hr />";
                  }
                }
                $("#storeOrPendig1").html("<font color=\'white\'>"+storeOrPendigInnerHtml1+"</font>");
                $("#storeOrPendig2").html("<font color=\'white\'>"+storeOrPendigInnerHtml2+"</font>");
                $("#clientId").focus();
                $("#lot").val(1);
                $("#errorMsg").html("");
              }
              else
              {
                holdingArray[handleResponce[1]][\'status\'] = "I";
                holdingArray[handleResponce[1]].pop();
                $("#errorMsg").html("Invalid Client Id!");
              }
            }
          });
          for(i = (holdingArray.length - 1) ; i >= 0 ; i--)
          {
            if(holdingArray[i][\'status\'] == "Pending")
            {
              storeOrPendigInnerHtml1 += holdingArray[i][\'tradeHiddenArray\']+"&nbsp;";
              storeOrPendigInnerHtml1 += holdingArray[i][\'dateObjectArray\']+"&nbsp;";
              storeOrPendigInnerHtml1 += holdingArray[i][\'buySellHidden\']+"&nbsp;";
              storeOrPendigInnerHtml1 += holdingArray[i][\'clientIdArray\']+"&nbsp;";
              storeOrPendigInnerHtml1 += holdingArray[i][\'clientId2Array\']+"&nbsp;";
              storeOrPendigInnerHtml1 += holdingArray[i][\'price1Array\']+"&nbsp;";
              storeOrPendigInnerHtml1 += holdingArray[i][\'price2Array\']+"&nbsp;";
              storeOrPendigInnerHtml1 += holdingArray[i][\'itemIdArray\']+"&nbsp;";
              storeOrPendigInnerHtml1 += holdingArray[i][\'lotArray\']+"&nbsp;";
              storeOrPendigInnerHtml1 += "("+holdingArray[i][\'quantityArray\']+")&nbsp;";
              storeOrPendigInnerHtml1 += holdingArray[i][\'status\']+"<hr />";
            }
            else if(holdingArray[i][\'status\'] == "Stored")
            {
              storeOrPendigInnerHtml2 += holdingArray[i][\'tradeHiddenArray\']+"&nbsp;";
              storeOrPendigInnerHtml2 += holdingArray[i][\'dateObjectArray\']+"&nbsp;";
              storeOrPendigInnerHtml2 += holdingArray[i][\'buySellHidden\']+"&nbsp;";
              storeOrPendigInnerHtml2 += holdingArray[i][\'clientIdArray\']+"&nbsp;";
              storeOrPendigInnerHtml2 += holdingArray[i][\'clientId2Array\']+"&nbsp;";
              storeOrPendigInnerHtml2 += holdingArray[i][\'price1Array\']+"&nbsp;";
              storeOrPendigInnerHtml2 += holdingArray[i][\'price2Array\']+"&nbsp;";
              storeOrPendigInnerHtml2 += holdingArray[i][\'itemIdArray\']+"&nbsp;";
              storeOrPendigInnerHtml2 += holdingArray[i][\'lotArray\']+"&nbsp;";
              storeOrPendigInnerHtml2 += "("+holdingArray[i][\'quantityArray\']+")&nbsp;";
              storeOrPendigInnerHtml2 += holdingArray[i][\'status\']+"<hr />";
            }
          }
          $("#storeOrPendig1").html("<font color=\'white\'>"+storeOrPendigInnerHtml1+"</font>");
          $("#storeOrPendig2").html("<font color=\'white\'>"+storeOrPendigInnerHtml2+"</font>");
          '; ?>

          <?php if ($this->_tpl_vars['isEdit'] == 1): ?>
          location.href = "<?php echo $this->_tpl_vars['goTo']; ?>
.php";
          <?php endif; ?>
          <?php echo '
        }
      }
    });
    $("#clientId").keydown(function()
    {
      '; ?>

      <?php if ($this->_tpl_vars['clientIdAskInTextBox'] == 1): ?>
      $("#clientIdDiv").html("");
      <?php unset($this->_sections['secClient1']);
$this->_sections['secClient1']['name'] = 'secClient1';
$this->_sections['secClient1']['loop'] = is_array($_loop=$this->_tpl_vars['clientIdValues']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['secClient1']['show'] = true;
$this->_sections['secClient1']['max'] = $this->_sections['secClient1']['loop'];
$this->_sections['secClient1']['step'] = 1;
$this->_sections['secClient1']['start'] = $this->_sections['secClient1']['step'] > 0 ? 0 : $this->_sections['secClient1']['loop']-1;
if ($this->_sections['secClient1']['show']) {
    $this->_sections['secClient1']['total'] = $this->_sections['secClient1']['loop'];
    if ($this->_sections['secClient1']['total'] == 0)
        $this->_sections['secClient1']['show'] = false;
} else
    $this->_sections['secClient1']['total'] = 0;
if ($this->_sections['secClient1']['show']):

            for ($this->_sections['secClient1']['index'] = $this->_sections['secClient1']['start'], $this->_sections['secClient1']['iteration'] = 1;
                 $this->_sections['secClient1']['iteration'] <= $this->_sections['secClient1']['total'];
                 $this->_sections['secClient1']['index'] += $this->_sections['secClient1']['step'], $this->_sections['secClient1']['iteration']++):
$this->_sections['secClient1']['rownum'] = $this->_sections['secClient1']['iteration'];
$this->_sections['secClient1']['index_prev'] = $this->_sections['secClient1']['index'] - $this->_sections['secClient1']['step'];
$this->_sections['secClient1']['index_next'] = $this->_sections['secClient1']['index'] + $this->_sections['secClient1']['step'];
$this->_sections['secClient1']['first']      = ($this->_sections['secClient1']['iteration'] == 1);
$this->_sections['secClient1']['last']       = ($this->_sections['secClient1']['iteration'] == $this->_sections['secClient1']['total']);
?>
      if(this.value == <?php echo $this->_tpl_vars['clientIdValues'][$this->_sections['secClient1']['index']]; ?>
)
        $("#clientIdDiv").html("<?php echo $this->_tpl_vars['clientIdOutput'][$this->_sections['secClient1']['index']]; ?>
");
      <?php endfor; endif; ?>
      if(document.getElementById("clientIdDiv").innerHTML == "")
        $("#clientIdDiv").html("Client not available");
      <?php endif; ?>
      <?php echo '
    });
    $("#clientId2").keydown(function()
    {
      '; ?>

      <?php if ($this->_tpl_vars['clientIdAskInTextBox'] == 1): ?>
      $("#clientIdDiv2").html("");
      <?php unset($this->_sections['secClient1']);
$this->_sections['secClient1']['name'] = 'secClient1';
$this->_sections['secClient1']['loop'] = is_array($_loop=$this->_tpl_vars['clientIdValues']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['secClient1']['show'] = true;
$this->_sections['secClient1']['max'] = $this->_sections['secClient1']['loop'];
$this->_sections['secClient1']['step'] = 1;
$this->_sections['secClient1']['start'] = $this->_sections['secClient1']['step'] > 0 ? 0 : $this->_sections['secClient1']['loop']-1;
if ($this->_sections['secClient1']['show']) {
    $this->_sections['secClient1']['total'] = $this->_sections['secClient1']['loop'];
    if ($this->_sections['secClient1']['total'] == 0)
        $this->_sections['secClient1']['show'] = false;
} else
    $this->_sections['secClient1']['total'] = 0;
if ($this->_sections['secClient1']['show']):

            for ($this->_sections['secClient1']['index'] = $this->_sections['secClient1']['start'], $this->_sections['secClient1']['iteration'] = 1;
                 $this->_sections['secClient1']['iteration'] <= $this->_sections['secClient1']['total'];
                 $this->_sections['secClient1']['index'] += $this->_sections['secClient1']['step'], $this->_sections['secClient1']['iteration']++):
$this->_sections['secClient1']['rownum'] = $this->_sections['secClient1']['iteration'];
$this->_sections['secClient1']['index_prev'] = $this->_sections['secClient1']['index'] - $this->_sections['secClient1']['step'];
$this->_sections['secClient1']['index_next'] = $this->_sections['secClient1']['index'] + $this->_sections['secClient1']['step'];
$this->_sections['secClient1']['first']      = ($this->_sections['secClient1']['iteration'] == 1);
$this->_sections['secClient1']['last']       = ($this->_sections['secClient1']['iteration'] == $this->_sections['secClient1']['total']);
?>
      if(this.value == <?php echo $this->_tpl_vars['clientIdValues'][$this->_sections['secClient1']['index']]; ?>
)
        $("#clientIdDiv2").html("<?php echo $this->_tpl_vars['clientIdOutput'][$this->_sections['secClient1']['index']]; ?>
");
      <?php endfor; endif; ?>
      if(document.getElementById("clientIdDiv2").innerHTML == "")
        $("#clientIdDiv2").html("Client not available");
      <?php endif; ?>
      <?php echo '
    });
    $("#itemId").autocomplete(item,
    {
      minChars: 0,
      max: 30,
      autoFill: true,
      mustMatch: true,
      matchContains: false,
      scrollHeight: 220,
      formatItem: function(data, i, total)
      {
        return data[0];
      }
    });
    $("#lot").keydown(function()
    {
      minQuantityFunc(this.value);
    });
    $("#price1").blur(function()
    {
      '; ?>

      <?php if ($this->_tpl_vars['isEdit'] == 0): ?>
        $("#price2").val(this.value);
      <?php unset($this->_sections['sec']);
$this->_sections['sec']['name'] = 'sec';
$this->_sections['sec']['loop'] = is_array($_loop=$this->_tpl_vars['item']['rangeStart']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec']['show'] = true;
$this->_sections['sec']['max'] = $this->_sections['sec']['loop'];
$this->_sections['sec']['step'] = 1;
$this->_sections['sec']['start'] = $this->_sections['sec']['step'] > 0 ? 0 : $this->_sections['sec']['loop']-1;
if ($this->_sections['sec']['show']) {
    $this->_sections['sec']['total'] = $this->_sections['sec']['loop'];
    if ($this->_sections['sec']['total'] == 0)
        $this->_sections['sec']['show'] = false;
} else
    $this->_sections['sec']['total'] = 0;
if ($this->_sections['sec']['show']):

            for ($this->_sections['sec']['index'] = $this->_sections['sec']['start'], $this->_sections['sec']['iteration'] = 1;
                 $this->_sections['sec']['iteration'] <= $this->_sections['sec']['total'];
                 $this->_sections['sec']['index'] += $this->_sections['sec']['step'], $this->_sections['sec']['iteration']++):
$this->_sections['sec']['rownum'] = $this->_sections['sec']['iteration'];
$this->_sections['sec']['index_prev'] = $this->_sections['sec']['index'] - $this->_sections['sec']['step'];
$this->_sections['sec']['index_next'] = $this->_sections['sec']['index'] + $this->_sections['sec']['step'];
$this->_sections['sec']['first']      = ($this->_sections['sec']['iteration'] == 1);
$this->_sections['sec']['last']       = ($this->_sections['sec']['iteration'] == $this->_sections['sec']['total']);
?>
      <?php if ($this->_tpl_vars['item']['rangeStart'][$this->_sections['sec']['index']] != $this->_tpl_vars['item']['rangeEnd'][$this->_sections['sec']['index']]): ?>
      <?php if ($this->_tpl_vars['item']['rangeStart'][$this->_sections['sec']['index']] > 0 && $this->_tpl_vars['item']['rangeEnd'][$this->_sections['sec']['index']] > 0): ?>
      if((document.form1.price1.value >= <?php echo $this->_tpl_vars['item']['rangeStart'][$this->_sections['sec']['index']]; ?>
) && (document.form1.price1.value <= <?php echo $this->_tpl_vars['item']['rangeEnd'][$this->_sections['sec']['index']]; ?>
))<?php echo '
      {
        '; ?>

        document.form1.min.value = <?php echo $this->_tpl_vars['item']['min'][$this->_sections['sec']['index']]; ?>
;
        <?php if ($this->_tpl_vars['exchange'] == 'MCX' || $this->_tpl_vars['exchange'] == 'CX'): ?>
        selectOptionByValue(document.form1.itemId, "<?php echo $this->_tpl_vars['item']['itemId'][$this->_sections['sec']['index']]; ?>
");
        minQuantityFunc(document.form1.lot.value);
        <?php endif; ?>
        <?php echo '
      }
      '; ?>

      <?php endif; ?>
      <?php endif; ?>
      <?php endfor; endif; ?>
      <?php endif; ?>
      <?php echo '
    });
    $(document).keydown(function(e)
    {
      var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      '; ?>

      <?php if ($this->_tpl_vars['exchange'] == 'MCX' || $this->_tpl_vars['exchange'] == 'CX'): ?>
      <?php echo '
      if(e.ctrlKey && code == 117)
      {
        window.open("orderlist.php","orderListWindow",\'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250\');
        return false;
      }
      if(e.ctrlKey && e.shiftKey && code == 119)
      {
        window.open("brokerTradesMcx.php","orderListWindow",\'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250\');
        return false;
      }
      else if(e.ctrlKey && code == 49)
      {
        window.open("clientTradesMcx.php","orderListWindow",\'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250\');
        return false;
      }
      '; ?>

      else if(e.ctrlKey && code == 119)
        window.open("clientTradesMcx.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      <?php else: ?>
      if(e.ctrlKey && e.shiftKey && code == 119)
        window.open("brokerTradesPer2side2fo.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      else if(e.ctrlKey && code == 119)
        window.open("clientTradesPer2side2fo.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      <?php endif; ?>
      <?php echo '
      if(e.ctrlKey && e.shiftKey && code == 13)
      {
        document.form1.tradeHidden.value = "sl";
        $("#trade").click();
        return false;
      }
      if(e.shiftKey && code == 13)
      {
        document.form1.tradeHidden.value = "rl";
        $("#trade").click();
        return false;
      }
      '; ?>

      <?php if ($this->_tpl_vars['cfgStoreOnEnter'] == 1): ?>
      <?php echo '
      if(code == 13)
      {
        document.form1.tradeHidden.value = "trade";
        $("#trade").click();
        return false;
      }
      '; ?>

      <?php else: ?>
      <?php echo '
      if(e.ctrlKey && code == 13)
      {
        document.form1.tradeHidden.value = "trade";
        $("#trade").click();
        return false;
      }
      if(code == 13)
      {
        window.event.keyCode = 9;
      }
      '; ?>

      <?php endif; ?>
      <?php echo '
      if(code == 109 || code == 123)
      {
        $("#body").css("background-color","red");
        $("#buySell").val("Sell");
        $("#buySellHidden").val("Sell");
        return false;
      }
      if(code == 107 || code == 120)
      {
        $("#body").css("background-color","blue");
        $("#buySell").val("Buy");
        $("#buySellHidden").val("Buy");
        return false;
      }
    });

  });
  function changePrice(theObject,event)
  {
    var changePriceCode = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
    if(changePriceCode == 38)
      theObject.value = parseInt(theObject.value)+1;
    if(changePriceCode == 40)
      theObject.value = parseInt(theObject.value)-1;
    if(theObject.name == "lot")
      plusOrMinusValue = 5;
    else
      plusOrMinusValue = 10;
    if(changePriceCode == 33)
      theObject.value = parseInt(theObject.value)+plusOrMinusValue;
    if(changePriceCode == 34)
      theObject.value = parseInt(theObject.value)-plusOrMinusValue;
  }
  function minQuantityFunc(lot)
  {
    document.form1.quantity.value = lot*document.form1.min.value;
    document.form1.quantityHidden.value = lot*document.form1.min.value;
  }
  function itemChange(theObject)
  {
    var form   = theObject.form;
    itemId     = document.form1.itemId;
    expiryDate = document.form1.expiryDate;
    expiryDate.options.length = 0;
    '; ?>

    <?php if ($this->_tpl_vars['exchange'] == 'MCX' || $this->_tpl_vars['exchange'] == 'CX'): ?>
      <?php unset($this->_sections['itemSec']);
$this->_sections['itemSec']['name'] = 'itemSec';
$this->_sections['itemSec']['loop'] = is_array($_loop=$this->_tpl_vars['item']['itemId']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['itemSec']['show'] = true;
$this->_sections['itemSec']['max'] = $this->_sections['itemSec']['loop'];
$this->_sections['itemSec']['step'] = 1;
$this->_sections['itemSec']['start'] = $this->_sections['itemSec']['step'] > 0 ? 0 : $this->_sections['itemSec']['loop']-1;
if ($this->_sections['itemSec']['show']) {
    $this->_sections['itemSec']['total'] = $this->_sections['itemSec']['loop'];
    if ($this->_sections['itemSec']['total'] == 0)
        $this->_sections['itemSec']['show'] = false;
} else
    $this->_sections['itemSec']['total'] = 0;
if ($this->_sections['itemSec']['show']):

            for ($this->_sections['itemSec']['index'] = $this->_sections['itemSec']['start'], $this->_sections['itemSec']['iteration'] = 1;
                 $this->_sections['itemSec']['iteration'] <= $this->_sections['itemSec']['total'];
                 $this->_sections['itemSec']['index'] += $this->_sections['itemSec']['step'], $this->_sections['itemSec']['iteration']++):
$this->_sections['itemSec']['rownum'] = $this->_sections['itemSec']['iteration'];
$this->_sections['itemSec']['index_prev'] = $this->_sections['itemSec']['index'] - $this->_sections['itemSec']['step'];
$this->_sections['itemSec']['index_next'] = $this->_sections['itemSec']['index'] + $this->_sections['itemSec']['step'];
$this->_sections['itemSec']['first']      = ($this->_sections['itemSec']['iteration'] == 1);
$this->_sections['itemSec']['last']       = ($this->_sections['itemSec']['iteration'] == $this->_sections['itemSec']['total']);
?>
      if(itemId.selectedIndex == <?php echo $this->_sections['itemSec']['index']; ?>
)
      <?php echo '
      {
        '; ?>

        <?php unset($this->_sections['expiryDateSec']);
$this->_sections['expiryDateSec']['name'] = 'expiryDateSec';
$this->_sections['expiryDateSec']['loop'] = is_array($_loop=$this->_tpl_vars['expiryDate'][$this->_sections['itemSec']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['expiryDateSec']['show'] = true;
$this->_sections['expiryDateSec']['max'] = $this->_sections['expiryDateSec']['loop'];
$this->_sections['expiryDateSec']['step'] = 1;
$this->_sections['expiryDateSec']['start'] = $this->_sections['expiryDateSec']['step'] > 0 ? 0 : $this->_sections['expiryDateSec']['loop']-1;
if ($this->_sections['expiryDateSec']['show']) {
    $this->_sections['expiryDateSec']['total'] = $this->_sections['expiryDateSec']['loop'];
    if ($this->_sections['expiryDateSec']['total'] == 0)
        $this->_sections['expiryDateSec']['show'] = false;
} else
    $this->_sections['expiryDateSec']['total'] = 0;
if ($this->_sections['expiryDateSec']['show']):

            for ($this->_sections['expiryDateSec']['index'] = $this->_sections['expiryDateSec']['start'], $this->_sections['expiryDateSec']['iteration'] = 1;
                 $this->_sections['expiryDateSec']['iteration'] <= $this->_sections['expiryDateSec']['total'];
                 $this->_sections['expiryDateSec']['index'] += $this->_sections['expiryDateSec']['step'], $this->_sections['expiryDateSec']['iteration']++):
$this->_sections['expiryDateSec']['rownum'] = $this->_sections['expiryDateSec']['iteration'];
$this->_sections['expiryDateSec']['index_prev'] = $this->_sections['expiryDateSec']['index'] - $this->_sections['expiryDateSec']['step'];
$this->_sections['expiryDateSec']['index_next'] = $this->_sections['expiryDateSec']['index'] + $this->_sections['expiryDateSec']['step'];
$this->_sections['expiryDateSec']['first']      = ($this->_sections['expiryDateSec']['iteration'] == 1);
$this->_sections['expiryDateSec']['last']       = ($this->_sections['expiryDateSec']['iteration'] == $this->_sections['expiryDateSec']['total']);
?>
        expiryDate.options[<?php echo $this->_sections['expiryDateSec']['index']; ?>
] = new Option("<?php echo $this->_tpl_vars['expiryDate'][$this->_sections['itemSec']['index']][$this->_sections['expiryDateSec']['index']]; ?>
","<?php echo $this->_tpl_vars['expiryDate'][$this->_sections['itemSec']['index']][$this->_sections['expiryDateSec']['index']]; ?>
");
        expiryDate.options[<?php echo $this->_sections['expiryDateSec']['index']; ?>
].selected = true;
        <?php endfor; endif; ?>
        <?php echo '
      }
      '; ?>

      <?php endfor; endif; ?>
    <?php else: ?>
      <?php unset($this->_sections['expiryDateSec']);
$this->_sections['expiryDateSec']['name'] = 'expiryDateSec';
$this->_sections['expiryDateSec']['loop'] = is_array($_loop=$this->_tpl_vars['expiryDate']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['expiryDateSec']['show'] = true;
$this->_sections['expiryDateSec']['max'] = $this->_sections['expiryDateSec']['loop'];
$this->_sections['expiryDateSec']['step'] = 1;
$this->_sections['expiryDateSec']['start'] = $this->_sections['expiryDateSec']['step'] > 0 ? 0 : $this->_sections['expiryDateSec']['loop']-1;
if ($this->_sections['expiryDateSec']['show']) {
    $this->_sections['expiryDateSec']['total'] = $this->_sections['expiryDateSec']['loop'];
    if ($this->_sections['expiryDateSec']['total'] == 0)
        $this->_sections['expiryDateSec']['show'] = false;
} else
    $this->_sections['expiryDateSec']['total'] = 0;
if ($this->_sections['expiryDateSec']['show']):

            for ($this->_sections['expiryDateSec']['index'] = $this->_sections['expiryDateSec']['start'], $this->_sections['expiryDateSec']['iteration'] = 1;
                 $this->_sections['expiryDateSec']['iteration'] <= $this->_sections['expiryDateSec']['total'];
                 $this->_sections['expiryDateSec']['index'] += $this->_sections['expiryDateSec']['step'], $this->_sections['expiryDateSec']['iteration']++):
$this->_sections['expiryDateSec']['rownum'] = $this->_sections['expiryDateSec']['iteration'];
$this->_sections['expiryDateSec']['index_prev'] = $this->_sections['expiryDateSec']['index'] - $this->_sections['expiryDateSec']['step'];
$this->_sections['expiryDateSec']['index_next'] = $this->_sections['expiryDateSec']['index'] + $this->_sections['expiryDateSec']['step'];
$this->_sections['expiryDateSec']['first']      = ($this->_sections['expiryDateSec']['iteration'] == 1);
$this->_sections['expiryDateSec']['last']       = ($this->_sections['expiryDateSec']['iteration'] == $this->_sections['expiryDateSec']['total']);
?>
      expiryDate.options[<?php echo $this->_sections['expiryDateSec']['index']; ?>
] = new Option("<?php echo $this->_tpl_vars['expiryDate'][$this->_sections['expiryDateSec']['index']]; ?>
","<?php echo $this->_tpl_vars['expiryDate'][$this->_sections['expiryDateSec']['index']]; ?>
");
      expiryDate.options[<?php echo $this->_sections['expiryDateSec']['index']; ?>
].selected = true;
      <?php endfor; endif; ?>
    <?php endif; ?>

    <?php unset($this->_sections['sec']);
$this->_sections['sec']['name'] = 'sec';
$this->_sections['sec']['loop'] = is_array($_loop=$this->_tpl_vars['item']['itemId']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec']['show'] = true;
$this->_sections['sec']['max'] = $this->_sections['sec']['loop'];
$this->_sections['sec']['step'] = 1;
$this->_sections['sec']['start'] = $this->_sections['sec']['step'] > 0 ? 0 : $this->_sections['sec']['loop']-1;
if ($this->_sections['sec']['show']) {
    $this->_sections['sec']['total'] = $this->_sections['sec']['loop'];
    if ($this->_sections['sec']['total'] == 0)
        $this->_sections['sec']['show'] = false;
} else
    $this->_sections['sec']['total'] = 0;
if ($this->_sections['sec']['show']):

            for ($this->_sections['sec']['index'] = $this->_sections['sec']['start'], $this->_sections['sec']['iteration'] = 1;
                 $this->_sections['sec']['iteration'] <= $this->_sections['sec']['total'];
                 $this->_sections['sec']['index'] += $this->_sections['sec']['step'], $this->_sections['sec']['iteration']++):
$this->_sections['sec']['rownum'] = $this->_sections['sec']['iteration'];
$this->_sections['sec']['index_prev'] = $this->_sections['sec']['index'] - $this->_sections['sec']['step'];
$this->_sections['sec']['index_next'] = $this->_sections['sec']['index'] + $this->_sections['sec']['step'];
$this->_sections['sec']['first']      = ($this->_sections['sec']['iteration'] == 1);
$this->_sections['sec']['last']       = ($this->_sections['sec']['iteration'] == $this->_sections['sec']['total']);
?>
    if((document.form1.itemId.value == "<?php echo $this->_tpl_vars['item']['itemId'][$this->_sections['sec']['index']]; ?>
"))<?php echo '
    {
      '; ?>

      document.form1.min.value = <?php echo $this->_tpl_vars['item']['min'][$this->_sections['sec']['index']]; ?>
;
      minQuantityFunc(document.form1.lot.value);
      <?php echo '
    }
    '; ?>

    <?php endfor; endif; ?>
    <?php echo '
  }
  function selectOptionByValue(selObj, val)
  {
    var A = selObj.options, L = A.length;
    while(L)
    {
      if (A[--L].value == val)
      {
        selObj.selectedIndex = L;
        L = 0;
        break;
      }
    }
    itemChange(document.form1.itemId);
  }
  '; ?>

  $('#clientId').focus();
  itemChange(document.form1.itemId);

</script>
</body>
</html>