<?php
  session_start();
?>
<HTML>
<HEAD><TITLE>Item settings</TITLE></HEAD>
<BODY bgColor="#FFCEE7">
<CENTER>
<FORM name="form1" action="<?php echo $_SERVER['PHP_SELF']; ?>" METHOD="post">
<A href="./index.php">Home</A>&nbsp;&nbsp;&nbsp;<A href="./itemAdd.php">Add Item</A>
<TABLE border="1" cellspacing="0" cellpadding="6">
<TR>
  <TD align="center" align="center">
    <?php
      if(isset($_SESSION['ses_decidePackFor']) && $_SESSION['ses_decidePackFor'] == 7 )
        $selectedExchange = "F_O";
      else if(isset($_SESSION['ses_decidePackFor']) && $_SESSION['ses_decidePackFor'] == 6 )
        $selectedExchange = "MCX";
      else
        $selectedExchange = "All";
      if(isset($_POST['exchangeName']))
        $selectedExchange = $_POST['exchangeName'];
      //CREATE THE EXCHANGE COMBO : start
      include "etc/om_config.inc"; 
      $selectExchang = "SELECT * FROM exchange";
      $selectExchangResult = mysql_query($selectExchang);
      $exchangeId = "";
      $exchange = "";
      echo "<SELECT name='exchangeName' onChange='submit();'>";
      echo "<option>All</option>";
      while($rowExchange = mysql_fetch_array($selectExchangResult))
      {
        if($rowExchange['exchange'] == $selectedExchange)           //THIS IS FOR SELECTED VALUE WHICH WE SELECT
          echo "<option selected>".$rowExchange['exchange']."</option>"; 
        else
          echo "<option>".$rowExchange['exchange']."</option>";
      }
      echo "</SELECT>";
      //CREATE THE EXCHANGE COMBO : end
    ?>
  </TD>
  <TD colspan="8" align="center" ><B>Item settings : </B></TD>
</TR>
<TR>
  <TD align="center"><FONT color="red"><STRONG>Exchange</STRONG></FONT></TD>
  <TD align="center"><FONT color="red"><STRONG>Item</STRONG></FONT></TD>
  <TD align="center"><FONT color="red"><STRONG>OneSideBrok</STRONG></FONT></TD>
  <TD align="center"><FONT color="red"><STRONG>Minimum</STRONG></FONT></TD>
  <TD align="center"><FONT color="red"><STRONG>Price Range</STRONG></FONT></TD>
  <TD align="center"><FONT color="red"><STRONG>Multiply</STRONG></FONT></TD>
  <TD align="center"><FONT color="red"><STRONG>&nbsp;</STRONG></FONT></TD>
  <TD align="center"><FONT color="red"><STRONG>&nbsp;</STRONG></FONT></TD>
</TR>
<?php
// DISPLAY THE DATA IN TABLE : start                                
$query = "SELECT * FROM item ";                                     
if(isset($selectedExchange) && $selectedExchange != "All")
  $query .= "where exchange ='".$selectedExchange."' ";        

$query .= "ORDER BY exchange,item";
$result = mysql_query($query);
while($row = mysql_fetch_array($result))
{
  echo "
    <TR>
      <TD align='right'>&nbsp;".$row['exchange']."</TD>
      <TD align='right'>".$row['item']."</TD>
      <TD align='right'>".$row['oneSideBrok']."</TD>
      <TD align='right'>".$row['min']."</TD>
      <TD align='right' NOWRAP>".$row['rangeStart']." :: ".$row['rangeEnd']."</TD>
      <TD align='right' NOWRAP>".$row['multiply']."</TD>
      <TD align='right'><a href='editBrokerage.php?item=".$row['item']."'>Edit</a></TD>
      <TD align='right'><a href='deleteBrokerage.php?item=".$row['item']."' ONCLICK='return confirm(\"Are You Sure?\");'>Delete</a></TD>
    </TR>" ;  
}
// DISPLAY THE DATA IN TABLE : end
?>
</TABLE>
</FORM>
</CENTER>
</BODY>
</HTML>