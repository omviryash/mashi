<?php
session_start();
if(!isset($_SESSION['user']))
  header("Location: login.php");
else
{
  include "./etc/om_config.inc";
  $origPackFor = $packfor;

	$packForAllSelected = "";
	$packForF_OSelected = "";
	$packForMCXSelected = "";
  if(isset($_SESSION['ses_decidePackFor']))
  {
  	$packForAllSelected = $_SESSION['ses_decidePackFor']=="5"?"SELECTED":"";
  	$packForF_OSelected = $_SESSION['ses_decidePackFor']=="7"?"SELECTED":"";
  	$packForMCXSelected = $_SESSION['ses_decidePackFor']=="6"?"SELECTED":"";
  	$packfor = $_SESSION['ses_decidePackFor'];
  }
  if(isset($_POST['decidePackFor']))
  {
  	$packForAllSelected = $_POST['decidePackFor']=="5"?"SELECTED":"";
  	$packForF_OSelected = $_POST['decidePackFor']=="7"?"SELECTED":"";
  	$packForMCXSelected = $_POST['decidePackFor']=="6"?"SELECTED":"";
  	$packfor = $_POST['decidePackFor'];
  }
  $_SESSION['ses_decidePackFor'] = $packfor;
  
  $useShortCuts = 1;
  if($_SESSION['ses_decidePackFor'] == '6')
  {
    $openPageForA = "addTrade.php?exchange=MCX";
    $openPageForO = "orderList.php";
  }
  else if($_SESSION['ses_decidePackFor'] == '7')
  {
    $openPageForA = "addTrade.php?exchange=F_O";
    $openPageForO = "orderList.php";
  }
  else
    $useShortCuts = 0;

  include "./templates/headerMainIndex.tpl";
  include "./header.php";
  include "./templates/footer1.tpl";
}
?>