<?php
include "etc/om_config.inc";
session_start();
if(!isset($_SESSION['user']))
  header("Location: login.php");
else
{
  $smarty = new SmartyWWW();
  $client          = array();
  $parentClient    = array();
  $exchange        = array();
  $selExchange     = 'All';
  $selectClient    = 0;
  $selParentClient = 0;
  $selCommissionIn = 0;
  $isUpdate        = 0;
  $selCommission   = '';
  
  if(isset($_POST['saveBtn']))
  {
    if(isset($_POST['parentclient']) && $_POST['parentclient'] != '' && $_POST['commission'] > 0)
    {
      if($_POST['isUpdate'] == '1')
      {
        $updateQuery = "UPDATE clientexchange SET parentClientId = ".$_POST['parentclient'].",
                                                  commissionIn = '".$_POST['commissionIn']."',
                                                  commission = ".$_POST['commission']."
                         WHERE clientId = ".$_POST['client']."
                           AND exchange = '".$_POST['exchange']."'";
        $updateQueryRes = mysql_query($updateQuery);
      }
      else
      {
        $insertQuery = "INSERT INTO clientexchange (clientId,exchange,parentClientId,commissionIn,commission)
                                            VALUES (".$_POST['client'].",'".$_POST['exchange']."',".$_POST['parentclient'].",
                                                    '".$_POST['commissionIn']."',".$_POST['commission'].")";
        $insertQueryRes = mysql_query($insertQuery);
      }
    }
  }
  
  $selectQuery = "SELECT clientId,firstName,middleName,lastName FROM client
                   ORDER BY firstName,middleName,lastName";
  $selectQueryRes = mysql_query($selectQuery);
  $a = 0;
  while($clientRow = mysql_fetch_array($selectQueryRes))
  {
    $client['id'][$a]   = $clientRow['clientId'];
    $client['name'][$a] = $clientRow['firstName']." ".$clientRow['middleName']." ".$clientRow['lastName'];
    $a++;
  }
  
  $selectExchange = "SELECT exchangeId,exchange FROM exchange
                      ORDER BY exchange";
  $selectExchangeRes = mysql_query($selectExchange);
  $b = 0;
  while($exchangeRow = mysql_fetch_assoc($selectExchangeRes))
  {
    $exchange['id'][$b]   = $exchangeRow['exchangeId'];
    $exchange['name'][$b] = $exchangeRow['exchange'];
    $b++;
  }
  
  if(isset($_POST['client']))
  {
    $selectClient = $_POST['client'];
    $selExchange  = $_POST['exchange'];
    $selectParentClient = "SELECT exchange,parentClientId,commissionIn,commission FROM clientexchange
                            WHERE clientId = ".$_POST['client']."
                              AND exchange = '".$_POST['exchange']."'";
    $selectParentClientRes = mysql_query($selectParentClient);
    $a = 0;
    while($parentRow = mysql_fetch_array($selectParentClientRes))
    {
      $isUpdate        = 1;
      $selParentClient = $parentRow['parentClientId'];
      $selCommissionIn = $parentRow['commissionIn'];
      $selCommission   = $parentRow['commission'];
    }
    
    $selectParentClient = "SELECT clientId,firstName,middleName,lastName FROM client
                                  WHERE clientId != ".$_POST['client'];
    $selectParentClientRes = mysql_query($selectParentClient);
    $a = 0;
    while($parentClientRow = mysql_fetch_array($selectParentClientRes))
    {
      $parentClient['id'][$a]   = $parentClientRow['clientId'];
      $parentClient['name'][$a] = $parentClientRow['firstName']." ".$parentClientRow['middleName']." ".$parentClientRow['lastName'];
      $a++;
    }
  }
  
  $smarty->assign("selectClient",$selectClient);
  $smarty->assign("client",$client);
  $smarty->assign("parentClient",$parentClient);
  $smarty->assign("selParentClient",$selParentClient);
  $smarty->assign("selCommissionIn",$selCommissionIn);
  $smarty->assign("selCommission",$selCommission);
  $smarty->assign("exchange",$exchange);
  $smarty->assign("selExchange",$selExchange);
  $smarty->assign("isUpdate",$isUpdate);
  $smarty->display("clientCommision.tpl");
}
?>