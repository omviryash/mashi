ALTER TABLE `client` ADD `perentClientId` INT( 6 ) UNSIGNED NULL DEFAULT '0',
ADD `commissionIn` CHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
ADD `commission` DOUBLE NULL DEFAULT '0.0';
 CREATE TABLE `brokrecieved` (
`brokRecievedId` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`brokDate` DATE NULL DEFAULT '0000-00-00',
`clientId` INT( 6 ) NULL DEFAULT '0',
`brok` DOUBLE NULL
);
ALTER TABLE `brokrecieved` ADD `lot` DOUBLE NULL ;
-- 6/30/2010 6:55:23 PM
CREATE TABLE `otherincome` (
  `otherIncomId` int(11) unsigned NOT NULL auto_increment,
  `otherIncomName` varchar(50) character set utf8 default NULL,
  `otherIncomDate` date default NULL,
  `otherIncomAmount` double default NULL,
  `note` varchar(60) character set utf8 default NULL,
  `otherIncomMode` varchar(60) character set utf8 default NULL,
  PRIMARY KEY  (`otherIncomId`)
) ;

CREATE TABLE `incomemaster` (
  `otherIncomeId` tinyint(3) unsigned NOT NULL auto_increment,
  `otherIncomName` varchar(60) character set utf8 default NULL,
  PRIMARY KEY  (`otherIncomeId`)
) ;

-- 8/9/2010 1:03:27 PM
ALTER TABLE `client` DROP `perentClientId` ,
DROP `commissionIn` ,
DROP `commission` ;

ALTER TABLE `clientexchange` ADD `parentClientId` INT( 6 ) UNSIGNED NULL DEFAULT '0',
ADD `commissionIn` CHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
ADD `commission` DOUBLE NULL DEFAULT '0';

ALTER TABLE `brokrecieved` ADD `exchange` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `clientId` ;
