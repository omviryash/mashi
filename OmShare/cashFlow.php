<?php
session_start();
  include "./etc/om_config.inc";
  include "./dateRange.php";
  $smarty=new smartyWWW();
  $opening       = 0;
  $totalAmount   = 0;
  $cashFlowCount = 0;
  $otherExpCount = 0;
  $cashFlow      = array();
  $otherExp      = array();
  $transModeOptSelected = "";
  
  $bankQuery = "SELECT DISTINCT transMode FROM cashflow
                 ORDER BY transMode";
  $bankResult = mysql_query($bankQuery);
  $b = 0;
  while($bankRow = mysql_fetch_array($bankResult))
  {
    $bank['name'][$b] = $bankRow['transMode'];
    $b++;
  }
    
  $selectQuery = "SELECT * FROM cashflow
                   WHERE 1=1";
  if(isset($_REQUEST['transModeOpt']) && $_REQUEST['transModeOpt'] != "0" )
  {
    $selectQuery .= " AND transMode = '".$_REQUEST['transModeOpt']."'"; 
    $transModeOptSelected = $_REQUEST['transModeOpt'];
  }
  if(isset($_SESSION['fromDate']))
     $selectQuery .= " AND transactionDate >= '".$_SESSION['fromDate']."' AND transactionDate <= '".$_SESSION['toDate']."'" ;
  $selectQuery .= " ORDER BY transactionDate";
  $selectQueryResult = mysql_query($selectQuery);
  $k = 0;
  while($rowFound = mysql_fetch_array($selectQueryResult))
  {
    $cashFlow[$k]['mode']      = $rowFound['transMode'];
    $cashFlow[$k]['id']        = $rowFound['cashFlowId'];
    $cashFlow[$k]['clientId']  = $rowFound['clientId'];
    $cashFlow[$k]['date']      = $rowFound['transactionDate'];
    $cashFlow[$k]['transtype'] = $rowFound['transType'];
    $cashFlow[$k]['notes']     = $rowFound['itemIdExpiryDate'];
    $cashFlow[$k]['dwstatus']  = $rowFound['dwStatus'];
    $cashFlow[$k]['amount']    = $rowFound['dwAmount'];
    $cashFlow[$k]['firstName']  = "";
    $cashFlow[$k]['middleName'] = "";
    $cashFlow[$k]['lastName']   = "";
    
    $selectClientQuery = "SELECT firstName, middleName, lastName FROM client
                           WHERE clientId = ".$rowFound['clientId'];
    $selectClientResult = mysql_query($selectClientQuery);
    if($selectClientRow = mysql_fetch_array($selectClientResult))
    {
      $cashFlow[$k]['firstName']  = $selectClientRow['firstName'];
      $cashFlow[$k]['middleName'] = $selectClientRow['middleName'];
      $cashFlow[$k]['lastName']   = $selectClientRow['lastName'];
    }
    if($rowFound['dwStatus'] == "d")
    {
      $totalAmount  += $rowFound['dwAmount'];
    }
    elseif($rowFound['dwStatus'] == "w")
    {
      $totalAmount  -= $rowFound['dwAmount'];
    }
    $cashFlow[$k]['totalAmount']  = $totalAmount;
    $k++;
  }
  $cashFlowCount = count($cashFlow);
  
  // This For Get The Data From Other Exp : Start
  $selectExp = "SELECT * FROM otherexp 
                 WHERE 1=1";
  if($transModeOptSelected != 0)
    $selectExp .=" AND otherExpMode = '".$transModeOptSelected."'";
  if(isset($_SESSION['fromDate']))
     $selectExp .= " AND otherExpDate >= '".$_SESSION['fromDate']."' AND otherExpDate <= '".$_SESSION['toDate']."'" ;
  $selectExp .= " ORDER BY otherExpDate";
  $selectExpResult = mysql_query($selectExp);
  $i = 0;
  while($expRowFound = mysql_fetch_array($selectExpResult))
  {
    $otherExp[$i]['id']          = $expRowFound['otherexpId'];
    $otherExp[$i]['amount']      = $expRowFound['otherExpAmount'];
    $otherExp[$i]['date']        = $expRowFound['otherExpDate'];
    $otherExp[$i]['name']        = $expRowFound['otherExpName'];
    $otherExp[$i]['note']        = $expRowFound['note'];
    $totalAmount                -= $expRowFound['otherExpAmount'];
    $otherExp[$i]['totalAmount'] = $totalAmount;
    $i++;
  }
  $otherExpCount = count($otherExp);
  // This For Get The Data From Other Exp : End
  
  $smarty->assign("bank",$bank);
  $smarty->assign("transModeOptSelected",$transModeOptSelected);
  $smarty->assign("cashFlow",$cashFlow);
  $smarty->assign("cashFlowCount",$cashFlowCount);
  $smarty->assign("otherExp",$otherExp);
  $smarty->assign("otherExpCount",$otherExpCount);
  $smarty->assign("fromDate", substr($_SESSION['fromDate'],0,4)."-".substr($_SESSION['fromDate'],5,2)."-".substr($_SESSION['fromDate'],8,2));
  $smarty->assign("toDate",   substr($_SESSION['toDate'],0,4)."-".substr($_SESSION['toDate'],5,2)."-".substr($_SESSION['toDate'],8,2));

  $smarty->display("cashFlow.tpl");
?>