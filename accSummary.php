<?php
session_start();
if(!isset($_SESSION['toDate'])) 
{
  header("Location: selectDtSession.php?goTo=accTransList");
}
$selectClient = "SELECT * FROM CLIENT ORDER BY firstName,middleName,lastName";
$resultClient = mysql_query($selectClient);
while($rowClient = mysql_fetch_array($resultClient))
{
  $clientInfoQuery="SELECT * FROM cashflow WHERE 1=1";
  $clientInfoQuery.=" AND clientId='".$rowClient['clientId']."'";
  $clientInfoQuery.=" AND transactionDate <= '".$_SESSION['toDate']."'";
  $clientInfoQuery.=" ORDER BY transactionDate";
  $k=0;
  $cashFlowId = array();
  $reportDate=array();
  $transType = array();
  $itemIdExpiryDate=array();
  $reportdwStatus=array();

  $reportdwAmount1=array();
  $reportdwAmount2=array();

  $reportplStatus=array();
  
  $reportplAmount1=array();
  $reportplAmount2=array();
  
  $totMargin = array();
  $totOther  = array();
  $totProfit = array();
  $totLoss   = array();
  $totWithoutMargin = array();
  
  $reportcurrentBal=array();
  $currentBal = $opening;
  
  $cashFlowId[0]       = 0;
  $reportDate[0]       = substr($_SESSION['fromDate'],8,2)."-".substr($_SESSION['fromDate'],5,2)."-".substr($_SESSION['fromDate'],2,2);
  $transType[0]        = '&nbsp;';
  $itemIdExpiryDate[0] = 'Opening';
  $reportdwStatus[0]   = 0;
  $reportdwAmount1[0]  = $opening;
  $reportdwAmount2[0]  = 0;
  $reportplStatus[0]   = 0;
  $reportplAmount1[0]  = 0;
  $reportplAmount2[0]  = 0;
  
  $storeTotMargin[$clientCount] = 0;
  $storeTotOther       = 0;
  $storeTotProfit      = 0;
  $storeTotLoss        = 0;
  $totMargin[0]        = $storeTotMargin;
  $totOther[0]         = $storeTotOther;
  $totProfit[0]        = $storeTotProfit;
  $totLoss[0]          = $storeTotLoss;
  $totWithoutMargin[0] = $storeTotOther+$storeTotProfit+$storeTotLoss;
  
  $reportcurrentBal[$k]=$currentBal;
  $transMode[0] = "";
  
  $clientInfoResult=mysql_query($clientInfoQuery,$link);
  while ($resInfo=mysql_fetch_array($clientInfoResult))
  {
    if($resInfo['dwStatus'] == 'd')
      $currentBal += $resInfo['dwAmount'];
    if($resInfo['dwStatus'] == 'w')
      $currentBal -= $resInfo['dwAmount'];
    if($resInfo['plStatus'] == 'p')
      $currentBal += $resInfo['plAmount'];
    if($resInfo['plStatus'] == 'l')
      $currentBal += $resInfo['plAmount'];
    
    if($resInfo['transactionDate'] < $_SESSION['fromDate'])
    {
      if($resInfo['dwStatus'] != "d")
      {
        $reportdwAmount1[$k] += 0;
        $reportdwAmount2[$k] += $resInfo['dwAmount'];
      }
      else
      {
        $reportdwAmount1[$k] += $resInfo['dwAmount'];
        $reportdwAmount2[$k] += 0;
      }
      
      if($resInfo['plStatus'] != "p")
      {
        $reportplAmount1[$k] += 0;
        $reportplAmount2[$k] += $resInfo['plAmount'];
      }
      else
      {
        $reportplAmount1[$k] += $resInfo['plAmount'];
        $reportplAmount2[$k] += 0;
      }
      
      $reportcurrentBal[$k]=$currentBal;

      if($resInfo['transType'] == 'Margin')
      {
        if($resInfo['dwStatus'] == 'd' || $resInfo['plStatus'] == 'p')
          $storeTotMargin += $resInfo['dwAmount'];
        elseif($resInfo['dwStatus'] == 'w' || $resInfo['plStatus'] == 'l')
          $storeTotMargin -= $resInfo['dwAmount'];
      }
      else
        $storeTotOther      += $resInfo['dwAmount'];
      $totMargin[$k] = $storeTotMargin;
    }
    else
    {
      $k++;
      
      $cashFlowId[$k]=$resInfo['cashFlowId'];
      $reportDate[$k]=substr($resInfo['transactionDate'],8,2)."-".substr($resInfo['transactionDate'],5,2)."-".substr($resInfo['transactionDate'],2,2);
      $transType[$k] = $resInfo['transType'];
      $itemIdExpiryDate[$k]=$resInfo['itemIdExpiryDate'];
      $reportdwStatus[$k]=$resInfo['dwStatus'];
	    $transMode[$k] = $resInfo['transMode'];
      
      if($resInfo['dwStatus'] != "d")
      {
        $reportdwAmount1[$k] = 0;
        $reportdwAmount2[$k] = $resInfo['dwAmount'];
      }
      else
      {
        $reportdwAmount1[$k] = $resInfo['dwAmount'];
        $reportdwAmount2[$k] = 0;
      }
      
      $reportplStatus[$k]=$resInfo['plStatus'];
      
      if($resInfo['plStatus'] != "p")
      {
        $reportplAmount1[$k]=0;
        $reportplAmount2[$k]=$resInfo['plAmount'];
        $storeTotLoss     += $resInfo['plAmount'];
      }
      else
      {
        $reportplAmount1[$k]=$resInfo['plAmount'];
        $reportplAmount2[$k]=0;
        $storeTotProfit      += $resInfo['plAmount'];
      }
      
      $reportcurrentBal[$k]=$currentBal;
      
      if($resInfo['transType'] == 'Margin')
      {
        if($resInfo['dwStatus'] == 'd' || $resInfo['plStatus'] == 'p')
          $storeTotMargin += $resInfo['dwAmount'];
        elseif($resInfo['dwStatus'] == 'w' || $resInfo['plStatus'] == 'l')
          $storeTotMargin -= $resInfo['dwAmount'];
      }
      else
        $storeTotOther      += $resInfo['dwAmount'];
    }
    $totMargin[$k] = $storeTotMargin;
    $totOther[$k]  = $storeTotOther;
    $totProfit[$k] = $storeTotProfit;
    $totLoss[$k]   = $storeTotLoss;
    $totWithoutMargin[$k] = $storeTotOther+$storeTotProfit+$storeTotLoss;
  }
}
  $smarty->assign()  
  $smarty->display("accTransList.tpl");
}
?>