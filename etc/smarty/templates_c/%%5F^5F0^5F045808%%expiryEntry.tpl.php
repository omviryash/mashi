<?php /* Smarty version 2.6.10, created on 2016-01-18 10:51:21
         compiled from expiryEntry.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'expiryEntry.tpl', 52, false),array('function', 'html_select_date', 'expiryEntry.tpl', 63, false),)), $this); ?>
<html>
<HEAD>
<TITLE>Expiry Date Entry Form</TITLE>

<SCRIPT language="javascript">
  /* for exchange change combo :Start */
  <?php echo '
  function companyChange(theObject)
  {
    var form = theObject.form;
    for(i=0;i<form.elements.length;i++)
    {
      if(form.elements[i]==theObject)
      {
        exchange = document.form1.exchange;
        itemId = document.form1.itemId;
      }
    }
    itemId.options.length = 0;
  '; ?>

  <?php unset($this->_sections['sec1']);
$this->_sections['sec1']['name'] = 'sec1';
$this->_sections['sec1']['loop'] = is_array($_loop=$this->_tpl_vars['exchange']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec1']['show'] = true;
$this->_sections['sec1']['max'] = $this->_sections['sec1']['loop'];
$this->_sections['sec1']['step'] = 1;
$this->_sections['sec1']['start'] = $this->_sections['sec1']['step'] > 0 ? 0 : $this->_sections['sec1']['loop']-1;
if ($this->_sections['sec1']['show']) {
    $this->_sections['sec1']['total'] = $this->_sections['sec1']['loop'];
    if ($this->_sections['sec1']['total'] == 0)
        $this->_sections['sec1']['show'] = false;
} else
    $this->_sections['sec1']['total'] = 0;
if ($this->_sections['sec1']['show']):

            for ($this->_sections['sec1']['index'] = $this->_sections['sec1']['start'], $this->_sections['sec1']['iteration'] = 1;
                 $this->_sections['sec1']['iteration'] <= $this->_sections['sec1']['total'];
                 $this->_sections['sec1']['index'] += $this->_sections['sec1']['step'], $this->_sections['sec1']['iteration']++):
$this->_sections['sec1']['rownum'] = $this->_sections['sec1']['iteration'];
$this->_sections['sec1']['index_prev'] = $this->_sections['sec1']['index'] - $this->_sections['sec1']['step'];
$this->_sections['sec1']['index_next'] = $this->_sections['sec1']['index'] + $this->_sections['sec1']['step'];
$this->_sections['sec1']['first']      = ($this->_sections['sec1']['iteration'] == 1);
$this->_sections['sec1']['last']       = ($this->_sections['sec1']['iteration'] == $this->_sections['sec1']['total']);
?>
    if(exchange.selectedIndex == <?php echo $this->_sections['sec1']['index']; ?>
 )
  <?php echo '
    {
  '; ?>

      <?php unset($this->_sections['sec2']);
$this->_sections['sec2']['name'] = 'sec2';
$this->_sections['sec2']['loop'] = is_array($_loop=$this->_tpl_vars['itemId'][$this->_sections['sec1']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec2']['show'] = true;
$this->_sections['sec2']['max'] = $this->_sections['sec2']['loop'];
$this->_sections['sec2']['step'] = 1;
$this->_sections['sec2']['start'] = $this->_sections['sec2']['step'] > 0 ? 0 : $this->_sections['sec2']['loop']-1;
if ($this->_sections['sec2']['show']) {
    $this->_sections['sec2']['total'] = $this->_sections['sec2']['loop'];
    if ($this->_sections['sec2']['total'] == 0)
        $this->_sections['sec2']['show'] = false;
} else
    $this->_sections['sec2']['total'] = 0;
if ($this->_sections['sec2']['show']):

            for ($this->_sections['sec2']['index'] = $this->_sections['sec2']['start'], $this->_sections['sec2']['iteration'] = 1;
                 $this->_sections['sec2']['iteration'] <= $this->_sections['sec2']['total'];
                 $this->_sections['sec2']['index'] += $this->_sections['sec2']['step'], $this->_sections['sec2']['iteration']++):
$this->_sections['sec2']['rownum'] = $this->_sections['sec2']['iteration'];
$this->_sections['sec2']['index_prev'] = $this->_sections['sec2']['index'] - $this->_sections['sec2']['step'];
$this->_sections['sec2']['index_next'] = $this->_sections['sec2']['index'] + $this->_sections['sec2']['step'];
$this->_sections['sec2']['first']      = ($this->_sections['sec2']['iteration'] == 1);
$this->_sections['sec2']['last']       = ($this->_sections['sec2']['iteration'] == $this->_sections['sec2']['total']);
?>
        itemId.options[<?php echo $this->_sections['sec2']['index']; ?>
]=new Option("<?php echo $this->_tpl_vars['itemId'][$this->_sections['sec1']['index']][$this->_sections['sec2']['index']]; ?>
","<?php echo $this->_tpl_vars['itemId'][$this->_sections['sec1']['index']][$this->_sections['sec2']['index']]; ?>
");
      <?php endfor; endif; ?>
  <?php echo '
    }
  '; ?>

  <?php endfor; endif; ?>
  <?php echo '
  }
  '; ?>

/* for exchange change combo :End */
  </SCRIPT>
</HEAD>
<HEAD><TITLE>Expiry Date Entry Form</TITLE></HEAD>
<BODY bgColor="#FFCEE7">
<FORM name=form1 action="<?php echo $this->_tpl_vars['PHP_SELF']; ?>
" method = POST>
<A href="index.php">Home</A>
<A href="expiryList.php">List</A>
<BR><BR>
<B>Set Expiry Date : </B>
<TABLE border="1">
 <TR>
 <TD>Exchange</TD>
 <TD>
   <select name="exchange" onChange="companyChange(this);">
			<?php unset($this->_sections['sec']);
$this->_sections['sec']['name'] = 'sec';
$this->_sections['sec']['loop'] = is_array($_loop=$this->_tpl_vars['exchange']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec']['show'] = true;
$this->_sections['sec']['max'] = $this->_sections['sec']['loop'];
$this->_sections['sec']['step'] = 1;
$this->_sections['sec']['start'] = $this->_sections['sec']['step'] > 0 ? 0 : $this->_sections['sec']['loop']-1;
if ($this->_sections['sec']['show']) {
    $this->_sections['sec']['total'] = $this->_sections['sec']['loop'];
    if ($this->_sections['sec']['total'] == 0)
        $this->_sections['sec']['show'] = false;
} else
    $this->_sections['sec']['total'] = 0;
if ($this->_sections['sec']['show']):

            for ($this->_sections['sec']['index'] = $this->_sections['sec']['start'], $this->_sections['sec']['iteration'] = 1;
                 $this->_sections['sec']['iteration'] <= $this->_sections['sec']['total'];
                 $this->_sections['sec']['index'] += $this->_sections['sec']['step'], $this->_sections['sec']['iteration']++):
$this->_sections['sec']['rownum'] = $this->_sections['sec']['iteration'];
$this->_sections['sec']['index_prev'] = $this->_sections['sec']['index'] - $this->_sections['sec']['step'];
$this->_sections['sec']['index_next'] = $this->_sections['sec']['index'] + $this->_sections['sec']['step'];
$this->_sections['sec']['first']      = ($this->_sections['sec']['iteration'] == 1);
$this->_sections['sec']['last']       = ($this->_sections['sec']['iteration'] == $this->_sections['sec']['total']);
?>
				<?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['exchange'][$this->_sections['sec']['index']],'output' => $this->_tpl_vars['exchange'][$this->_sections['sec']['index']],'selected' => $this->_tpl_vars['selectedExchange']), $this);?>

			<?php endfor; endif; ?>
		</select>
 </TD></TR>
 <TR>
 <TD>Item Name</TD>
 <TD><SELECT name="itemId">
  <option value='0'>Select Id</option>
</SELECT>
 </TD></TR>
 <TR><TD>Expiry Date</TD>
    <TD> <?php echo smarty_function_html_select_date(array('prefix' => 'expiryDate','start_year' => 1990,'end_year' => 2025,'day_value_format' => "%02d",'month_value_format' => "%b",'month_format' => "%b",'field_order' => 'DMY'), $this);?>
</TD>
 </TR>
</TABLE>
<BR>
<INPUT type = submit Value=SAVE>
</FORM>
<SCRIPT language="javascript">
  document.form1.elements[0].focus();
  companyChange(document.form1.exchange);
</SCRIPT>
</BODY>
</HTML>